﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Lekkerbek.Web.ViewModels.OrderLines
{
  public class OrderLineViewModel
  {
    public string DishName { get; set; }
    [DisplayName("Prijs")]
    public decimal Price { get; set; }
    public decimal Tax { get; set; }
    public int Quantity { get; set; }
  }
}
