﻿using System.ComponentModel;

namespace Lekkerbek.Web.ViewModels.Companies
{
    public class IndexViewModel
    {
        public int Id { get; set; }
        [DisplayName("Firma")]
        public string Name { get; set; }
        [DisplayName("BTW-nummer")]
        public string VatNumber { get; set; }
        [DisplayName("Contactpersoon")]
        public int ContactPersonId { get; set; }
        [DisplayName("Contactpersoon")]
        public string ContactName { get; set; }
    }
}
