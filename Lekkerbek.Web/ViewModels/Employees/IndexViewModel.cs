﻿using System.ComponentModel;

namespace Lekkerbek.Web.ViewModels.Employees
{
    public class IndexViewModel
    {
        public int Id { get; set; }

        [DisplayName("Voornaam")]
        public string FirstName { get; set; }

        [DisplayName("Familienaam")]
        public string LastName { get; set; }

        [DisplayName("Naam")]
        public string Name { get { return FirstName + " " + LastName; } }

        [DisplayName("Geboortedatum")]
        public DateTime BirthDate { get; set; }

        [DisplayName("Geboortedatum")]
        public string BirthdayString { get { return BirthDate.ToString("dd/MM/yyyy"); } }

        [DisplayName("Adres")]
        public string Adress { get; set; }

        [DisplayName("E-mail")]
        public string Email { get; set; }

        [DisplayName("Telefoonnummer")]
        public string PhoneNumber { get; set; }
    }
}
