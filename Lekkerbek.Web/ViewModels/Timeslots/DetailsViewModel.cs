﻿using Lekkerbek.Web.Models.Entities;
using System.ComponentModel;

namespace Lekkerbek.Web.ViewModels.Timeslots
{
    public class DetailsViewModel
    {
        public int Id { get; set; }
        [DisplayName("Starttijd")]
        public DateTime StartTime { get; set; }

        [DefaultValue(false)]
        [DisplayName("Bezet")]
        public bool IsOccupied { get; set; }

        [DisplayName("Kok")]
        public string CookFirstName { get; set; }

        [DisplayName("Bestelling")]
        public int OrderId { get; set; }
        [DisplayName("Bijkomende opmerkingen")]
        public string? OrderExtraComments { get; set; }
    }
}
