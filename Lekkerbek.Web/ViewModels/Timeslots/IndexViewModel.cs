﻿using Lekkerbek.Web.Models.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace Lekkerbek.Web.ViewModels.Timeslots
{
    public class IndexViewModel
    {
        public int Id { get; set; }
        [DisplayName("Starttijd")]
        public string StartTime { get; set; }

        [DefaultValue(false)]
        [DisplayName("Bezet")]
        public bool IsOccupied { get; set; }

        [DisplayName("Kok")]
        public string CookFirstName { get; set; }
        [DisplayName("Bestelling")]
        public int OrderId { get; set; }

        public string CookIdentityUserId { get; set; }

    }
}
