﻿using Lekkerbek.Web.Models.Entities;
using Lekkerbek.Web.Models.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace Lekkerbek.Web.ViewModels.Customers
{
    public class DetailsViewModel
    {
        public int Id { get; set; }

        [DisplayName("Voornaam")]
        public string FirstName { get; set; }

        [DisplayName("Familienaam")]
        public string LastName { get; set; }

        [DisplayName("Naam")]
        public string Name { get { return FirstName + " " + LastName; } }

        [DisplayName("Geboortedatum")]
        public DateTime BirthDate { get; set; }

        [DisplayName("Adres")]
        public string Adress { get; set; }

        [DisplayName("E-mail")]
        public string Email { get; set; }

        [DisplayName("Telefoonnummer")]
        public string PhoneNumber { get; set; }

        [DisplayName("Bestellingen")]
        public virtual ICollection<Order> Orders { get; set; }

        [DisplayName("Voorkeur")]
        public FoodCategory? FoodPreference { get; set; }

        [DisplayName("Firma")]
        public virtual Company? Company { get; set; }

        [DefaultValue(false)]
        [Display(Name = "Professionele klant?")]
        public bool IsProfessional { get; set; }
    }
}
