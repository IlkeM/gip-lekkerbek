﻿namespace Lekkerbek.Web.ViewModels.Customers
{
    public class CustomerViewModel
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
    }
}
