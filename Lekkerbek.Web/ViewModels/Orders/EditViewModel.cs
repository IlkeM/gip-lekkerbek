﻿using Lekkerbek.Web.Models.Entities;
using Lekkerbek.Web.Models.Enums;
using Lekkerbek.Web.ViewModels.Customers;
using Lekkerbek.Web.ViewModels.Products;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace Lekkerbek.Web.ViewModels.Orders
{
    public class EditViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Klant")]
        public int CustomerId { get; set; }

        [Display(Name = "Tijdslot")]
        public int TimeSlotId { get; set; }

        [Display(Name = "Betaalwijze")]
        public virtual PaymentProcessor? PaymentProcessor { get; set; }

        [Display(Name = "Bestelstatus")]
        public virtual OrderStatus OrderStatus { get; set; }

        [Display(Name = "Bijkomende opmerkingen")]
        public string? ExtraComments { get; set; }

        public Order Order { get; set; }

        public List<Customer> ViewCustomers { get; set; }

        public List<CustomerViewModel> SelectListCreate { get; set; }

        public List<IGrouping<Cook, TimeSlot>> ViewTimeSlots { get; set; } = new List<IGrouping<Cook, TimeSlot>>();

        public List<ProductViewModel> ProductsIdList { get; set; }

        public string? EditAlert { get; set; }

    }
}
