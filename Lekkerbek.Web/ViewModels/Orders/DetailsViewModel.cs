﻿using Lekkerbek.Web.Models.Entities;
using Lekkerbek.Web.Models.Enums;
using Lekkerbek.Web.ViewModels.OrderLines;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Lekkerbek.Web.ViewModels.Orders
{
    public class DetailsViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Aanmaakdatum")]
        public DateTime CreationDate { get; set; }

        [Display(Name = "Bijkomende opmerkingen")]
        public string? ExtraComments { get; set; }

        [Display(Name = "Tijdslot")]
        public DateTime StartTime { get; set; }

        [Display(Name = "Klant")]
        public string Name { get; set; }

        [Display(Name = "Betaalwijze")]
        public virtual PaymentProcessor? PaymentProcessor { get; set; }

        [DefaultValue(false)]
        [Display(Name = "Getrouwheidskorting")]
        public bool HasDiscount { get; set; }

        [Display(Name = "Bestelstatus")]
        public virtual OrderStatus OrderStatus { get; set; }

        public Order Order { get; set; }

        [Range(0, int.MaxValue)]
        public decimal TotalPrice { get; set; }

        public List<OrderLineViewModel> OrderLineList { get; set; }

        public bool CanBeEdited { get; set; }

        public bool IsProfessional { get; set; }

        [DisplayName("Firma")]
        public string CompanyName { get; set; }
        [DisplayName("BTW-nummer")]
        public string VatNumber { get; set; }

    }
}
