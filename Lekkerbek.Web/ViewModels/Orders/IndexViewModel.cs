﻿using Lekkerbek.Web.Models.Entities;
using Lekkerbek.Web.Models.Enums;
using Lekkerbek.Web.ViewModels.Customers;
using Lekkerbek.Web.ViewModels.Products;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Lekkerbek.Web.ViewModels.Orders
{
    public class IndexViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Aanmaakdatum")]
        public string CreationDate { get; set; }

        [Display(Name = "Bijkomende opmerkingen")]
        public string? ExtraComments { get; set; }

        [DisplayName("Tijdsslot")]
        public string? TimeSlotInfo { get; set; }

        [DisplayName("Naam")]
        public string Name { get; set; }

        [Display(Name = "Betaalwijze")]
        public virtual PaymentProcessor? PaymentProcessor { get; set; }

        [Display(Name = "Bestelstatus")]
        public virtual OrderStatus OrderStatus { get; set; }

        [DefaultValue(false)]
        [Display(Name = "Getrouwheidskorting")]
        public bool HasDiscount { get; set; }

        public bool EditPossible { get; set; }

        public bool DeletePossible { get; set; }

        public string? IdentityUserId { get; set; }

    }
}
