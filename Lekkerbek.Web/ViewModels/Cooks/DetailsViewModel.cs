﻿using Lekkerbek.Web.Models.Entities;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel;

namespace Lekkerbek.Web.ViewModels.Cooks
{
    public class DetailsViewModel
    {
        [DisplayName("Tijdslot")]
        public virtual ICollection<TimeSlot> TimeSlots { get; set; }
        [DefaultValue(false)]
        public int Id { get; set; }

        [DisplayName("Voornaam")]
        public string FirstName { get; set; }

        [DisplayName("Familienaam")]
        public string LastName { get; set; }

        [DisplayName("Naam")]
        public string Name { get { return FirstName + " " + LastName; } }

        [DisplayName("Geboortedatum")]
        public DateTime BirthDate { get; set; }

        [DisplayName("Adres")]
        public string Adress { get; set; }

        [DisplayName("E-mail")]
        public string Email { get; set; }

        [DisplayName("Telefoonnummer")]
        public string PhoneNumber { get; set; }
        public bool IsDeleted { get; set; }
        public string IdentityUserId { get; set; }
        public IdentityUser IdentityUser { get; set; }
    }
}
