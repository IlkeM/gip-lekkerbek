﻿using System;
using System.Collections.Generic;
using Lekkerbek.Web;
using Lekkerbek.Web.Models.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace Lekkerbek.Web.Data;

public partial class LekkerbekDbContext : IdentityDbContext
{
    public DbSet<Cook> Cooks { get; set; }
    public DbSet<Clerk> Clerks { get; set; }
    public DbSet<Customer> Customers { get; set; }
    public DbSet<Product> Products { get; set; }
    public DbSet<ProductCategory> ProductCategories { get; set; }
    public DbSet<Menu> Menus { get; set; }
    public DbSet<Order> Orders { get; set; }
    public DbSet<OrderLine> OrderLines { get; set; }
    public DbSet<TimeSlot> TimeSlots { get; set; }
    public DbSet<Company> Companies { get; set; }
    public LekkerbekDbContext()
    {
    }

    public LekkerbekDbContext(DbContextOptions<LekkerbekDbContext> options)
        : base(options)
    {
    }

    //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    //{
    //    IConfiguration configuration = new ConfigurationBuilder()
    //        .AddJsonFile("appsettings.json")
    //        .Build();
    //    optionsBuilder.UseSqlServer(configuration.GetConnectionString("PersonalDB-dev"));
    //    optionsBuilder.EnableSensitiveDataLogging();
    //    optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
    //}

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {

        // makes the courseIDs link to Dishes

        modelBuilder.Entity<Menu>()
          .HasOne(m => m.Starter)
          .WithMany()
          .HasForeignKey(m => m.StarterID)
          .OnDelete(DeleteBehavior.Restrict);

        modelBuilder.Entity<Menu>()
          .HasOne(m => m.Main)
          .WithMany()
          .HasForeignKey(m => m.MainID)
          .OnDelete(DeleteBehavior.Restrict);

        modelBuilder.Entity<Menu>()
            .HasOne(m => m.Dessert)
            .WithMany()
            .HasForeignKey(m => m.DessertID)
            .OnDelete(DeleteBehavior.Restrict);

    OnModelCreatingPartial(modelBuilder);

        base.OnModelCreating(modelBuilder);
    }

    //Identity framework - keys of identity tables are mapped in OnModelCreating method of IdentityDbContext
    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
