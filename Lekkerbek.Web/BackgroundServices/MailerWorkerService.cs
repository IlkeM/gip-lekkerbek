﻿using Lekkerbek.Web.BackgroundServices.Interfaces;
using Lekkerbek.Web.Data;
using Lekkerbek.Web.Models.Entities;
using Lekkerbek.Web.Services;
using Microsoft.EntityFrameworkCore;

namespace Lekkerbek.Web.BackgroundServices
{
    public class MailerWorkerService : IMailerWorkerService
    {
        private readonly ILogger<MailerWorkerService> _logger;
        private readonly IServiceScopeFactory _serviceScopeFactory;

        private readonly TimeSpan ReminderWindow = TimeSpan.FromHours(3);

        public MailerWorkerService(ILogger<MailerWorkerService> logger, IServiceScopeFactory serviceScopeFactory)
        {
            _logger = logger;
            _serviceScopeFactory = serviceScopeFactory;
        }
        public async Task CheckAndSendReminderMail()
        {
            await Console.Out.WriteLineAsync("loop");
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<LekkerbekDbContext>();
                var mailService = scope.ServiceProvider.GetRequiredService<IMailService>();

                List<Order> orders = context.Orders.Include(o => o.Customer).Include(o => o.TimeSlot).Where(o => o.TimeSlot.StartTime > DateTime.Now).ToList();

                DateTime lowerRangeWindowSpan = DateTime.Now + ReminderWindow;
                DateTime upperRangeWindowSpan = lowerRangeWindowSpan + TimeSpan.FromMinutes(1);

                foreach (Order order in orders)
                {
                    if ((lowerRangeWindowSpan < order.TimeSlot.EndTime) && (upperRangeWindowSpan > order.TimeSlot.EndTime))
                    {
                        try
                        {
                            await mailService.SendReminderMail(order);
                        }
                        catch (Exception)
                        {
                            await Console.Out.WriteLineAsync("something went wrong with the reminder email");
                        }
                    }
                }

            }

        }


    }
}
