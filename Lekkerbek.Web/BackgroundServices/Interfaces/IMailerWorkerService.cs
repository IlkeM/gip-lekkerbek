﻿namespace Lekkerbek.Web.BackgroundServices.Interfaces
{
    public interface IMailerWorkerService
    {
        Task CheckAndSendReminderMail();
    }
}
