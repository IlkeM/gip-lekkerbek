﻿using Lekkerbek.Web.BackgroundServices.Interfaces;
using System.Threading;

namespace Lekkerbek.Web.BackgroundServices
{
    public class MailerBackgroundService : BackgroundService
    {
        private readonly IMailerWorkerService _mailerWorkerService;
        public MailerBackgroundService(IMailerWorkerService mailerWorkerService)
        {
            _mailerWorkerService = mailerWorkerService;
        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                await _mailerWorkerService.CheckAndSendReminderMail();
                await Task.Delay(TimeSpan.FromSeconds(60), stoppingToken);
            }

        }
    }
}
