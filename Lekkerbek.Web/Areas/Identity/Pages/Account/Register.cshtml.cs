﻿// Licensed to the .NET Foundation under one or more agreements.
// The .NET Foundation licenses this file to you under the MIT license.
#nullable disable

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading;
using System.Threading.Tasks;
using Lekkerbek.Web.Models.Entities;
using Lekkerbek.Web.Models.Enums;
using Lekkerbek.Web.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Serialization;
using System.Text.RegularExpressions;
using Microsoft.IdentityModel.Tokens;

namespace Lekkerbek.Web.Areas.Identity.Pages.Account
{
    public class RegisterModel : PageModel
    {
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IUserStore<IdentityUser> _userStore;
        private readonly IUserEmailStore<IdentityUser> _emailStore;
        private readonly ILogger<RegisterModel> _logger;
        private readonly IEmailSender _emailSender;
        private readonly ICustomerService _customerService;
        private readonly ICompanyService _companyService;

        public RegisterModel(
            UserManager<IdentityUser> userManager,
            IUserStore<IdentityUser> userStore,
            SignInManager<IdentityUser> signInManager,
            ILogger<RegisterModel> logger,
            IEmailSender emailSender,
            ICustomerService customerService,
            ICompanyService companyService)
        {
            _userManager = userManager;
            _userStore = userStore;
            _emailStore = GetEmailStore();
            _signInManager = signInManager;
            _logger = logger;
            _emailSender = emailSender;
            _customerService = customerService;
            _companyService = companyService;
        }

        /// <summary>
        ///     This API supports the ASP.NET Core Identity default UI infrastructure and is not intended to be used
        ///     directly from your code. This API may change or be removed in future releases.
        /// </summary>
        [BindProperty]
        public InputModel Input { get; set; }

        /// <summary>
        ///     This API supports the ASP.NET Core Identity default UI infrastructure and is not intended to be used
        ///     directly from your code. This API may change or be removed in future releases.
        /// </summary>
        public string ReturnUrl { get; set; }

        /// <summary>
        ///     This API supports the ASP.NET Core Identity default UI infrastructure and is not intended to be used
        ///     directly from your code. This API may change or be removed in future releases.
        /// </summary>
        public IList<AuthenticationScheme> ExternalLogins { get; set; }

        /// <summary>
        /// Customer object supposed to be created during registering of new user.
        /// </summary>
        public Customer Customer { get; set; }

        /// <summary>
        ///     This API supports the ASP.NET Core Identity default UI infrastructure and is not intended to be used
        ///     directly from your code. This API may change or be removed in future releases.
        /// </summary>
        public class InputModel
        {
            /// <summary>
            ///     This API supports the ASP.NET Core Identity default UI infrastructure and is not intended to be used
            ///     directly from your code. This API may change or be removed in future releases.
            /// </summary>
            [Required(ErrorMessage = "Gelieve een e-mail in te geven")]
            [EmailAddress]
            [Display(Name = "E-mail")]
            public string Email { get; set; }

            /// <summary>
            ///     This API supports the ASP.NET Core Identity default UI infrastructure and is not intended to be used
            ///     directly from your code. This API may change or be removed in future releases.
            /// </summary>
            [Required(ErrorMessage = "Gelieve een wachtwoord in te geven")]
            [StringLength(100, ErrorMessage = "Het wachtwoord moet tenminste {2} en uiterst {1} tekens lang zijn.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = "Wachtwoord")]
            public string Password { get; set; }

            /// <summary>
            ///     This API supports the ASP.NET Core Identity default UI infrastructure and is not intended to be used
            ///     directly from your code. This API may change or be removed in future releases.
            /// </summary>
            [DataType(DataType.Password)]
            [Display(Name = "Wachtwoord bevestigen")]
            [Compare("Password", ErrorMessage = "Het wachtwoord komt niet overeen.")]
            public string ConfirmPassword { get; set; }

            [Required(ErrorMessage = "Gelieve een voornaam in te geven")]
            [Display(Name = "Voornaam")]
            public string FirstName { get; set; }

            [Required(ErrorMessage = "Gelieve een familienaam in te geven")]
            [Display(Name = "Familienaam")]
            public string LastName { get; set; }

            [Required(ErrorMessage = "Gelieve een geboortedatum in te geven")]
            [DataType(DataType.Date)]
            [Display(Name = "Geboortedatum")]
            public DateTime BirthDate { get; set; }

            [Required(ErrorMessage = "Gelieve een telefoonnummer in te geven")]
            [Display(Name = "Telefoonnummer")]
            public string PhoneNumber { get; set; }

            [Required(ErrorMessage = "Gelieve een adres in te geven")]
            [Display(Name = "Adres")]
            public string Adress { get; set; }

            [Display(Name = "Voorkeur")]
            public FoodCategory? FoodPreference { get; set; }

            [Display(Name = "Professionele klant?")]
            public bool IsProfessional { get; set; }

            [Display(Name = "Firma")]
            public string CompanyName { get; set; }

            [DisplayName("BTW-nummer")]
            public string VatNumber { get; set; }
        }


        public async Task OnGetAsync(string returnUrl = null)
        {
            ReturnUrl = returnUrl;
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl ??= Url.Content("~/");
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();

            if (Input.IsProfessional)
            {
                if (Input.CompanyName == null)
                {
                    ModelState.AddModelError("Input.CompanyName", "Gelieve een naam voor uw firma in te geven");
                }

                if (Input.VatNumber == null)
                {
                    ModelState.AddModelError("Input.VatNumber", "Gelieve een BTW-nummer in te geven");
                } else
                {
                    bool isValid = Regex.IsMatch(Input.VatNumber, @"^BE0\d{9}$");

                    if (!isValid)
                    {
                        ModelState.AddModelError("Input.VatNumber", "Gelieve een correct BTW-nummer in te geven");
                    }
                }
            }

            if (ModelState.IsValid)
            {
                var user = CreateUser();

                await _userStore.SetUserNameAsync(user, Input.Email, CancellationToken.None);
                await _emailStore.SetEmailAsync(user, Input.Email, CancellationToken.None);
                var result = await _userManager.CreateAsync(user, Input.Password);

                if (result.Succeeded)
                {
                    Customer customer = new Customer
                    {
                        FirstName = Input.FirstName,
                        LastName = Input.LastName,
                        BirthDate = Input.BirthDate,
                        Adress = Input.Adress,
                        Email = Input.Email,
                        PhoneNumber = Input.PhoneNumber,
                        FoodPreference = Input.FoodPreference,
                        IsProfessional = Input.IsProfessional,
                        IdentityUser = user,
                        IdentityUserId = user.Id
                    };

                    await _customerService.CreateCustomer(customer);

                    if (customer.IsProfessional)
                    {
                        Company company = new Company
                        {
                            Name = Input.CompanyName,
                            VatNumber = Input.VatNumber,
                            ContactPerson = customer,
                            ContactPersonId = customer.Id
                        };

                        await _companyService.CreateCompany(company);

                        customer.CompanyId = company.Id;

                    }

                    //default role
                    _userManager.AddToRoleAsync(user, "Customer").Wait();
                    //end default role

                    _logger.LogInformation("User created a new account with password.");

                    var userId = await _userManager.GetUserIdAsync(user);
                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
                    var callbackUrl = Url.Page(
                        "/Account/ConfirmEmail",
                        pageHandler: null,
                        values: new { area = "Identity", userId = userId, code = code, returnUrl = returnUrl },
                        protocol: Request.Scheme);

                    await _emailSender.SendEmailAsync(Input.Email, "Bevestig uw e-mail",
                        $"Bevestig uw e-mail door <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>hier</a> te klikken.");

                    if (_userManager.Options.SignIn.RequireConfirmedAccount)
                    {
                        return RedirectToPage("RegisterConfirmation", new { email = Input.Email, returnUrl = returnUrl });
                    }
                    else
                    {
                        await _signInManager.SignInAsync(user, isPersistent: false);
                        return LocalRedirect(returnUrl);
                    }
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }

        private IdentityUser CreateUser()
        {
            try
            {
                return Activator.CreateInstance<IdentityUser>();
            }
            catch
            {
                throw new InvalidOperationException($"Can't create an instance of '{nameof(IdentityUser)}'. " +
                    $"Ensure that '{nameof(IdentityUser)}' is not an abstract class and has a parameterless constructor, or alternatively " +
                    $"override the register page in /Areas/Identity/Pages/Account/Register.cshtml");
            }
        }

        private IUserEmailStore<IdentityUser> GetEmailStore()
        {
            if (!_userManager.SupportsUserEmail)
            {
                throw new NotSupportedException("The default UI requires a user store with email support.");
            }
            return (IUserEmailStore<IdentityUser>)_userStore;
        }
    }
}
