﻿using System.ComponentModel;

namespace Lekkerbek.Web.Models.Entities
{
    public class Company
    {
        public int Id { get; set; }
        [DisplayName("Firma")]
        public string Name { get; set; }
        [DisplayName("BTW-nummer")]
        public string VatNumber { get; set; }
        [DisplayName("ContactpersoonId")]
        public int ContactPersonId { get; set; }
        [DisplayName("Contactpersoon")]
        public virtual Customer ContactPerson { get; set; }
    }
}
