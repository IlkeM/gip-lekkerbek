﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel;

namespace Lekkerbek.Web.Models.Entities
{
    public class ProductCategory
    {
        public int Id { get; set; }
        [DisplayName("Categorie")]
        public string Category { get; set; }
        [DisplayName("BTW")]
        public decimal Tax { get; set; }
    }
}
