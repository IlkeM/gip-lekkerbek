﻿using Lekkerbek.Web.Models.Enums;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Lekkerbek.Web.Models.Entities
{
  public class Customer : Person
  {
    [DisplayName("Bestellingen")]
    public virtual ICollection<Order> Orders { get; set; }

    [DisplayName("Voorkeur")]
    public FoodCategory? FoodPreference { get; set; }

    [ForeignKey("Company")]
    public int? CompanyId { get; set; }
    public virtual Company Company { get; set; }

    [DefaultValue(false)]
    [Display(Name = "Professionele klant?")]
    public bool IsProfessional { get; set; }

    public string? IdentityUserId { get; set; }

    public IdentityUser? IdentityUser { get; set; }
    }
}
