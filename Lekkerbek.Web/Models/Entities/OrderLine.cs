﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace Lekkerbek.Web.Models.Entities
{
    public class OrderLine
    {
        public int OrderLineId { get; set; }
        public int OrderID { get; set; }
        public virtual Order Order { get; set; }
        public int DishId { get; set; }
        public virtual Product Dish { get; set; }
        [Range(0, int.MaxValue)]
        public int Quantity { get; set; }
    }
}
