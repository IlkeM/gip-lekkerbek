﻿using Lekkerbek.Web.Models.Enums;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Lekkerbek.Web.Models.Entities
{
    public class Product
    {

        public int Id { get; set; }

        [Required(ErrorMessage = "Gelieve een naam in te geven")]
        [DisplayName("Naam")]
        public string Name { get; set; }

        [Range(0, 50)]
        [DisplayName("Prijs")]
        public decimal Price { get; set; }

        [Required(ErrorMessage = "Gelieve een beschrijving in te geven")]
        [DisplayName("Beschrijving")]
        public string Description { get; set; }

        [DisplayName("Bestellijn")]
        public virtual ICollection<OrderLine> OrderLines { get; set; }

        [DisplayName("Categorie")]
        public FoodCategory? FoodCategory { get; set; }

        [DisplayName("Gang")]
        public Course? Course { get; set; }
        [DisplayName("Menus")]
        public virtual ICollection<Menu> Menus { get; set; }
        public int ProductCategoryId { get; set; }
        [DisplayName("Produkt Categorie")]
        public virtual ProductCategory ProductCategory { get; set; }
    }
}
