﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Update.Internal;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Lekkerbek.Web.Models.Entities
{
    public abstract class Person
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Gelieve een voornaam in te geven")]
        [DisplayName("Voornaam")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Gelieve een voornaam in te geven")]
        [DisplayName("Familienaam")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Gelieve een geboortedatum in te geven")]
        [DataType(DataType.Date)]
        [DisplayName("Geboortedatum")]

        public DateTime BirthDate { get; set; }

        [Required(ErrorMessage = "Gelieve een adres in te geven")]
        [DisplayName("Adres")]
        public string Adress { get; set; }

        [Required(ErrorMessage = "Gelieve een email in te geven")]
        [DataType(DataType.EmailAddress)]
        [DisplayName("E-mail")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Gelieve een telefoonnummer in te geven")]
        [DataType(DataType.PhoneNumber)]
        [DisplayName("Telefoonnummer")]
        public string PhoneNumber { get; set; }

        [NotMapped]
        public string FullName { get => FirstName + " " + LastName; }

        /* Update-database na toevoegen van : 
         * 
           * public string IdentityUserId { get; set; }
              public IdentityUser IdentityUser { get; set; }

        Geeft deze errors:
         * - ALTER TABLE [Cooks] ADD CONSTRAINT [FK_Cooks_AspNetUsers_IdentityUserId] FOREIGN KEY ([IdentityUserId]) REFERENCES [AspNetUsers] ([Id]) ON DELETE CASCADE
         * - The ALTER TABLE statement conflicted with the FOREIGN KEY constraint "FK_Cooks_AspNetUsers_IdentityUserId". The conflict occurred in database "gipIlke", table "dbo.AspNetUsers", column 'Id'.
         * 

        */
    }
}
