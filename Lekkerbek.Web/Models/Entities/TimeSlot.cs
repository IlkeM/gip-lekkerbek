﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Lekkerbek.Web.Models.Entities
{
    public class TimeSlot
    {
        public int Id { get; set; }
        [DisplayName("Starttijd")]
        public DateTime StartTime { get; set; }
        [DisplayName("Datum")]
        public DateTime Date { get; set; }

        [DefaultValue(false)]
        [DisplayName("Bezet")]
        public bool IsOccupied { get; set; }

        [DisplayName("Bestelling")]
        public virtual Order Order { get; set; }

        public int? CookId { get; set; }
        [DisplayName("Kok")]
        public virtual Cook Cook { get; set; }

        // deze getter return the starttijd + 15 minuten
        [NotMapped]
        [DisplayName("Eindtijd")]
        public DateTime EndTime
        {
            get
            {
                TimeSpan Shift = new TimeSpan(hours: 0, minutes: 15, seconds: 0);
                return StartTime + Shift;
            }
        }

    }
}
