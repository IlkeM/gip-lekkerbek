﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Lekkerbek.Web.Models.Entities
{
    public class Cook : Person
    {
        [DisplayName("Tijdslot")]
        public virtual ICollection<TimeSlot> TimeSlots { get; set; }
        [DefaultValue(false)]
        public bool IsDeleted { get; set; }
        public string IdentityUserId { get; set; }
        public IdentityUser IdentityUser { get; set; }

        [DisplayName("Paswoord")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Gelieve een paswoord in te geven")]
        [RegularExpression(@"^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!.]).*$",
         ErrorMessage = "Paswoord minstens 8 characters, 1 hoofdletter, 1 kleineletter, 1 cijfer, 1 speciale teken")]
        [NotMapped]
        public string Password { get; set; }
    }
}
