﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Lekkerbek.Web.Models.Entities
{
  public class Menu
  {
    public int Id { get; set; }
    [DisplayName("Naam")]
    public string Name { get; set; }
    [DisplayName("Omschrijving")]
    public string Description { get; set; }

    public int? StarterID { get; set; }
    [DisplayName("Voorgerecht")]
    public virtual Product Starter { get; set; }
    public int? MainID { get; set; }
    [DisplayName("Hoofdgerecht")]
    public virtual Product Main { get; set; }
    public int? DessertID { get; set; }
    [DisplayName("Nagerecht")]
    public virtual Product Dessert { get; set; }
  }
}
