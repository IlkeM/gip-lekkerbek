﻿using Lekkerbek.Web.Models.Enums;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Lekkerbek.Web.Models.Entities
{
    public class Order
    {
        public int Id { get; set; }

        [Display(Name = "Aanmaakdatum")]
        public DateTime CreationDate { get; set; }

        [Display(Name = "Bijkomende opmerkingen")]
        public string? ExtraComments { get; set; }

        [Display(Name = "Tijdslot ID")]
        public int TimeSlotId { get; set; }

        [Display(Name = "Tijdslot")]
        public virtual TimeSlot TimeSlot { get; set; }

        [Display(Name = "Klant")]
        public int CustomerId { get; set; }

        [Display(Name = "Klant")]
        public virtual Customer Customer { get; set; }

        [DefaultValue(false)]
        [Display(Name = "Getrouwheidskorting")]
        public bool HasDiscount { get; set; }

        [Display(Name = "Betaalwijze")]
        public virtual PaymentProcessor? PaymentProcessor { get; set; }

        [Display(Name = "Bestelstatus")]
        public virtual OrderStatus OrderStatus { get; set; }

        public virtual ICollection<OrderLine> OrderLines { get; set; }


    }
}
