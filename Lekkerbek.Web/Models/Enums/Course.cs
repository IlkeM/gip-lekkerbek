﻿using System.ComponentModel.DataAnnotations;

namespace Lekkerbek.Web.Models.Enums
{
  public enum Course
  {
    [Display(Name = "Voorgerecht")]
    Voorgerecht, //0
    [Display(Name = "Hoofdgerecht")]
    Hoofdgerecht, //1
    [Display(Name = "Nagerecht")]
    Nagerecht //2
  }
}
