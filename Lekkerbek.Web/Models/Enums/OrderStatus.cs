﻿using System.ComponentModel.DataAnnotations;

namespace Lekkerbek.Web.Models.Enums
{
  public enum OrderStatus
  {
    [Display(Name = "Wachten op betaling")]
    Wachten_op_betaling, //0
    [Display(Name = "Betaald")]
    Betaald, //1
    [Display(Name = "Afgerond")]
    Afgerond //2
  }
}
