﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Lekkerbek.Web.Models.Enums
{
  public enum PaymentProcessor
  {
    [Display(Name = "Contant")]
    Contant, //0
    [Display(Name ="Kaart")]
    Kaart, //1
    [Display(Name = "Gsm")]
    Gsm, //2
    [Display(Name = "Overschrijving")]
    Overschrijving //3
  }
}
