﻿using System.ComponentModel.DataAnnotations;

namespace Lekkerbek.Web.Models.Enums
{
  public enum FoodCategory
  {
    [Display(Name = "Vlees")]
    Vlees, //0
    [Display(Name = "Vis")]
    Vis, //1
    [Display(Name = "Vegetarisch")]
    Vegetarisch, //2
    [Display(Name = "Oosters")]
    Oosters //3
  }
}
