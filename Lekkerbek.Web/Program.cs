using Lekkerbek.Web.BackgroundServices;
using Lekkerbek.Web.BackgroundServices.Interfaces;
using Lekkerbek.Web.Data;
using Lekkerbek.Web.Models.Entities;
using Lekkerbek.Web.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

var builder = WebApplication.CreateBuilder(args);
/*var connectionString = builder.Configuration.GetConnectionString("LekkerbekDbContextConnection") ?? throw new InvalidOperationException("Connection string 'LekkerbekDbContextConnection' not found.");*/


//Kendo Tables
builder.Services.AddRazorPages()
// Maintain property names during serialization. See:
// https://github.com/aspnet/Announcements/issues/194
 .AddNewtonsoftJson(options => options.SerializerSettings.ContractResolver =
new Newtonsoft.Json.Serialization.DefaultContractResolver());
// Add Kendo UI services to the services container
builder.Services.AddKendo();
builder.Services.AddRazorPages();

// Add services to the container.
builder.Services.AddControllersWithViews();
builder.Services.AddDbContext<LekkerbekDbContext>(options =>
options.UseSqlServer(builder.Configuration.GetConnectionString("PersonalDb-dev")).EnableSensitiveDataLogging().UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking));

builder.Services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
  .AddRoles<IdentityRole>()
  .AddEntityFrameworkStores<LekkerbekDbContext>();

builder.Services.AddScoped<ICustomerService, CustomersService>();
builder.Services.AddTransient<ICompanyService, CompanyService>();
builder.Services.AddTransient<IContactPersonService, CustomersService>();
builder.Services.AddTransient<IOrderService, OrdersService>();
builder.Services.AddTransient<ICookService, CookService>();
builder.Services.AddTransient<ITimeSlotService, TimeSlotService>();
builder.Services.AddTransient<IMailService, MailService>();
builder.Services.AddTransient<IClerkService, ClerkService>();
builder.Services.AddHostedService<MailerBackgroundService>();
builder.Services.AddTransient<IMailerWorkerService, MailerWorkerService>();
builder.Services.AddTransient<IEmailSender, EmailSender>();

var app = builder.Build();


#region Identity framework

var serviceProvider = app.Services.CreateScope().ServiceProvider;

//Create roles
var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();

await roleManager.CreateAsync(new IdentityRole("Super"));
await roleManager.CreateAsync(new IdentityRole("Administrator"));
await roleManager.CreateAsync(new IdentityRole("Customer"));
await roleManager.CreateAsync(new IdentityRole("Cook"));
await roleManager.CreateAsync(new IdentityRole("Clerk"));


//User - roles link
var userManager = serviceProvider.GetRequiredService<UserManager<IdentityUser>>();

//When user is already created/registered in database

//var adminUser = await userManager.FindByEmailAsync("diederik.billiet@student.ucll.be");
//await userManager.AddToRoleAsync(adminUser, "Administrator");

//Create user in program

//var createAdmin = new IdentityUser
//{
//    Email = "diederik.billiet@student.ucll.be",
//    UserName = "diederik.billiet@student.ucll.be",
//    EmailConfirmed = true
//};
//var resultAdmin = await userManager.CreateAsync(createAdmin, ""); //Passwoord met minstens hoofdletter/kleine letter/cijfer/leesteken
//if (resultAdmin.Succeeded)
//{
//    await userManager.AddToRoleAsync(createAdmin, "Administrator");
//}


var clerkUser = new IdentityUser
{
    Email = "clerk@ucll.be",
    UserName = "clerk@ucll.be",
    EmailConfirmed = true
};
var resultClerk = await userManager.CreateAsync(clerkUser, "ClerkPass123..");
if (resultClerk.Succeeded)
{
    await userManager.AddToRoleAsync(clerkUser, "Clerk");
}

var adminUser = new IdentityUser
{
    Email = "admin@ucll.be",
    UserName = "admin@ucll.be",
    EmailConfirmed = true
};
var resultAdmin = await userManager.CreateAsync(adminUser, "AdminPass123..");
if (resultAdmin.Succeeded)
{
    await userManager.AddToRoleAsync(adminUser, "Administrator");
}
var superUser = new IdentityUser
{
    Email = "super@ucll.be",
    UserName = "super@ucll.be",
    EmailConfirmed = true
};
var resultSuper = await userManager.CreateAsync(superUser, "SuperPass123..");
if (resultSuper.Succeeded)
{
    await userManager.AddToRoleAsync(superUser, "Super");
}
var customerUser = new IdentityUser
{
    Email = "customer@ucll.be",
    UserName = "customer@ucll.be",
    EmailConfirmed = true
};
var resultCustomer = await userManager.CreateAsync(customerUser, "CustomerPass123..");
if (resultCustomer.Succeeded)
{
    await userManager.AddToRoleAsync(customerUser, "Customer");
}
var cookUser = new IdentityUser
{
    Email = "cook@ucll.be",
    UserName = "cook@ucll.be",
    EmailConfirmed = true
};
var resultCook = await userManager.CreateAsync(cookUser, "CookPass123..");
if (resultCook.Succeeded)
{
    await userManager.AddToRoleAsync(cookUser, "Cook");
}



#endregion


// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

app.MapRazorPages();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
