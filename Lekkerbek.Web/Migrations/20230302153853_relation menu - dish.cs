﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Lekkerbek.Web.Migrations
{
    /// <inheritdoc />
    public partial class relationmenudish : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Menus_Dishes_DessertID",
                table: "Menus");

            migrationBuilder.DropForeignKey(
                name: "FK_Menus_Dishes_MainID",
                table: "Menus");

            migrationBuilder.DropForeignKey(
                name: "FK_Menus_Dishes_StarterID",
                table: "Menus");

            migrationBuilder.AlterColumn<int>(
                name: "StarterID",
                table: "Menus",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "MainID",
                table: "Menus",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DessertID",
                table: "Menus",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DishId",
                table: "Menus",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Menus_DishId",
                table: "Menus",
                column: "DishId");

            migrationBuilder.AddForeignKey(
                name: "FK_Menus_Dishes_DessertID",
                table: "Menus",
                column: "DessertID",
                principalTable: "Dishes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Menus_Dishes_DishId",
                table: "Menus",
                column: "DishId",
                principalTable: "Dishes",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Menus_Dishes_MainID",
                table: "Menus",
                column: "MainID",
                principalTable: "Dishes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Menus_Dishes_StarterID",
                table: "Menus",
                column: "StarterID",
                principalTable: "Dishes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Menus_Dishes_DessertID",
                table: "Menus");

            migrationBuilder.DropForeignKey(
                name: "FK_Menus_Dishes_DishId",
                table: "Menus");

            migrationBuilder.DropForeignKey(
                name: "FK_Menus_Dishes_MainID",
                table: "Menus");

            migrationBuilder.DropForeignKey(
                name: "FK_Menus_Dishes_StarterID",
                table: "Menus");

            migrationBuilder.DropIndex(
                name: "IX_Menus_DishId",
                table: "Menus");

            migrationBuilder.DropColumn(
                name: "DishId",
                table: "Menus");

            migrationBuilder.AlterColumn<int>(
                name: "StarterID",
                table: "Menus",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "MainID",
                table: "Menus",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "DessertID",
                table: "Menus",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Menus_Dishes_DessertID",
                table: "Menus",
                column: "DessertID",
                principalTable: "Dishes",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Menus_Dishes_MainID",
                table: "Menus",
                column: "MainID",
                principalTable: "Dishes",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Menus_Dishes_StarterID",
                table: "Menus",
                column: "StarterID",
                principalTable: "Dishes",
                principalColumn: "Id");
        }
    }
}
