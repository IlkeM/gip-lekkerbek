﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Lekkerbek.Web.Migrations
{
    /// <inheritdoc />
    public partial class dishtoproduct : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Menus_Dishes_DessertID",
                table: "Menus");

            migrationBuilder.DropForeignKey(
                name: "FK_Menus_Dishes_DishId",
                table: "Menus");

            migrationBuilder.DropForeignKey(
                name: "FK_Menus_Dishes_MainID",
                table: "Menus");

            migrationBuilder.DropForeignKey(
                name: "FK_Menus_Dishes_StarterID",
                table: "Menus");

            migrationBuilder.DropForeignKey(
                name: "FK_OrderLines_Dishes_DishId",
                table: "OrderLines");

            migrationBuilder.DropTable(
                name: "Dishes");

            migrationBuilder.RenameColumn(
                name: "DishId",
                table: "Menus",
                newName: "ProductId");

            migrationBuilder.RenameIndex(
                name: "IX_Menus_DishId",
                table: "Menus",
                newName: "IX_Menus_ProductId");

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Price = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    FoodCategory = table.Column<int>(type: "int", nullable: true),
                    Course = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Menus_Products_DessertID",
                table: "Menus",
                column: "DessertID",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Menus_Products_MainID",
                table: "Menus",
                column: "MainID",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Menus_Products_ProductId",
                table: "Menus",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Menus_Products_StarterID",
                table: "Menus",
                column: "StarterID",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OrderLines_Products_DishId",
                table: "OrderLines",
                column: "DishId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Menus_Products_DessertID",
                table: "Menus");

            migrationBuilder.DropForeignKey(
                name: "FK_Menus_Products_MainID",
                table: "Menus");

            migrationBuilder.DropForeignKey(
                name: "FK_Menus_Products_ProductId",
                table: "Menus");

            migrationBuilder.DropForeignKey(
                name: "FK_Menus_Products_StarterID",
                table: "Menus");

            migrationBuilder.DropForeignKey(
                name: "FK_OrderLines_Products_DishId",
                table: "OrderLines");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.RenameColumn(
                name: "ProductId",
                table: "Menus",
                newName: "DishId");

            migrationBuilder.RenameIndex(
                name: "IX_Menus_ProductId",
                table: "Menus",
                newName: "IX_Menus_DishId");

            migrationBuilder.CreateTable(
                name: "Dishes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Course = table.Column<int>(type: "int", nullable: false),
                    FoodCategory = table.Column<int>(type: "int", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Price = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dishes", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Menus_Dishes_DessertID",
                table: "Menus",
                column: "DessertID",
                principalTable: "Dishes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Menus_Dishes_DishId",
                table: "Menus",
                column: "DishId",
                principalTable: "Dishes",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Menus_Dishes_MainID",
                table: "Menus",
                column: "MainID",
                principalTable: "Dishes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Menus_Dishes_StarterID",
                table: "Menus",
                column: "StarterID",
                principalTable: "Dishes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OrderLines_Dishes_DishId",
                table: "OrderLines",
                column: "DishId",
                principalTable: "Dishes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
