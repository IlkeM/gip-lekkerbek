﻿// <auto-generated />
using System;
using Lekkerbek.Web.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace Lekkerbek.Web.Migrations
{
    [DbContext(typeof(LekkerbekDbContext))]
    [Migration("20230302153853_relation menu - dish")]
    partial class relationmenudish
    {
        /// <inheritdoc />
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.3")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder);

            modelBuilder.Entity("Lekkerbek.Web.Models.Entities.Dish", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<int>("Course")
                        .HasColumnType("int");

                    b.Property<int>("FoodCategory")
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<decimal>("Price")
                        .HasColumnType("decimal(18,2)");

                    b.HasKey("Id");

                    b.ToTable("Dishes");
                });

            modelBuilder.Entity("Lekkerbek.Web.Models.Entities.Menu", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("DessertID")
                        .IsRequired()
                        .HasColumnType("int");

                    b.Property<int?>("DishId")
                        .HasColumnType("int");

                    b.Property<int?>("MainID")
                        .IsRequired()
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int?>("StarterID")
                        .IsRequired()
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("DessertID");

                    b.HasIndex("DishId");

                    b.HasIndex("MainID");

                    b.HasIndex("StarterID");

                    b.ToTable("Menus");
                });

            modelBuilder.Entity("Lekkerbek.Web.Models.Entities.Order", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<DateTime>("CreationDate")
                        .HasColumnType("datetime2");

                    b.Property<int>("CustomerId")
                        .HasColumnType("int");

                    b.Property<string>("ExtraComments")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("OrderStatus")
                        .HasColumnType("int");

                    b.Property<int>("PaymentProcessor")
                        .HasColumnType("int");

                    b.Property<DateTime>("PickupTime")
                        .HasColumnType("datetime2");

                    b.Property<int>("TimeSlotId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("CustomerId");

                    b.HasIndex("TimeSlotId")
                        .IsUnique();

                    b.ToTable("Orders");
                });

            modelBuilder.Entity("Lekkerbek.Web.Models.Entities.OrderLine", b =>
                {
                    b.Property<int>("OrderLineId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("OrderLineId"));

                    b.Property<int>("DishId")
                        .HasColumnType("int");

                    b.Property<int>("OrderID")
                        .HasColumnType("int");

                    b.Property<int>("Quantity")
                        .HasColumnType("int");

                    b.HasKey("OrderLineId");

                    b.HasIndex("DishId");

                    b.HasIndex("OrderID");

                    b.ToTable("OrderLine");
                });

            modelBuilder.Entity("Lekkerbek.Web.Models.Entities.Person", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("Adress")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("BirthDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("Discriminator")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PhoneNumber")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Persons");

                    b.HasDiscriminator<string>("Discriminator").HasValue("Person");

                    b.UseTphMappingStrategy();
                });

            modelBuilder.Entity("Lekkerbek.Web.Models.Entities.TimeSlot", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<int>("CookId")
                        .HasColumnType("int");

                    b.Property<DateTime>("Date")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("StartTime")
                        .HasColumnType("datetime2");

                    b.HasKey("Id");

                    b.HasIndex("CookId");

                    b.ToTable("TimeSlots");
                });

            modelBuilder.Entity("Lekkerbek.Web.Models.Entities.Cook", b =>
                {
                    b.HasBaseType("Lekkerbek.Web.Models.Entities.Person");

                    b.HasDiscriminator().HasValue("Cook");
                });

            modelBuilder.Entity("Lekkerbek.Web.Models.Entities.Customer", b =>
                {
                    b.HasBaseType("Lekkerbek.Web.Models.Entities.Person");

                    b.Property<int>("FoodPreference")
                        .HasColumnType("int");

                    b.HasDiscriminator().HasValue("Customer");
                });

            modelBuilder.Entity("Lekkerbek.Web.Models.Entities.Menu", b =>
                {
                    b.HasOne("Lekkerbek.Web.Models.Entities.Dish", "Dessert")
                        .WithMany()
                        .HasForeignKey("DessertID")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();

                    b.HasOne("Lekkerbek.Web.Models.Entities.Dish", null)
                        .WithMany("Menus")
                        .HasForeignKey("DishId");

                    b.HasOne("Lekkerbek.Web.Models.Entities.Dish", "Main")
                        .WithMany()
                        .HasForeignKey("MainID")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();

                    b.HasOne("Lekkerbek.Web.Models.Entities.Dish", "Starter")
                        .WithMany()
                        .HasForeignKey("StarterID")
                        .OnDelete(DeleteBehavior.Restrict)
                        .IsRequired();

                    b.Navigation("Dessert");

                    b.Navigation("Main");

                    b.Navigation("Starter");
                });

            modelBuilder.Entity("Lekkerbek.Web.Models.Entities.Order", b =>
                {
                    b.HasOne("Lekkerbek.Web.Models.Entities.Customer", "Customer")
                        .WithMany("Orders")
                        .HasForeignKey("CustomerId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Lekkerbek.Web.Models.Entities.TimeSlot", "TimeSlot")
                        .WithOne("Order")
                        .HasForeignKey("Lekkerbek.Web.Models.Entities.Order", "TimeSlotId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Customer");

                    b.Navigation("TimeSlot");
                });

            modelBuilder.Entity("Lekkerbek.Web.Models.Entities.OrderLine", b =>
                {
                    b.HasOne("Lekkerbek.Web.Models.Entities.Dish", "Dish")
                        .WithMany("OrderLines")
                        .HasForeignKey("DishId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Lekkerbek.Web.Models.Entities.Order", "Order")
                        .WithMany("OrderLines")
                        .HasForeignKey("OrderID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Dish");

                    b.Navigation("Order");
                });

            modelBuilder.Entity("Lekkerbek.Web.Models.Entities.TimeSlot", b =>
                {
                    b.HasOne("Lekkerbek.Web.Models.Entities.Cook", "Cook")
                        .WithMany("TimeSlots")
                        .HasForeignKey("CookId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Cook");
                });

            modelBuilder.Entity("Lekkerbek.Web.Models.Entities.Dish", b =>
                {
                    b.Navigation("Menus");

                    b.Navigation("OrderLines");
                });

            modelBuilder.Entity("Lekkerbek.Web.Models.Entities.Order", b =>
                {
                    b.Navigation("OrderLines");
                });

            modelBuilder.Entity("Lekkerbek.Web.Models.Entities.TimeSlot", b =>
                {
                    b.Navigation("Order")
                        .IsRequired();
                });

            modelBuilder.Entity("Lekkerbek.Web.Models.Entities.Cook", b =>
                {
                    b.Navigation("TimeSlots");
                });

            modelBuilder.Entity("Lekkerbek.Web.Models.Entities.Customer", b =>
                {
                    b.Navigation("Orders");
                });
#pragma warning restore 612, 618
        }
    }
}
