﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Lekkerbek.Web.Migrations
{
  /// <inheritdoc />
  public partial class Allentities : Migration
  {
    /// <inheritdoc />
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.CreateTable(
          name: "Dishes",
          columns: table => new
          {
            Id = table.Column<int>(type: "int", nullable: false)
                  .Annotation("SqlServer:Identity", "1, 1"),
            Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
            Price = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
            FoodCategory = table.Column<int>(type: "int", nullable: false),
            Course = table.Column<int>(type: "int", nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_Dishes", x => x.Id);
          });

      migrationBuilder.CreateTable(
          name: "Persons",
          columns: table => new
          {
            Id = table.Column<int>(type: "int", nullable: false)
                  .Annotation("SqlServer:Identity", "1, 1"),
            FirstName = table.Column<string>(type: "nvarchar(max)", nullable: false),
            LastName = table.Column<string>(type: "nvarchar(max)", nullable: false),
            BirthDate = table.Column<DateTime>(type: "datetime2", nullable: false),
            Adress = table.Column<string>(type: "nvarchar(max)", nullable: false),
            Email = table.Column<string>(type: "nvarchar(max)", nullable: false),
            PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: false),
            Discriminator = table.Column<string>(type: "nvarchar(max)", nullable: false),
            FoodPreference = table.Column<int>(type: "int", nullable: true)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_Persons", x => x.Id);
          });

      migrationBuilder.CreateTable(
          name: "Menus",
          columns: table => new
          {
            Id = table.Column<int>(type: "int", nullable: false)
                  .Annotation("SqlServer:Identity", "1, 1"),
            Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
            Description = table.Column<string>(type: "nvarchar(max)", nullable: false),
            StarterID = table.Column<int>(type: "int", nullable: true),
            MainID = table.Column<int>(type: "int", nullable: true),
            DessertID = table.Column<int>(type: "int", nullable: true)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_Menus", x => x.Id);
            table.ForeignKey(
                      name: "FK_Menus_Dishes_DessertID",
                      column: x => x.DessertID,
                      principalTable: "Dishes",
                      principalColumn: "Id");
            table.ForeignKey(
                      name: "FK_Menus_Dishes_MainID",
                      column: x => x.MainID,
                      principalTable: "Dishes",
                      principalColumn: "Id");
            table.ForeignKey(
                      name: "FK_Menus_Dishes_StarterID",
                      column: x => x.StarterID,
                      principalTable: "Dishes",
                      principalColumn: "Id");
          });

      migrationBuilder.CreateTable(
          name: "TimeSlots",
          columns: table => new
          {
            Id = table.Column<int>(type: "int", nullable: false)
                  .Annotation("SqlServer:Identity", "1, 1"),
            StartTime = table.Column<DateTime>(type: "datetime2", nullable: false),
            Date = table.Column<DateTime>(type: "datetime2", nullable: false),
            CookId = table.Column<int>(type: "int", nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_TimeSlots", x => x.Id);
            table.ForeignKey(
                      name: "FK_TimeSlots_Persons_CookId",
                      column: x => x.CookId,
                      principalTable: "Persons",
                      principalColumn: "Id",
                      onDelete: ReferentialAction.Cascade);
          });

      migrationBuilder.CreateTable(
          name: "Orders",
          columns: table => new
          {
            Id = table.Column<int>(type: "int", nullable: false)
                  .Annotation("SqlServer:Identity", "1, 1"),
            CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
            PickupTime = table.Column<DateTime>(type: "datetime2", nullable: false),
            ExtraComments = table.Column<string>(type: "nvarchar(max)", nullable: true),
            TimeSlotId = table.Column<int>(type: "int", nullable: false),
            CustomerId = table.Column<int>(type: "int", nullable: false),
            PaymentProcessor = table.Column<int>(type: "int", nullable: false),
            OrderStatus = table.Column<int>(type: "int", nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_Orders", x => x.Id);
            table.ForeignKey(
                      name: "FK_Orders_Persons_CustomerId",
                      column: x => x.CustomerId,
                      principalTable: "Persons",
                      principalColumn: "Id",
                      onDelete: ReferentialAction.Cascade);
            table.ForeignKey(
                      name: "FK_Orders_TimeSlots_TimeSlotId",
                      column: x => x.TimeSlotId,
                      principalTable: "TimeSlots",
                      principalColumn: "Id",
                      onDelete: ReferentialAction.NoAction);
          });

      migrationBuilder.CreateTable(
          name: "OrderLine",
          columns: table => new
          {
            OrderLineId = table.Column<int>(type: "int", nullable: false)
                  .Annotation("SqlServer:Identity", "1, 1"),
            OrderID = table.Column<int>(type: "int", nullable: false),
            DishId = table.Column<int>(type: "int", nullable: false),
            Quantity = table.Column<int>(type: "int", nullable: false)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_OrderLine", x => x.OrderLineId);
            table.ForeignKey(
                      name: "FK_OrderLine_Dishes_DishId",
                      column: x => x.DishId,
                      principalTable: "Dishes",
                      principalColumn: "Id",
                      onDelete: ReferentialAction.Cascade);
            table.ForeignKey(
                      name: "FK_OrderLine_Orders_OrderID",
                      column: x => x.OrderID,
                      principalTable: "Orders",
                      principalColumn: "Id",
                      onDelete: ReferentialAction.Cascade);
          });

      migrationBuilder.CreateIndex(
          name: "IX_Menus_DessertID",
          table: "Menus",
          column: "DessertID");

      migrationBuilder.CreateIndex(
          name: "IX_Menus_MainID",
          table: "Menus",
          column: "MainID");

      migrationBuilder.CreateIndex(
          name: "IX_Menus_StarterID",
          table: "Menus",
          column: "StarterID");

      migrationBuilder.CreateIndex(
          name: "IX_OrderLine_DishId",
          table: "OrderLine",
          column: "DishId");

      migrationBuilder.CreateIndex(
          name: "IX_OrderLine_OrderID",
          table: "OrderLine",
          column: "OrderID");

      migrationBuilder.CreateIndex(
          name: "IX_Orders_CustomerId",
          table: "Orders",
          column: "CustomerId");

      migrationBuilder.CreateIndex(
          name: "IX_Orders_TimeSlotId",
          table: "Orders",
          column: "TimeSlotId",
          unique: true);

      migrationBuilder.CreateIndex(
          name: "IX_TimeSlots_CookId",
          table: "TimeSlots",
          column: "CookId");
    }

    /// <inheritdoc />
    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropTable(
          name: "Menus");

      migrationBuilder.DropTable(
          name: "OrderLine");

      migrationBuilder.DropTable(
          name: "Dishes");

      migrationBuilder.DropTable(
          name: "Orders");

      migrationBuilder.DropTable(
          name: "TimeSlots");

      migrationBuilder.DropTable(
          name: "Persons");
    }
  }
}
