﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Lekkerbek.Web.Migrations
{
    /// <inheritdoc />
    public partial class addedidentitytocook : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "IdentityUserId",
                table: "Cooks",
                type: "nvarchar(450)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_Cooks_IdentityUserId",
                table: "Cooks",
                column: "IdentityUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cooks_AspNetUsers_IdentityUserId",
                table: "Cooks",
                column: "IdentityUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cooks_AspNetUsers_IdentityUserId",
                table: "Cooks");

            migrationBuilder.DropIndex(
                name: "IX_Cooks_IdentityUserId",
                table: "Cooks");

            migrationBuilder.DropColumn(
                name: "IdentityUserId",
                table: "Cooks");
        }
    }
}
