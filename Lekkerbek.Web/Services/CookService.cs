﻿using Lekkerbek.Web.Data;
using Lekkerbek.Web.Models.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Telerik.SvgIcons;

namespace Lekkerbek.Web.Services
{
    public class CookService : ICookService
    {
        private readonly LekkerbekDbContext _context;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly ITimeSlotService _timeSlotService;
        public CookService(LekkerbekDbContext context, UserManager<IdentityUser> userManager, ITimeSlotService timeSlotService)
        {
            _context = context;
            _userManager = userManager;
            _timeSlotService = timeSlotService;
        }

        public async Task<List<Cook>> GetCooks()
        {

            var cooks = await _context.Cooks.Where(c => !c.IsDeleted).ToListAsync();
            return cooks;
        }

        public async Task<Cook> GetCook(int? id)
        {
            var cook = await _context.Cooks
                .FirstOrDefaultAsync(m => m.Id == id);
            return cook;
        }

        public async Task<Cook> CreateCook(Cook cook)
        {

            if (cook != null)
            {
                var cookUser = new IdentityUser
                {
                    Email = cook.Email,
                    UserName = cook.Email,
                    EmailConfirmed = true
                };
                var result = await _userManager.CreateAsync(cookUser, cook.Password);
                if (result.Succeeded)
                {
                    await _userManager.AddToRoleAsync(cookUser, "Cook");
                    cook.IdentityUserId = cookUser.Id;
                    _context.Add(cook);
                    await _context.SaveChangesAsync();
                    await _timeSlotService.CreateTimeSlots(DateTime.Now, cook);
                }

                return cook;
            }
            return null;

        }

        public async Task<Cook> EditCook(Cook cook)
        {
            if (cook != null)
            {
                _context.Update(cook);
                await _context.SaveChangesAsync();
                return cook;
            }
            return null;

        }

        public async Task<Cook> DeleteCook(Cook cook)
        {
            cook.IsDeleted = true;
            _context.Cooks.Update(cook);
            await _context.SaveChangesAsync();
            return cook;
        }

        public bool CookExist(int id)
        {
            return (_context.Cooks?.Any(e => e.Id == id)).GetValueOrDefault();
        }


    }
}
