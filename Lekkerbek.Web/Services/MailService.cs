﻿using Lekkerbek.Web.Models.Entities;
using System.Net.Mail;
using System.Net;

namespace Lekkerbek.Web.Services
{
    public class MailService : IMailService
    {
        public void Mail(Order order)
        {
            if (order == null)
            {
                throw new ArgumentNullException();
            }
            MailMessage message = new MailMessage();

            message.From = new MailAddress("Lekkerbekgroep5@outlook.com");
            message.To.Add(order.Customer.Email);

            string orderLines = @"<table><thead><tr><th>Product</th><th>Prijs excl.</th><th>BTW</th><th>Prijs</th><th>Aantal</th><th>Totaal</th></tr></thead><tbody>";
            foreach (OrderLine line in order.OrderLines)
            {
                orderLines += $@"<tr><td>{line.Dish.Name}</td><td>{Math.Round(line.Dish.Price / (1 + line.Dish.ProductCategory.Tax), 2)}</td><td>{(100 * line.Dish.ProductCategory.Tax)}</td><td>{line.Dish.Price}</td><td>{line.Quantity}</td><td>{line.Dish.Price * line.Quantity}</td></tr>";
            }
            orderLines += @"</tbody><tfoot><td></td><td></td><td></td><td></td>";
            decimal totPrice = order.OrderLines.Sum(ol => ol.Quantity * ol.Dish.Price);
            decimal showPrice = order.HasDiscount ? totPrice * (decimal)0.9 : totPrice;
            orderLines += order.HasDiscount ? $@"<td>Met korting</td><td>{showPrice}</td>" : $@"<td></td><td>{showPrice}</td>";
            orderLines += @"<tfoot></table>";
            message.IsBodyHtml = true;
            message.Subject = $"Faktuur {order.Id} Lekkerbek";
            message.Body = $"""
                <p>Aanmaakdatum: {order.CreationDate.Date}</p>
                <p>Klant: {order.Customer.FirstName} {order.Customer.LastName}</p>
                <p>Tijdslot: {order.TimeSlot.StartTime.TimeOfDay}</p>
                <p>{orderLines}</p>
                <p>Bijkomende opmerkingen: {order.ExtraComments}</p>
                <p>Betaalwijze: {order.PaymentProcessor}</p>
                <p>Bestelstatus: {order.OrderStatus}</p>
                """;



            SmtpClient smtpClient = new SmtpClient("smtp.office365.com", 587);

            smtpClient.Credentials = new NetworkCredential("Lekkerbekgroep5@outlook.com", "Groep5groep5");

            smtpClient.EnableSsl = true;

            try
            {
                smtpClient.Send(message);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task SendReminderMail(Order order)
        {

            if (order == null)
            {
                throw new ArgumentNullException();
            }
            MailMessage message = new MailMessage();

            message.From = new MailAddress("Lekkerbekgroep5@outlook.com");
            message.To.Add(order.Customer.Email);

            message.IsBodyHtml = true;

            message.Subject = $"Herinneringsmail Lekkerbek Bestelling {order.Id}";
            message.Body = $"""
                <p>Beste {order.Customer.FirstName},</p>
                <p>Uw bestelling is om {order.TimeSlot.EndTime.TimeOfDay} klaar</p>
                <br>
                <p>Met vriendelijke groeten,</p>
                <p>Het Lekkerbek team</p>
                """;

            SmtpClient smtpClient = new SmtpClient("smtp.office365.com", 587);

            smtpClient.Credentials = new NetworkCredential("Lekkerbekgroep5@outlook.com", "Groep5groep5");

            smtpClient.EnableSsl = true;

            try
            {
                await smtpClient.SendMailAsync(message);
            }
            catch (Exception)
            {

                throw;
            }

        }
    }
}
