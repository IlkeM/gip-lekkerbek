﻿using Lekkerbek.Web.Models.Entities;

namespace Lekkerbek.Web.Services
{
    public interface ICompanyService
    {
        Task<List<Company>> GetCompanies();

        Task<List<Company>> GetKendoCompanies();
        Task<Company> GetCompany(int? id);
        Task<Company> CreateCompany(Company company);

        Task<Company> EditCompany(Company company);

        Task<Company> DeleteCompany(Company company);

        bool CompanyExist(int id);
    }
}
