﻿using Lekkerbek.Web.Models.Entities;

namespace Lekkerbek.Web.Services
{
    public interface IMailService
    {
        void Mail(Order order);
        Task SendReminderMail(Order order);
    }
}
