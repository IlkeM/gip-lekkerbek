﻿using Lekkerbek.Web.Models.Entities;

namespace Lekkerbek.Web.Services
{
    public interface ITimeSlotService
    {
        Task<List<TimeSlot>> GetTimeSlots();

        Task<TimeSlot> DetailTimeSlot(int? id);

        Task CreateTimeSlots(DateTime date, Cook cook);
    }
}
