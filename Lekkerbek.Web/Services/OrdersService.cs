﻿using Lekkerbek.Web.Data;
using Lekkerbek.Web.Models.Entities;
using Lekkerbek.Web.ViewModels.Orders;
using Microsoft.EntityFrameworkCore;
using System.Collections;
using System.Linq;

namespace Lekkerbek.Web.Services
{
    public class OrdersService : IOrderService
    {
        private readonly LekkerbekDbContext _context;
        public OrdersService(LekkerbekDbContext context)
        {
            _context = context;
        }
        public async Task<List<Order>> GetOrders()
        {
            var orders = await _context.Orders.Include(o => o.Customer).Include(o => o.TimeSlot).ToListAsync();
            return orders;
        }

        public async Task<Tuple<Order, decimal, List<Product>, List<OrderLine>>> GetDetails(int? id)
        {
            var order = await _context.Orders
                .Include(o => o.Customer)
                .ThenInclude(c => c.Company)
                .Include(o => o.TimeSlot)
                .Include(o => o.OrderLines)
                .ThenInclude(ol => ol.Dish)
                .ThenInclude(d => d.ProductCategory)
                .FirstOrDefaultAsync(m => m.Id == id);

            decimal totalPrice = 0;

            List<Product> dishList = new List<Product>();

            List<OrderLine> orderLineList = new List<OrderLine>();

            foreach (OrderLine orderLine in order.OrderLines)
            {
                var dish = await _context.Products.FirstOrDefaultAsync(x => x.Id == orderLine.DishId);

                totalPrice += dish.Price * orderLine.Quantity;
                dishList.Add(dish);
                if (orderLine.Quantity > 0)
                {
                    orderLineList.Add(orderLine);
                }
            }

            return Tuple.Create(order, totalPrice, dishList, orderLineList);

        }

        public async Task<Tuple<List<Customer>, List<IGrouping<Cook, TimeSlot>>, List<Product>>> GetCreate()
        {

            var viewCustomers = await _context.Customers.Include(c => c.Company).ToListAsync();


            //List<object> newCustomers = new List<object>();

            //foreach (var customer in viewCustomers)
            //{
            //    newCustomers.Add(new { Id = customer.Id, ViewText = customer.ViewText });
            //}

            //SelectList selectList = new SelectList(viewCustomers, "Id", "ViewText");

            //ICollection viewCustomersCollection = viewCustomers;

            var viewTimeSlots = await _context.TimeSlots.Where(x => x.StartTime >= DateTime.Now).Include(x => x.Cook).Where(x => !x.Cook.IsDeleted).GroupBy(x => x.Cook).ToListAsync();

            //IQueryable viewProducts = _context.Products.Select(p => new { Id = p.Id, Price = p.Price, ViewText = p.Name, Quantity = 0 });
            List<Product> productList = await _context.Products.Include(p => p.ProductCategory).ToListAsync();

            //var DishIDs = productList.Where(d => d.ProductCategoryId == 1).Select(p => new { Id = p.Id, Price = p.Price, ViewText = p.Name, Quantity = 0 }).ToList();

            //List<object> newDishIDs = new List<object>();

            //foreach (var dish in DishIDs)
            //{
            //    newDishIDs.Add(new { dish.Id, dish.Price, dish.ViewText, dish.Quantity });
            //}

            //IEnumerable viewDishIDsCollection = DishIDs.ToList();

            //var NonAlcholicIDs = productList.Where(d => d.ProductCategoryId == 2).Select(p => new { Id = p.Id, Price = p.Price, ViewText = p.Name, Quantity = 0 }).ToList();

            //List<object> newNonAlcholicIDs = new List<object>();

            //foreach (var nonAlcholic in NonAlcholicIDs)
            //{
            //    newNonAlcholicIDs.Add(nonAlcholic);
            //}

            //IEnumerable viewNonAlcholicIDsCollection = NonAlcholicIDs.ToList();

            //var AlcoholicIDs = productList.Where(d => d.ProductCategoryId == 3).Select(p => new { Id = p.Id, Price = p.Price, ViewText = p.Name, Quantity = 0 }).ToList();

            //List<object> newAlcoholicIDs = new List<object>();

            //foreach (var alcoholic in AlcoholicIDs)
            //{
            //    newAlcoholicIDs.Add(alcoholic);
            //}

            //IEnumerable viewAlcoholicIDsCollection = AlcoholicIDs.ToList();

            return Tuple.Create(viewCustomers, viewTimeSlots, productList);
        }

        public void AddOrder(Order order, int TimeSlotId, int[] productIds, int[] quantities)
        {

            order.CreationDate = DateTime.Now;

            TimeSlot timeSlot = _context.TimeSlots.FirstOrDefault(x => x.Id == TimeSlotId);

            timeSlot.IsOccupied = true;
            _context.Update(timeSlot);
            order.TimeSlotId = TimeSlotId;

            List<OrderLine> orderLines = new List<OrderLine>();
            int index = 0;
            foreach (int id in productIds)
            {
                int quantity = quantities[index];
                if (quantity > 0)
                {
                    orderLines.Add(new OrderLine() { DishId = id, Quantity = quantity });
                }
                index++;
            }
            order.OrderLines = orderLines;
            _context.Add(order);
            _context.SaveChanges();
        }

        public async Task<Tuple<List<Customer>, List<IGrouping<Cook, TimeSlot>>, List<Product>>> GetCreateException(int[] productIds, int[] quantities)
        {
            var viewCustomers = await _context.Customers.ToListAsync();

            //SelectList selectList = new SelectList(viewCustomers, "Id", "ViewText");

            //ICollection selectListCollection = viewCustomers.ToList();

            var viewTimeSlots = await _context.TimeSlots.Where(x => x.StartTime > DateTime.Now).Include(x => x.Cook).Where(x => !x.Cook.IsDeleted).GroupBy(x => x.Cook).ToListAsync();

            //List<object> viewProducts = new List<object>();
            //int counter = 0;

            //foreach (int id in productIds)
            //{
            //    int quantity = quantities[counter];
            //    viewProducts.Add(new { Id = id, ViewText = _context.Products.First(d => d.Id == id).Name, Quantity = quantity });
            //    counter++;
            //}

            List<Product> productList = new List<Product>();
            int counter = 0;
            foreach (int id in productIds)
            {
                int quantity = quantities[counter];
                productList.Add(_context.Products.First(d => d.Id == id));
                counter++;
            }
            return Tuple.Create(viewCustomers, viewTimeSlots, productList);

        }

        public async Task<Tuple<Order, List<Customer>, List<IGrouping<Cook, TimeSlot>>, List<Product>>> GetEdit(int? id)
        {
            var order = await _context.Orders.Include(x => x.OrderLines).ThenInclude(x => x.Dish).FirstOrDefaultAsync(x => x.Id == id);


            //SelectList selectList = new SelectList(viewCustomers, "Id", "ViewText", order.CustomerId);

            var customers = await _context.Customers.ToListAsync();


            var viewTimeSlots = await _context.TimeSlots.Where(x => x.StartTime >= DateTime.Now).Include(x => x.Cook).Where(x => !x.Cook.IsDeleted).GroupBy(x => x.Cook).ToListAsync();

            //foreach (var vp in _context.Products)
            //{
            //    var ol = order.OrderLines.FirstOrDefault(x => x.DishId == vp.Id);
            //    int quantity = ol != null ? ol.Quantity : 0;
            //    viewProducts.Add(new { Id = vp.Id, Price = vp.Price, ViewText = vp.Name, Quantity = quantity });
            //}

            var products = await _context.Products.ToListAsync();

            return Tuple.Create(order, customers, viewTimeSlots, products);
        }

        public async Task<Tuple<List<Customer>, List<IGrouping<Cook, TimeSlot>>, List<Product>>> CheckTimeSlotEdit(int id, int[] productIds, int[] quantities)
        {
            TimeSlot timeslot = _context.TimeSlots.Find(_context.Orders.First(o => o.Id == id).TimeSlotId);
            TimeSpan ts = timeslot.StartTime - DateTime.Now;
            if (Math.Floor(ts.TotalMinutes) < 60)
            {

                //var orders = await _context.Orders.Include(x => x.OrderLines).ThenInclude(x => x.Dish);
                var fetchedOrder = await _context.Orders.Include(x => x.OrderLines).ThenInclude(x => x.Dish).FirstOrDefaultAsync(x => x.Id == id);
                var viewCustomers = await _context.Customers.ToListAsync();
                //SelectList selectList = new SelectList(viewCustomers, "Id", "ViewText", fetchedOrder.CustomerId);
                //ICollection selectListCollection = viewCustomers.ToList();

                var viewTimeSlots = _context.TimeSlots.Where(x => x.StartTime >= DateTime.Now).Include(x => x.Cook).Where(x => !x.Cook.IsDeleted).GroupBy(x => x.Cook).ToList();

                Console.WriteLine("edit");

                var products = await _context.Products.ToListAsync();

                //List<object> viewProducts = new List<object>();
                //int counter = 0;
                //foreach (int pid in productIds)
                //{

                //    int quantity = quantities[counter];
                //    Console.WriteLine(quantity);
                //    Console.WriteLine("list");
                //    viewProducts.Add(new { Id = pid, ViewText = _context.Products.First(d => d.Id == pid).Name, Quantity = quantity });
                //    counter++;
                //}
                //viewProducts.ForEach(x => Console.WriteLine("line" + x.ToString()));
                return Tuple.Create(viewCustomers, viewTimeSlots, products);
            }
            return null;
        }

        public async void ChangeTimeslotEdit(Order order, int oldTimeSlotId, int TimeSlotId)
        {
            // zet oude timeslot op false
            TimeSlot oldTimeSlot = _context.TimeSlots.FirstOrDefault(x => x.Id == oldTimeSlotId);
            oldTimeSlot.IsOccupied = false;

            // zet nieuwe timeslot op true
            TimeSlot newTimeSlot = _context.TimeSlots.FirstOrDefault(x => x.Id == TimeSlotId);
            newTimeSlot.IsOccupied = true;

            order.TimeSlot = newTimeSlot;

            _context.Update(oldTimeSlot);
            _context.Update(newTimeSlot);
        }

        public async void UpdateOrRemoveOrderLines(int id, int[] productIds, int[] quantities)
        {
            foreach (OrderLine ol in _context.OrderLines.Where(ol => ol.OrderID == id))
            {
                if (productIds.Contains(ol.DishId))
                {
                    int location = Array.IndexOf(productIds, ol.DishId);
                    ol.Quantity = quantities[location];
                    if (ol.Quantity > 0)
                    {
                        _context.Update(ol);
                    }
                    else
                    {
                        _context.Remove(ol);
                    }

                }
            }
        }

        public async void AddNewOrderLines(int id, int[] productIds, int[] quantities)
        {
            List<OrderLine> newOrderLines = new List<OrderLine>();
            int index = 0;
            foreach (int pid in productIds)
            {
                // als de orderlines van de huidige order, de product id niet bevatten
                if (!_context.OrderLines.Where(ol => ol.OrderID == id).Select(ol => ol.DishId).Contains(pid))
                {
                    int quantity = quantities[index];
                    if (quantity > 0)
                    {
                        newOrderLines.Add(new OrderLine() { OrderID = id, DishId = pid, Quantity = quantity });
                    }
                }
                index++;
            }
            _context.AddRange(newOrderLines);
        }

        public async void UpdateOrder(Order order)
        {
            _context.Update(order);

            _context.SaveChanges();
        }

        public async Task<Tuple<List<Customer>, List<IGrouping<Cook, TimeSlot>>, List<Product>>> EditException(int id, int[] productIds, int[] quantities)
        {
            //var orders = _context.Orders.Include(x => x.OrderLines).ThenInclude(x => x.Dish);
            var fetchedOrder = await _context.Orders.Include(x => x.OrderLines).ThenInclude(x => x.Dish).FirstOrDefaultAsync(x => x.Id == id);
            var viewCustomers = await _context.Customers.ToListAsync();
            //SelectList selectList = new SelectList(viewCustomers, "Id", "ViewText", fetchedOrder.CustomerId);
            //ICollection selectListCollection = viewCustomers.ToList();

            var viewTimeSlots = _context.TimeSlots.Where(x => x.StartTime >= DateTime.Now).Include(x => x.Cook).GroupBy(x => x.Cook).ToList();

            Console.WriteLine("edit");

            var products = await _context.Products.ToListAsync();

            //List<object> viewProducts = new List<object>();
            //int counter = 0;
            //foreach (int pid in productIds)
            //{

            //    int quantity = quantities[counter];
            //    Console.WriteLine(quantity);
            //    Console.WriteLine("list");
            //    viewProducts.Add(new { Id = pid, ViewText = _context.Products.First(d => d.Id == pid).Name, Quantity = quantity });
            //    counter++;
            //}
            //viewProducts.ForEach(x => Console.WriteLine("line" + x.ToString()));
            return Tuple.Create(viewCustomers, viewTimeSlots, products);
        }

        public async Task<Order> GetOrder(int? id)
        {
            var order = await _context.Orders
                .Include(o => o.Customer)
                .Include(o => o.TimeSlot)
                .FirstOrDefaultAsync(m => m.Id == id);

            if (order != null)
            {
                return order;
            }
            return null;
        }
        public async Task<Order> GetOrderWithOrderLines(int? id)
        {
            Order? order;
            try
            {
                order = await _context.Orders
                    .Include(o => o.Customer)
                    .Include(o => o.TimeSlot)
                    .Include(o => o.OrderLines)
                    .ThenInclude(ol => ol.Dish)
                    .ThenInclude(d => d.ProductCategory)
                    .FirstOrDefaultAsync(m => m.Id == id);
            }
            catch (Exception)
            {

                throw;
            }
            if (order == null)
            {
                throw new KeyNotFoundException();
            }
            return order;

        }
        public async Task<Order> DeleteOrder(Order order)
        {
            var timeslot = await _context.TimeSlots.FindAsync(order.TimeSlotId);

            TimeSpan ts = timeslot.StartTime - DateTime.Now;

            if (order != null && (Math.Floor(ts.TotalMinutes) > 120))
            {
                timeslot.IsOccupied = false;
                _context.Orders.Remove(order);
                await _context.SaveChangesAsync();
                return order;
            }
            return null;
        }

        public bool OrderExist(int id)
        {
            return (_context.Orders?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
