﻿using Lekkerbek.Web.Data;
using Lekkerbek.Web.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace Lekkerbek.Web.Services
{
    public class CompanyService : ICompanyService
    {

        private readonly LekkerbekDbContext _context;
        public CompanyService(LekkerbekDbContext context)
        {
            _context = context;
        }

        public async Task<Company> CreateCompany(Company company)
        {
            if (company == null)
            {
                throw new ArgumentNullException();
            }

            _context.Add(company);

            try
            {
                await _context.SaveChangesAsync();
                return company;
            }
            catch (Exception)
            {

                throw;
            }
        }


        public async Task<List<Company>> GetCompanies()
        {
            return await _context.Companies.Include(c => c.ContactPerson).ToListAsync();
        }

        public async Task<List<Company>> GetKendoCompanies()
        {
            return await _context.Companies.ToListAsync();
        }

        public async Task<Company> GetCompany(int? id)
        {
            Company? company;
            try
            {
                company = await _context.Companies.Include(c => c.ContactPerson).FirstOrDefaultAsync(m => m.Id == id);
            }
            catch (Exception)
            {

                throw;
            }

            if (company == null)
            {
                throw new KeyNotFoundException();
            }

            return company;
        }

        public async Task<Company> EditCompany(Company company)
        {
            _context.Update(company);
            await _context.SaveChangesAsync();
            return company;
        }

        public async Task<Company> DeleteCompany(Company company)
        {
            if (company != null)
            {
                _context.Companies.Remove(company);
                await _context.SaveChangesAsync();
                return company;
            }
            return null;

        }

        public bool CompanyExist(int id)
        {
            return (_context.Companies?.Any(e => e.Id == id)).GetValueOrDefault();
        }


    }
}
