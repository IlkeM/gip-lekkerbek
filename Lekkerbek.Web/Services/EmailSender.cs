﻿using Microsoft.AspNetCore.Identity.UI.Services;
using System.Net.Mail;
using System.Net;

namespace Lekkerbek.Web.Services
{
    public class EmailSender : IEmailSender
    {


        public Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            MailMessage message = new MailMessage();

            message.From = new MailAddress("Lekkerbekgroep5@outlook.com");

            SmtpClient smtpClient = new SmtpClient("smtp.office365.com", 587);

            smtpClient.Credentials = new NetworkCredential("Lekkerbekgroep5@outlook.com", "Groep5groep5");

            smtpClient.EnableSsl = true;


            return smtpClient.SendMailAsync(
                new MailMessage("Lekkerbekgroep5@outlook.com", email, subject, htmlMessage) { IsBodyHtml = true }
            );

        }
    }
}
