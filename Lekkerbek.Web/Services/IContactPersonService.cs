﻿using Lekkerbek.Web.Models.Entities;

namespace Lekkerbek.Web.Services
{
    public interface IContactPersonService
    {
        Task CreateContactPerson(Customer customer);
        Task<Customer> GetContactPerson(int? id);
        Task<List<Customer>> GetContactPersons();
    }
}
