﻿using Lekkerbek.Web.Data;
using Lekkerbek.Web.Models.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Telerik.SvgIcons;

namespace Lekkerbek.Web.Services
{
    public class ClerkService : IClerkService
    {
        private readonly LekkerbekDbContext _context;
        private readonly UserManager<IdentityUser> _userManager;

        public ClerkService(UserManager<IdentityUser> userManager, LekkerbekDbContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        public async Task<Clerk> GetClerk(int? id)
        {
            var clerk = await _context.Clerks.FirstOrDefaultAsync(m => m.Id == id);
            return clerk;
        }

        public async Task<List<Clerk>> GetClerks()
        {
            var clerks = await _context.Clerks.ToListAsync();
            return clerks;
        }

        public async Task<Clerk> CreateClerk(Clerk clerk)
        {
            if (clerk != null)
            {
                var clerkUser = new IdentityUser
                {
                    Email = clerk.Email,
                    UserName = clerk.Email,
                    EmailConfirmed = true
                };
                var result = await _userManager.CreateAsync(clerkUser, clerk.Password);
                if (result.Succeeded)
                {
                    await _userManager.AddToRoleAsync(clerkUser, "Clerk");
                    clerk.IdentityUserId = clerkUser.Id;
                    _context.Add(clerk);
                    await _context.SaveChangesAsync();
                }
                return clerk;
            }
            return null;
        }

        public async Task<Clerk> EditClerk(Clerk clerk)
        {
            if (clerk != null)
            {
                _context.Update(clerk);
                await _context.SaveChangesAsync();
                return clerk;
            }
            return null;
        }

        public async Task<Clerk> DeleteClerk(Clerk clerk)
        {
            _context.Clerks.Remove(clerk);
            await _context.SaveChangesAsync();
            return clerk;
        }

        public bool ClerkExists(int id)
        {
            return (_context.Clerks?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
