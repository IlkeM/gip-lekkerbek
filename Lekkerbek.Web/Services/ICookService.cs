﻿using Lekkerbek.Web.Models.Entities;

namespace Lekkerbek.Web.Services
{
    public interface ICookService
    {
        Task<List<Cook>> GetCooks();

        Task<Cook> GetCook(int? id);

        Task<Cook> CreateCook(Cook cook);

        Task<Cook> EditCook(Cook cook);

        Task<Cook> DeleteCook(Cook cook);

        bool CookExist(int id);

    }
}
