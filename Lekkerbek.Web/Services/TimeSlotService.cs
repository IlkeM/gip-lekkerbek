﻿using Lekkerbek.Web.Data;
using Lekkerbek.Web.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace Lekkerbek.Web.Services
{
    public class TimeSlotService : ITimeSlotService
    {
        private readonly LekkerbekDbContext _context;
        public TimeSlotService(LekkerbekDbContext context)
        {
            _context = context;
        }

        public async Task<List<TimeSlot>> GetTimeSlots()
        {
            var timeSlots = await _context.TimeSlots.Where(x => x.StartTime >= DateTime.Now).Include(x => x.Order).Include(t => t.Cook).ToListAsync();
            return timeSlots;
        }

        public async Task<TimeSlot> DetailTimeSlot(int? id)
        {
            var timeSlot = await _context.TimeSlots.Where(x => x.StartTime >= DateTime.Now).Where(x => x.Order.TimeSlotId == x.Id).Include(x => x.Order).Include(t => t.Cook).FirstOrDefaultAsync(x => x.Id == id);
            return timeSlot;
        }
        public async Task CreateTimeSlots(DateTime date, Cook cook)
        {
            if (cook == null) throw new ArgumentNullException("argument is null");
            date = date.Date;
            DateTime endDate = date + TimeSpan.FromDays(14);
            List<TimeSlot> slots = new List<TimeSlot>();

            for (DateTime dt = date; dt <= endDate; dt = dt.AddDays(1))
            {
                dt = dt.Date;
                DateTime firstshift = dt.AddHours(12);
                DateTime secondshift = dt.AddHours(18);

                for (int i = 0; i < 8; i++)
                {
                    slots.Add(new TimeSlot() { CookId = cook.Id, Date = date, StartTime = firstshift.AddMinutes(15 * i) });
                }

                for (int i = 0; i < 20; i++)
                {
                    slots.Add(new TimeSlot() { CookId = cook.Id, Date = date, StartTime = secondshift.AddMinutes(15 * i) });
                }
            }



            try
            {
                await _context.AddRangeAsync(slots);
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {

                throw;
            }

        }
    }
}
