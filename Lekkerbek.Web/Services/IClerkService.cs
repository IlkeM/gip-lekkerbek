﻿using Lekkerbek.Web.Models.Entities;

namespace Lekkerbek.Web.Services
{
    public interface IClerkService
    {
        Task<List<Clerk>> GetClerks();

        Task<Clerk> GetClerk(int? id);

        Task<Clerk> CreateClerk(Clerk clerk);

        Task<Clerk> EditClerk(Clerk clerk);

        Task<Clerk> DeleteClerk(Clerk clerk);

        bool ClerkExists(int id);
    }
}
