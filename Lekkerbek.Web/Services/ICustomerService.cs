﻿using Lekkerbek.Web.Models.Entities;
namespace Lekkerbek.Web.Services
{
    public interface ICustomerService
    {
        Task<List<Customer>> GetCustomers();

        Task<Customer> GetCustomer(int? id);
        Task<Customer> GetCustomerWithOrders(int? id);

        Task<Customer> CreateCustomer(Customer customer);

        Task<Customer> EditCustomer(Customer customer);

        Task<Customer> DeleteCustomer(int id);

        bool CustomerExist(int id);
    }
}
