﻿using Lekkerbek.Web.Data;
using Lekkerbek.Web.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace Lekkerbek.Web.Services
{
    public class CustomersService : ICustomerService, IContactPersonService
    {
        private readonly LekkerbekDbContext _context;
        public CustomersService(LekkerbekDbContext context)
        {
            _context = context;
        }

        public async Task<Customer> GetCustomer(int? id)
        {
            var customer = await _context.Customers.FirstOrDefaultAsync(m => m.Id == id);
            if (customer == null)
            {
                throw new KeyNotFoundException(nameof(customer));
            }
            return customer;
        }

        public async Task<Customer> GetContactPerson(int? id)
        {
            Customer contactPerson;
            try
            {
                contactPerson = await GetCustomer(id);
            }
            catch (Exception)
            {
                throw;
            }
            if (contactPerson.IsProfessional)
            {
                return contactPerson;
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }

        public async Task<List<Customer>> GetCustomers()
        {
            var customers = await _context.Customers.ToListAsync();
            return customers;

        }

        public async Task<List<Customer>> GetContactPersons()
        {
            try
            {
                return await _context.Customers.Include(c => c.Company).Where(c => c.IsProfessional).ToListAsync();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<Customer> GetCustomerWithOrders(int? id)
        {
            Customer? customer;
            try
            {
                customer = await _context.Customers.Include(c => c.Orders).FirstOrDefaultAsync(c => c.Id == id);
            }
            catch (Exception)
            {

                throw;
            }

            if (customer == null) { throw new KeyNotFoundException(); }
            return customer;

        }

        public async Task<Customer> CreateCustomer(Customer customer)
        {

            if (customer != null)
            {
                _context.Customers.Add(customer);
                await _context.SaveChangesAsync();
                return customer;
            }

            return null;

        }

        public async Task CreateContactPerson(Customer customer)
        {
            if (customer == null)
            {
                throw new ArgumentNullException();
            }
            customer.IsProfessional = true;
            try
            {
                await CreateCustomer(customer);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<Customer> DeleteCustomer(int id)
        {
            var customer = await _context.Customers.FirstOrDefaultAsync(m => m.Id == id);

            if (customer != null)
            {
                _context.Customers.Remove(customer);
                await _context.SaveChangesAsync();
                return customer;
            }

            return null;

        }

        public async Task<Customer> EditCustomer(Customer customer)
        {

            if (customer != null)
            {
                _context.Update(customer);
                await _context.SaveChangesAsync();
                return customer;
            }

            return null;

        }

        public bool CustomerExist(int id)
        {
            return (_context.Customers?.Any(e => e.Id == id)).GetValueOrDefault();
        }


    }
}
