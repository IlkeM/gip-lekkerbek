﻿using Lekkerbek.Web.Models.Entities;
using Lekkerbek.Web.ViewModels.Orders;
using System.Collections;

namespace Lekkerbek.Web.Services
{
    public interface IOrderService
    {
        Task<List<Order>> GetOrders();

        Task<Tuple<Order, decimal, List<Product>, List<OrderLine>>> GetDetails(int? id);

        Task<Tuple<List<Customer>, List<IGrouping<Cook, TimeSlot>>, List<Product>>> GetCreate();

        void AddOrder(Order order, int TimeSlotId, int[] productIds, int[] quantities);

        Task<Tuple<List<Customer>, List<IGrouping<Cook, TimeSlot>>, List<Product>>> GetCreateException(int[] productIds, int[] quantities);


        Task<Tuple<Order, List<Customer>, List<IGrouping<Cook, TimeSlot>>, List<Product>>> GetEdit(int? id);

        Task<Tuple<List<Customer>, List<IGrouping<Cook, TimeSlot>>, List<Product>>> CheckTimeSlotEdit(int id, int[] productIds, int[] quantities);

        void ChangeTimeslotEdit(Order order, int oldTimeSlotId, int TimeSlotId);

        void UpdateOrRemoveOrderLines(int id, int[] productIds, int[] quantities);

        void AddNewOrderLines(int id, int[] productIds, int[] quantities);

        void UpdateOrder(Order order);

        Task<Tuple<List<Customer>, List<IGrouping<Cook, TimeSlot>>, List<Product>>> EditException(int id, int[] productIds, int[] quantities);

        Task<Order> GetOrder(int? id);
        Task<Order> GetOrderWithOrderLines(int? id);

        Task<Order> DeleteOrder(Order order);
        bool OrderExist(int id);

    }
}
