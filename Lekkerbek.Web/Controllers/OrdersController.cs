﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Lekkerbek.Web.Data;
using System.Collections;
using Lekkerbek.Web.Models.Entities;
using Microsoft.CodeAnalysis;
using System.Net.Mail;
using System.Net;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Lekkerbek.Web.Models.Enums;
using Lekkerbek.Web.Services;
using Lekkerbek.Web.Models;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using static NuGet.Packaging.PackagingConstants;
using Lekkerbek.Web.ViewModels.Orders;
using Lekkerbek.Web.ViewModels.Customers;
using Lekkerbek.Web.ViewModels.Products;
using Telerik.SvgIcons;
using Lekkerbek.Web.ViewModels.OrderLines;

namespace Lekkerbek.Web.Controllers
{
    [Authorize(Roles = "Customer, Clerk, Super, Administrator, Cook")]
    public class OrdersController : Controller
    {
        private readonly IOrderService _orderService;
        private readonly ICustomerService _customerService;
        private readonly IMailService _mailService;

        public OrdersController(IOrderService orderService, ICustomerService customerService, IMailService mailService)
        {
            _orderService = orderService;
            _customerService = customerService;
            _mailService = mailService;
        }


        // GET: Orders
        [Authorize(Roles = "Customer, Clerk, Super, Administrator")]
        public async Task<IActionResult> Index()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            var kendolekkerbekDbContextOrders = await _orderService.GetOrders();

            List<ViewModels.Orders.IndexViewModel> ordersVMList = new List<ViewModels.Orders.IndexViewModel>();
            foreach (var item in kendolekkerbekDbContextOrders)
            {
                if (User.IsInRole("Administrator") || User.IsInRole("Clerk") || User.IsInRole("Super"))
                {
                    bool canBeDeleted = false;
                    bool canBeEdited = false;
                    TimeSpan ts = item.TimeSlot.StartTime - DateTime.Now;
                    if (Math.Floor(ts.TotalMinutes) > 60)
                    {
                        canBeEdited = true;
                    }
                    if (Math.Floor(ts.TotalMinutes) > 120)
                    {
                        canBeDeleted = true;
                    }

                    ViewModels.Orders.IndexViewModel orderVM = new ViewModels.Orders.IndexViewModel
                    {
                        Id = item.Id,
                        CreationDate = item.CreationDate.ToString("dd/MM/yyyy HH:mm"),
                        ExtraComments = item.ExtraComments,
                        TimeSlotInfo = item.TimeSlot.StartTime.ToString("dd/MM/yyyy HH:mm") + " - " + item.TimeSlot.EndTime.ToString("HH:mm"),
                        Name = item.Customer.FirstName + " " + item.Customer.LastName,
                        PaymentProcessor = item.PaymentProcessor,
                        OrderStatus = item.OrderStatus,
                        HasDiscount = item.HasDiscount,
                        EditPossible = canBeEdited,
                        DeletePossible = canBeDeleted
                    };
                    ordersVMList.Add(orderVM);
                }
                else
                {
                    if (item.Customer.IdentityUserId == userId)
                    {
                        bool canBeDeleted = false;
                        bool canBeEdited = false;
                        TimeSpan ts = item.TimeSlot.StartTime - DateTime.Now;
                        if (Math.Floor(ts.TotalMinutes) > 60)
                        {
                            canBeEdited = true;
                        }
                        if (Math.Floor(ts.TotalMinutes) > 120)
                        {
                            canBeDeleted = true;
                        }

                        ViewModels.Orders.IndexViewModel orderVM = new ViewModels.Orders.IndexViewModel
                        {
                            Id = item.Id,
                            CreationDate = item.CreationDate.ToString("dd/MM/yyyy HH:mm"),
                            ExtraComments = item.ExtraComments,
                            TimeSlotInfo = item.TimeSlot.StartTime.ToString("dd/MM/yyyy HH:mm") + " - " + item.TimeSlot.EndTime.ToString("HH:mm"),
                            Name = item.Customer.FirstName + " " + item.Customer.LastName,
                            PaymentProcessor = item.PaymentProcessor,
                            OrderStatus = item.OrderStatus,
                            HasDiscount = item.HasDiscount,
                            EditPossible = canBeEdited,
                            DeletePossible = canBeDeleted,
                            IdentityUserId = userId,
                        };
                        ordersVMList.Add(orderVM);
                    }
                }
            }
            return View(ordersVMList);
        }

        // Kendo Table Orders Index
        [Authorize(Roles = "Customer, Clerk, Super, Administrator")]
        public async Task<IActionResult> GetOrders([DataSourceRequest] DataSourceRequest request)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

            var kendolekkerbekDbContextOrders = await _orderService.GetOrders();

            List<ViewModels.Orders.IndexViewModel> ordersVMList = new List<ViewModels.Orders.IndexViewModel>();

            foreach (var item in kendolekkerbekDbContextOrders)
            {
                if (User.IsInRole("Administrator") || User.IsInRole("Clerk") || User.IsInRole("Super"))
                {
                    bool canBeDeleted = false;
                    bool canBeEdited = false;
                    TimeSpan ts = item.TimeSlot.StartTime - DateTime.Now;
                    if (Math.Floor(ts.TotalMinutes) > 60)
                    {
                        canBeEdited = true;
                    }
                    if (Math.Floor(ts.TotalMinutes) > 120)
                    {
                        canBeDeleted = true;
                    }

                    ViewModels.Orders.IndexViewModel orderVM = new ViewModels.Orders.IndexViewModel
                    {
                        Id = item.Id,
                        CreationDate = item.CreationDate.ToString("dd/MM/yyyy HH:mm"),
                        ExtraComments = item.ExtraComments,
                        TimeSlotInfo = item.TimeSlot.StartTime.ToString("dd/MM/yyyy HH:mm") + " - " + item.TimeSlot.EndTime.ToString("HH:mm"),
                        Name = item.Customer.FirstName + " " + item.Customer.LastName,
                        PaymentProcessor = item.PaymentProcessor,
                        OrderStatus = item.OrderStatus,
                        HasDiscount = item.HasDiscount,
                        EditPossible = canBeEdited,
                        DeletePossible = canBeDeleted
                    };
                    ordersVMList.Add(orderVM);
                }
                else
                {
                    if (item.Customer.IdentityUserId == userId)
                    {
                        bool canBeDeleted = false;
                        bool canBeEdited = false;
                        TimeSpan ts = item.TimeSlot.StartTime - DateTime.Now;
                        if (Math.Floor(ts.TotalMinutes) > 60)
                        {
                            canBeEdited = true;
                        }
                        if (Math.Floor(ts.TotalMinutes) > 120)
                        {
                            canBeDeleted = true;
                        }

                        ViewModels.Orders.IndexViewModel orderVM = new ViewModels.Orders.IndexViewModel
                        {
                            Id = item.Id,
                            CreationDate = item.CreationDate.ToString("dd/MM/yyyy HH:mm"),
                            ExtraComments = item.ExtraComments,
                            TimeSlotInfo = item.TimeSlot.StartTime.ToString("dd/MM/yyyy HH:mm") + " - " + item.TimeSlot.EndTime.ToString("HH:mm"),
                            Name = item.Customer.FirstName + " " + item.Customer.LastName,
                            PaymentProcessor = item.PaymentProcessor,
                            OrderStatus = item.OrderStatus,
                            HasDiscount = item.HasDiscount,
                            EditPossible = canBeEdited,
                            DeletePossible = canBeDeleted
                        };
                        ordersVMList.Add(orderVM);
                    }
                }
            }
            JsonResult result = Json(ordersVMList.ToDataSourceResult(request));
            return result;
        }


        // GET: Orders/Details/5
        [Authorize(Roles = "Customer, Clerk, Super, Administrator, Cook")]
        public async Task<IActionResult> Details(int? id)
        {
            var tuple = await _orderService.GetDetails(id);

            if (id == null || await _orderService.GetOrders() == null)
            {
                return NotFound();
            }

            if (tuple == null)
            {
                return NotFound();
            }
            bool canBeEdited = false;
            TimeSpan ts = tuple.Item1.TimeSlot.StartTime - DateTime.Now;
            if (Math.Floor(ts.TotalMinutes) > 60)
            {
                canBeEdited = true;
            }
            List<OrderLineViewModel> orderLinelist = new List<OrderLineViewModel>();
            foreach (OrderLine o in tuple.Item4)
            {
                ViewModels.OrderLines.OrderLineViewModel orderVM = new ViewModels.OrderLines.OrderLineViewModel
                {
                    DishName = o.Dish.Name,
                    Price = o.Dish.Price,
                    Tax = o.Dish.ProductCategory.Tax,
                    Quantity = o.Quantity
                };
                orderLinelist.Add(orderVM);
            };
            ViewModels.Orders.DetailsViewModel detailVM = new ViewModels.Orders.DetailsViewModel
            {
                Id = tuple.Item1.Id,
                CreationDate = tuple.Item1.CreationDate,
                ExtraComments = tuple.Item1.ExtraComments,
                StartTime = tuple.Item1.TimeSlot.StartTime,
                Name = tuple.Item1.Customer.FirstName + " " + tuple.Item1.Customer.LastName,
                PaymentProcessor = tuple.Item1.PaymentProcessor,
                HasDiscount = tuple.Item1.HasDiscount,
                OrderStatus = tuple.Item1.OrderStatus,
                Order = tuple.Item1,
                TotalPrice = tuple.Item2,
                //DishList = tuple.Item3,
                OrderLineList = orderLinelist,
                CanBeEdited = canBeEdited,
                IsProfessional = tuple.Item1.Customer.IsProfessional,
            };

            if (detailVM.IsProfessional)
            {
                detailVM.CompanyName = tuple.Item1.Customer.Company.Name;
                detailVM.VatNumber = tuple.Item1.Customer.Company.VatNumber;
            }


            //Order order = tuple.Item1;
            //decimal totalPrice = tuple.Item2;
            //List<Product> dishList = tuple.Item3;
            //List<OrderLine> orderLineList = tuple.Item4;

            //ViewData["totalPrice"] = totalPrice;
            //ViewData["dishList"] = dishList;
            //ViewData["orderLineList"] = orderLineList;

            return View(detailVM);
        }

        // GET: Orders/Create
        [Authorize(Roles = "Customer, Clerk, Super, Administrator")]
        public async Task<IActionResult> Create()
        {

            var tuple = await _orderService.GetCreate();

            //SelectList selectList = new SelectList(tuple.Item1, "Id", "ViewText");
            List<CustomerViewModel> customersIdList = new List<CustomerViewModel>();

            if (User.IsInRole("Administrator") || User.IsInRole("Clerk") || User.IsInRole("Super"))
            {
                foreach (Customer c in tuple.Item1)
                {
                    CustomerViewModel customerVM = new CustomerViewModel
                    {
                        Id = c.Id,
                        CustomerName = $"{c.FullName}{(c.IsProfessional ? " - " + c.Company.Name : "")}"
                    };

                    customersIdList.Add(customerVM);
                }
            }
            else
            {
                var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

                foreach (Customer c in tuple.Item1)
                {
                    if (c.IdentityUserId == userId)
                    {
                        CustomerViewModel customerVM = new CustomerViewModel
                        {
                            Id = c.Id,
                            CustomerName = $"{c.FullName}{(c.IsProfessional ? " - " + c.Company.Name : "")}"
                        };
                        customersIdList.Add(customerVM);
                    }
                }
            }

            //foreach (Customer c in tuple.Item1)
            //{
            //    CustomerViewModel customerVM = new CustomerViewModel
            //    {
            //        Id = c.Id,
            //        CustomerName = c.FullName
            //    };
            //    customersIdList.Add(customerVM);
            //}

            List<ProductViewModel> DishIdList = new List<ProductViewModel>();
            List<ProductViewModel> NonAlcoholicIdList = new List<ProductViewModel>();
            List<ProductViewModel> AlcoholicIdList = new List<ProductViewModel>();
            foreach (Product p in tuple.Item3)
            {
                ProductViewModel productVM = new ProductViewModel
                {
                    Id = p.Id,
                    Price = p.Price,
                    Name = p.Name,
                    Description = p.Description,
                    Quantity = 0
                };
                int productCat = p.ProductCategoryId;
                switch (productCat)
                {
                    case 1:
                        DishIdList.Add(productVM);
                        break;
                    case 2:
                        NonAlcoholicIdList.Add(productVM);
                        break;
                    case 3:
                        AlcoholicIdList.Add(productVM);
                        break;
                    default:
                        break;
                }
            }

            CreateViewModel createVM = new CreateViewModel
            {
                ViewCustomers = tuple.Item1,
                SelectListCreate = customersIdList,
                ViewTimeSlots = tuple.Item2,
                DishIdList = DishIdList,
                NonAlcoholicIdList = NonAlcoholicIdList,
                AlcoholicIdList = AlcoholicIdList
                //new SelectList(customersIdList, "Id", "CustomerName"),
            };
            //foreach (Customer item in createVM.ViewCustomers)
            //{
            //    Console.WriteLine(item.);
            //}
            //ViewData["CustomerId"] = selectList;
            //ViewData["TimeSlotId"] = tuple.Item2;
            //ViewData["DishID"] = tuple.Item3.Where(d => d.ProductCategoryId == 1).Select(p => new { Id = p.Id, Price = p.Price, ViewText = p.Name, Quantity = 0 });
            //ViewData["NonAlcholicID"] = tuple.Item3.Where(d => d.ProductCategoryId == 2).Select(p => new { Id = p.Id, Price = p.Price, ViewText = p.Name, Quantity = 0 });
            //ViewData["AlcoholicID"] = tuple.Item3.Where(d => d.ProductCategoryId == 3).Select(p => new { Id = p.Id, Price = p.Price, ViewText = p.Name, Quantity = 0 });

            return View(createVM);
        }

        // POST: Orders/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Customer, Clerk, Super, Administrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ExtraComments,TimeSlotId,CustomerId,PaymentProcessor,OrderStatus")] Order order, int TimeSlotId, int[] productIds, int[] quantities)
        {

            try
            {
                _orderService.AddOrder(order, TimeSlotId, productIds, quantities);
                return RedirectToAction(nameof(Index));
            }
            catch (Exception)
            {
                var tuple = await _orderService.GetCreateException(productIds, quantities);

                List<CustomerViewModel> customersIdList = new List<CustomerViewModel>();
                if (User.IsInRole("Administrator") || User.IsInRole("Clerk") || User.IsInRole("Super"))
                {
                    foreach (Customer c in tuple.Item1)
                    {
                        CustomerViewModel customerVM = new CustomerViewModel
                        {
                            Id = c.Id,
                            CustomerName = $"{c.FullName}{(c.IsProfessional ? " - " + c.Company.Name : "")}"
                        };
                        customersIdList.Add(customerVM);
                    }
                }
                else
                {
                    var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

                    foreach (Customer c in tuple.Item1)
                    {
                        if (c.IdentityUserId == userId)
                        {
                            CustomerViewModel customerVM = new CustomerViewModel
                            {
                                Id = c.Id,
                                CustomerName = $"{c.FullName}{(c.IsProfessional ? " - " + c.Company.Name : "")}"
                            };
                            customersIdList.Add(customerVM);
                        }
                    }
                }

                List<ProductViewModel> DishIdList = new List<ProductViewModel>();
                List<ProductViewModel> NonAlcoholicIdList = new List<ProductViewModel>();
                List<ProductViewModel> AlcoholicIdList = new List<ProductViewModel>();
                foreach (Product p in tuple.Item3)
                {
                    ProductViewModel productVM = new ProductViewModel
                    {
                        Id = p.Id,
                        Price = p.Price,
                        Name = p.Name,
                        Description = p.Description,
                        Quantity = 0
                    };
                    int productCat = p.ProductCategoryId;
                    switch (productCat)
                    {
                        case 1:
                            DishIdList.Add(productVM);
                            break;
                        case 2:
                            NonAlcoholicIdList.Add(productVM);
                            break;
                        case 3:
                            AlcoholicIdList.Add(productVM);
                            break;
                        default:
                            break;
                    }
                }

                CreateViewModel createVM = new CreateViewModel
                {
                    ViewCustomers = tuple.Item1,
                    SelectListCreate = customersIdList,
                    ViewTimeSlots = tuple.Item2,
                    DishIdList = DishIdList,
                    NonAlcoholicIdList = NonAlcoholicIdList,
                    AlcoholicIdList = AlcoholicIdList
                };


                //SelectList selectList = new SelectList(tuple.Item1, "Id", "ViewText");

                //ViewData["CustomerId"] = selectList;
                //ViewData["TimeSlotId"] = tuple.Item2;
                //ViewData["DishID"] = tuple.Item3.Where(d => d.ProductCategoryId == 1).Select(p => new { Id = p.Id, Price = p.Price, ViewText = p.Name, Quantity = 0 });
                //ViewData["NonAlcholicID"] = tuple.Item3.Where(d => d.ProductCategoryId == 2).Select(p => new { Id = p.Id, Price = p.Price, ViewText = p.Name, Quantity = 0 });
                //ViewData["AlcoholicID"] = tuple.Item3.Where(d => d.ProductCategoryId == 3).Select(p => new { Id = p.Id, Price = p.Price, ViewText = p.Name, Quantity = 0 });

                return View(order);
            }

        }

        // GET: Orders/Edit/5
        [Authorize(Roles = "Customer, Clerk, Super, Administrator")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || await _orderService.GetOrders() == null)
            {
                return NotFound();
            }

            var tuple = await _orderService.GetEdit(id);

            List<CustomerViewModel> customersIdList = new List<CustomerViewModel>();
            if (User.IsInRole("Administrator") || User.IsInRole("Clerk") || User.IsInRole("Super"))
            {
                foreach (Customer c in tuple.Item2)
                {
                    CustomerViewModel customerVM = new CustomerViewModel
                    {
                        Id = c.Id,
                        CustomerName = c.FullName
                    };
                    customersIdList.Add(customerVM);
                }
            }
            else
            {
                var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

                foreach (Customer c in tuple.Item2)
                {
                    if (c.IdentityUserId == userId)
                    {
                        CustomerViewModel customerVM = new CustomerViewModel
                        {
                            Id = c.Id,
                            CustomerName = c.FullName
                        };
                        customersIdList.Add(customerVM);
                    }
                }
            }

            List<ProductViewModel> productsIdList = new List<ProductViewModel>();

            foreach (Product p in tuple.Item4)
            {
                int quantity = 0;
                foreach (OrderLine ol in tuple.Item1.OrderLines)
                {
                    if (ol.DishId == p.Id)
                    {
                        quantity = ol.Quantity;
                    }
                }
                ProductViewModel productVM = new ProductViewModel
                {
                    Id = p.Id,
                    Price = p.Price,
                    Name = p.Name,
                    Description = p.Description,
                    Quantity = quantity
                };
                productsIdList.Add(productVM);
            }

            EditViewModel editVM = new EditViewModel
            {
                Id = tuple.Item1.Id,
                CustomerId = tuple.Item1.CustomerId,
                PaymentProcessor = tuple.Item1.PaymentProcessor,
                OrderStatus = tuple.Item1.OrderStatus,
                ExtraComments = tuple.Item1.ExtraComments,
                Order = tuple.Item1,
                SelectListCreate = customersIdList,
                ViewTimeSlots = tuple.Item3,
                ProductsIdList = productsIdList
            };

            //SelectList selectList = new SelectList(tuple.Item2.Select(c => new { Id = c.Id, ViewText = $"{c.FirstName} {c.LastName}" }), "Id", "ViewText");

            //ViewData["CustomerId"] = selectList;
            //ViewData["TimeSlotId"] = tuple.Item3;

            //List<Object> viewProducts = new List<Object>();


            //foreach (var vp in tuple.Item4)
            //{
            //    var ol = tuple.Item1.OrderLines.FirstOrDefault(x => x.DishId == vp.Id);
            //    int quantity = ol != null ? ol.Quantity : 0;
            //    viewProducts.Add(new { Id = vp.Id, Price = vp.Price, ViewText = vp.Name, Quantity = quantity });
            //}

            //ViewData["ProductsID"] = viewProducts;

            return View(editVM);
        }

        // POST: Orders/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Customer, Clerk, Super, Administrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ExtraComments,CustomerId,PaymentProcessor,OrderStatus")] Order order, int TimeSlotId, int[] productIds, int[] quantities)
        {
            if (id != order.Id)
            {
                return NotFound();
            }
            // Check of aanpassing mogelijk is
            var tuple = await _orderService.CheckTimeSlotEdit(id, productIds, quantities);
            if (tuple != null)
            {
                List<CustomerViewModel> customersIdList = new List<CustomerViewModel>();
                if (User.IsInRole("Administrator") || User.IsInRole("Clerk") || User.IsInRole("Super"))
                {
                    foreach (Customer c in tuple.Item1)
                    {
                        CustomerViewModel customerVM = new CustomerViewModel
                        {
                            Id = c.Id,
                            CustomerName = c.FullName
                        };
                        customersIdList.Add(customerVM);
                    }
                }
                else
                {
                    var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

                    foreach (Customer c in tuple.Item1)
                    {
                        if (c.IdentityUserId == userId)
                        {
                            CustomerViewModel customerVM = new CustomerViewModel
                            {
                                Id = c.Id,
                                CustomerName = c.FullName
                            };
                            customersIdList.Add(customerVM);
                        }
                    }
                }

                List<ProductViewModel> productsIdList = new List<ProductViewModel>();
                List<OrderLine> orderLineList = new List<OrderLine>();

                foreach (Product p in tuple.Item3)
                {
                    int quantity = 0;
                    foreach (OrderLine ol in order.OrderLines)
                    {
                        if (ol.DishId == p.Id)
                        {
                            quantity = ol.Quantity;
                        }
                    }
                    ProductViewModel productVM = new ProductViewModel
                    {
                        Id = p.Id,
                        Price = p.Price,
                        Name = p.Name,
                        Description = p.Description,
                        Quantity = quantity
                    };
                    productsIdList.Add(productVM);
                }
                order.TimeSlotId = TimeSlotId;
                EditViewModel editVM = new EditViewModel
                {
                    Id = order.Id,
                    CustomerId = order.CustomerId,
                    PaymentProcessor = order.PaymentProcessor,
                    OrderStatus = order.OrderStatus,
                    ExtraComments = order.ExtraComments,
                    Order = order,
                    SelectListCreate = customersIdList,
                    ViewTimeSlots = tuple.Item2,
                    ProductsIdList = productsIdList,
                    EditAlert = "Aanpassen is niet meer mogelijk"
                };
                //SelectList selectList = new SelectList(tuple.Item1, "Id", "ViewText");

                //ViewData["CustomerId"] = selectList;
                //ViewData["TimeSlotId"] = tuple.Item2;
                //ViewData["ProductsID"] = tuple.Item3;
                //ViewBag.EditAlert = "Aanpassen is niet meer mogelijk";

                return View(editVM);
            }

            // get originele order
            var oldOrder = await _orderService.GetOrder(id);
            order.CreationDate = oldOrder.CreationDate;

            // get oude timeslot
            int oldTimeSlotId = oldOrder.TimeSlotId;


            //als gebruiker een andere timeslot gekozen heeft.
            if (oldTimeSlotId != TimeSlotId)
            {
                _orderService.ChangeTimeslotEdit(order, oldTimeSlotId, TimeSlotId);
            }
            else
            {
                order.TimeSlotId = TimeSlotId;
            }

            // update or remove old orderlines
            _orderService.UpdateOrRemoveOrderLines(id, productIds, quantities);

            // new orderlines
            _orderService.AddNewOrderLines(id, productIds, quantities);

            // Update order DB
            try
            {
                _orderService.UpdateOrder(order);

                return RedirectToAction(nameof(Index));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrderExists(order.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            catch (Exception e)
            {
                var tupleException = await _orderService.EditException(id, productIds, quantities);

                List<CustomerViewModel> customersIdList = new List<CustomerViewModel>();
                if (User.IsInRole("Administrator") || User.IsInRole("Clerk") || User.IsInRole("Super"))
                {
                    foreach (Customer c in tuple.Item1)
                    {
                        CustomerViewModel customerVM = new CustomerViewModel
                        {
                            Id = c.Id,
                            CustomerName = c.FullName
                        };
                        customersIdList.Add(customerVM);
                    }
                }
                else
                {
                    var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

                    foreach (Customer c in tuple.Item1)
                    {
                        if (c.IdentityUserId == userId)
                        {
                            CustomerViewModel customerVM = new CustomerViewModel
                            {
                                Id = c.Id,
                                CustomerName = c.FullName
                            };
                            customersIdList.Add(customerVM);
                        }
                    }
                }

                List<ProductViewModel> productsIdList = new List<ProductViewModel>();
                List<OrderLine> orderLineList = new List<OrderLine>();

                foreach (Product p in tupleException.Item3)
                {
                    int quantity = 0;
                    foreach (OrderLine ol in order.OrderLines)
                    {
                        if (ol.DishId == p.Id)
                        {
                            quantity = ol.Quantity;
                        }
                    }
                    ProductViewModel productVM = new ProductViewModel
                    {
                        Id = p.Id,
                        Price = p.Price,
                        Name = p.Name,
                        Description = p.Description,
                        Quantity = quantity
                    };
                    productsIdList.Add(productVM);
                }
                order.TimeSlotId = TimeSlotId;
                EditViewModel editVM = new EditViewModel
                {
                    Id = order.Id,
                    PaymentProcessor = order.PaymentProcessor,
                    OrderStatus = order.OrderStatus,
                    ExtraComments = order.ExtraComments,
                    Order = order,
                    SelectListCreate = customersIdList,
                    ViewTimeSlots = tupleException.Item2,
                    ProductsIdList = productsIdList,
                };

                //SelectList selectListException = new SelectList(tupleException.Item1, "Id", "ViewText");
                //ViewData["CustomerId"] = selectListException;
                //ViewData["TimeSlotId"] = tupleException.Item2;
                //ViewData["ProductsID"] = tupleException.Item3;
                return View(editVM);
            }
        }

        // GET: Orders/Delete/5
        [Authorize(Roles = "Customer, Clerk, Super, Administrator")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || await _orderService.GetOrders() == null)
            {
                return NotFound();
            }

            var orderGet = await _orderService.GetOrder(id);

            //if (_orderService == null)
            //{
            //    return NotFound();
            //}

            return View(orderGet);
        }

        // POST: Orders/Delete/5
        [Authorize(Roles = "Customer, Clerk, Super, Administrator")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            if (await _orderService.GetOrders() == null)
            {
                return Problem("Entity set 'LekkerbekDbContext.Orders'  is null.");
            }
            var order = await _orderService.GetOrder(id);
            var orderPost = await _orderService.DeleteOrder(order);

            if (orderPost != null)
            {
                return RedirectToAction(nameof(Index));
            }

            ViewBag.DeleteAlert = "Verwijderen is niet meer mogelijk!";

            return View(await _orderService.GetOrder(id));
        }

        [Authorize(Roles = "Clerk, Super, Administrator")]
        public async Task<IActionResult> SendEmail(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Order order;
            try
            {
                order = await _orderService.GetOrderWithOrderLines(id);
            }
            catch (KeyNotFoundException ke)
            {
                return NotFound(ke.Message);
            }
            catch (Exception)
            {
                throw;
            }

            try
            {
                _mailService.Mail(order);
                ViewData["emailStatus"] = "E-mail succesvol verzonden.";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                ViewData["emailStatus"] = "E-mail kon niet worden verzonden. Kijk of e-mail adres geldig is.";
            }
            return View(order);
        }
        [Authorize(Roles = "Clerk, Super, Administrator")]
        public async Task<IActionResult> InvoiceAsync(int? id)
        {
            if (!id.HasValue)
            {
                return NotFound();
            }

            Order order;
            try
            {
                order = await _orderService.GetOrderWithOrderLines(id);
            }
            catch (KeyNotFoundException ke)
            {
                return NotFound(ke.Message);
            }
            catch (Exception)
            {
                throw;
            }

            Customer customer = await _customerService.GetCustomerWithOrders(order.CustomerId);
            int ammountOrders = customer.Orders.Count;

            ViewData["ammountOrders"] = ammountOrders;
            return View(order);
        }

        [HttpPost]
        [Authorize(Roles = "Clerk, Super, Administrator")]
        public IActionResult Invoice([Bind("Id,CreationDate,ExtraComments,TimeSlotId,CustomerId,PaymentProcessor,OrderStatus,HasDiscount")] Order order, bool HasDiscount)
        {
            if (order == null)
            {
                return NotFound();
            }
            try
            {
                _orderService.UpdateOrder(order);
                return RedirectToAction(nameof(Index));
            }
            catch (Exception)
            {

                throw;
            }
        }
        private bool OrderExists(int id)
        {
            return _orderService.OrderExist(id);
        }


    }
}
