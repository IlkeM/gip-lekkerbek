﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Lekkerbek.Web.Models.Entities;
using Lekkerbek.Web.Services;
using Lekkerbek.Web.ViewModels.Employees;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Lekkerbek.Web.Controllers
{
    [Authorize(Roles = "Administrator, Super")]
    public class EmployeesController : Controller
    {
        private readonly ICookService _cookService;
        private readonly IClerkService _clerkService;

        public EmployeesController(ICookService cookService, IClerkService clerkService)
        {
            _cookService = cookService;
            _clerkService = clerkService;
        }


        // GET: Cooks
        public async Task<IActionResult> Index()
        {
            return View();
        }

        // Kendo Table Cooks Index
        public async Task<IActionResult> GetCooks([DataSourceRequest] DataSourceRequest request)
        {
            var kendolekkerbekDbContextCooks = await _cookService.GetCooks();

            List<IndexViewModel> CooksVMList = new List<IndexViewModel>();

            foreach (var item in kendolekkerbekDbContextCooks)
            {
                IndexViewModel cooksVM = new IndexViewModel
                {
                    Id = item.Id,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    BirthDate = item.BirthDate,
                    Adress = item.Adress,
                    Email = item.Email,
                    PhoneNumber = item.PhoneNumber,
                };
                CooksVMList.Add(cooksVM);
            }

            JsonResult result = Json(CooksVMList.ToDataSourceResult(request));
            return result;
        }

        // Kendo Table Clerks Index
        public async Task<IActionResult> GetClerks([DataSourceRequest] DataSourceRequest request)
        {
            var kendolekkerbekDbContextClerks = await _clerkService.GetClerks();

            List<IndexViewModel> ClerksVMList = new List<IndexViewModel>();

            foreach (var item in kendolekkerbekDbContextClerks)
            {
                IndexViewModel clerksVM = new IndexViewModel
                {
                    Id = item.Id,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    BirthDate = item.BirthDate,
                    Adress = item.Adress,
                    Email = item.Email,
                    PhoneNumber = item.PhoneNumber,
                };
                ClerksVMList.Add(clerksVM);
            }


            JsonResult result = Json(ClerksVMList.ToDataSourceResult(request));
            return result;
        }

        // GET: Cooks/Details/5
        public async Task<IActionResult> CookDetails(int? id)
        {
            if (id == null || await _cookService.GetCooks() == null)
            {
                return NotFound();
            }

            var cook = await _cookService.GetCook(id);
            if (cook == null)
            {
                return NotFound();
            }

            return View(cook);
        }

        // GET: Cooks/Details/5
        public async Task<IActionResult> ClerkDetails(int? id)
        {
            if (id == null || await _clerkService.GetClerks() == null)
            {
                return NotFound();
            }

            var clerk = await _clerkService.GetClerk(id);
            if (clerk == null)
            {
                return NotFound();
            }

            return View(clerk);
        }

        // GET: Cooks/Create

        public IActionResult CookCreate()
        {
            return View();
        }

        // POST: Cooks/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CookCreate([Bind("Id,FirstName,LastName,BirthDate,Adress,Email,PhoneNumber,Password")] Cook cook)
        {
            try
            {
                await _cookService.CreateCook(cook);
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                //ViewBag.PasswordMessage = "minstens 1 hoofdletter + 1 kleineletter + 1 cijfer + 1 speciale leesteken";
                return RedirectToAction(nameof(CookCreate));
            }

        }

        // GET: Clerks/Create

        public IActionResult ClerkCreate()
        {
            return View();
        }

        // POST: Clerks/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ClerkCreate([Bind("Id,FirstName,LastName,BirthDate,Adress,Email,PhoneNumber,Password")] Clerk clerk)
        {
            try
            {
                await _clerkService.CreateClerk(clerk);
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                //ViewBag.PasswordMessage = "minstens 1 hoofdletter + 1 kleineletter + 1 cijfer + 1 speciale leesteken";
                return RedirectToAction(nameof(ClerkCreate));
            }

        }

        // GET: Cooks/Edit/5
        public async Task<IActionResult> CookEdit(int? id)
        {
            if (id == null || await _cookService.GetCooks() == null)
            {
                return NotFound();
            }

            var cook = await _cookService.GetCook(id);
            if (cook == null)
            {
                return NotFound();
            }
            return View(cook);
        }

        // POST: Cooks/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CookEdit(int id, [Bind("Id,FirstName,LastName,BirthDate,Adress,Email,PhoneNumber,IdentityUserId")] Cook cook)
        {
            if (id != cook.Id)
            {
                return NotFound();
            }

            try
            {
                await _cookService.EditCook(cook);
                return RedirectToAction(nameof(Index));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CookExists(cook.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
        }

        // GET: Clerks/Edit/5
        public async Task<IActionResult> ClerkEdit(int? id)
        {
            if (id == null || await _clerkService.GetClerks() == null)
            {
                return NotFound();
            }

            var clerk = await _clerkService.GetClerk(id);
            if (clerk == null)
            {
                return NotFound();
            }
            return View(clerk);
        }

        // POST: Clerks/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ClerkEdit(int id, [Bind("Id,FirstName,LastName,BirthDate,Adress,Email,PhoneNumber,IdentityUserId")] Clerk clerk)
        {
            if (id != clerk.Id)
            {
                return NotFound();
            }

            try
            {
                await _clerkService.EditClerk(clerk);
                return RedirectToAction(nameof(Index));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClerkExists(clerk.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
        }

        // GET: Cooks/Delete/5
        public async Task<IActionResult> CookDelete(int? id)
        {
            if (id == null || await _cookService.GetCooks() == null)
            {
                return NotFound();
            }

            var cook = await _cookService.GetCook(id);
            if (cook == null)
            {
                return NotFound();
            }

            return View(cook);
        }

        // POST: Cooks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CookDelete(int id)
        {
            if (await _cookService.GetCooks() == null)
            {
                return Problem("Entity set 'LekkerbekDbContext.Cooks'  is null.");
            }
            var cook = await _cookService.GetCook(id);
            if (cook != null)
            {
                await _cookService.DeleteCook(cook);
                return RedirectToAction(nameof(Index));
            }

            return View(cook);
        }

        // GET: Clerks/Delete/5
        public async Task<IActionResult> ClerkDelete(int? id)
        {
            if (id == null || await _clerkService.GetClerks() == null)
            {
                return NotFound();
            }

            var clerk = await _clerkService.GetClerk(id);
            if (clerk == null)
            {
                return NotFound();
            }

            return View(clerk);
        }

        // POST: Clerks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ClerkDelete(int id)
        {
            if (await _clerkService.GetClerks() == null)
            {
                return Problem("Entity set 'LekkerbekDbContext.Clerks'  is null.");
            }
            var clerk = await _clerkService.GetClerk(id);
            if (clerk != null)
            {
                await _clerkService.DeleteClerk(clerk);
                return RedirectToAction(nameof(Index));
            }

            return View(clerk);
        }

        private bool CookExists(int id)
        {
            return _cookService.CookExist(id);
        }
        private bool ClerkExists(int id)
        {
            return _clerkService.ClerkExists(id);
        }
    }
}
