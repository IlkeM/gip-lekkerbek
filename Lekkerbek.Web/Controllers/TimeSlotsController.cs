﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Lekkerbek.Web.Data;
using Lekkerbek.Web.Models.Entities;
using Lekkerbek.Web.Services;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using Lekkerbek.Web.ViewModels.Timeslots;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;

namespace Lekkerbek.Web.Controllers
{
    [Authorize(Roles = "Cook, Super")]
    public class TimeSlotsController : Controller
    {
        private readonly ITimeSlotService _timeSlotService;

        public TimeSlotsController(ITimeSlotService timeSlotService)
        {
            _timeSlotService = timeSlotService;
        }

        // GET: TimeSlots
        public async Task<IActionResult> Index()
        {
            var timeSlots = await _timeSlotService.GetTimeSlots();

            List<IndexViewModel> timeSlotsVMList = new List<IndexViewModel>();

            foreach (var timeSlot in timeSlots)
            {
                int idOfOrder = 0;
                if (timeSlot.Order != null)
                {
                    idOfOrder = timeSlot.Order.Id;
                }
                IndexViewModel IndexVM = new IndexViewModel
                {
                    Id = timeSlot.Id,
                    StartTime = timeSlot.StartTime.ToString("dd/MM/yyyy HH:mm"),
                    IsOccupied = timeSlot.IsOccupied,
                    CookFirstName = timeSlot.Cook.FirstName,
                    CookIdentityUserId = timeSlot.Cook.IdentityUserId,
                    OrderId = idOfOrder,
                };
                timeSlotsVMList.Add(IndexVM);
            }
            if (User.IsInRole("Super"))
            {
                return View(timeSlotsVMList);
            }
            else
            {
                var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                return View(timeSlotsVMList.Where(ts => ts.CookIdentityUserId == userId));
            }
        }

        // Kendo Table TimeSlot Index
        public async Task<IActionResult> GetTimeSlots([DataSourceRequest] DataSourceRequest request)
        {
            var kendolekkerbekDbContextTimeSlots = await _timeSlotService.GetTimeSlots();

            List<IndexViewModel> timeSlotsVMList = new List<IndexViewModel>();

            foreach (var timeSlot in kendolekkerbekDbContextTimeSlots)
            {
                int idOfOrder = 0;
                if (timeSlot.Order != null)
                {
                    idOfOrder = timeSlot.Order.Id;
                }
                IndexViewModel IndexVM = new IndexViewModel
                {
                    Id = timeSlot.Id,
                    StartTime = timeSlot.StartTime.ToString("dd/MM/yyyy HH:mm"),
                    IsOccupied = timeSlot.IsOccupied,
                    CookFirstName = timeSlot.Cook.FirstName,
                    CookIdentityUserId = timeSlot.Cook.IdentityUserId,
                    OrderId = idOfOrder,
                };
                timeSlotsVMList.Add(IndexVM);
            }
            if (User.IsInRole("Super"))
            {
                JsonResult result = Json(timeSlotsVMList.ToDataSourceResult(request));
                return result;
            }
            else
            {
                var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                JsonResult result = Json(timeSlotsVMList.Where(ts => ts.CookIdentityUserId == userId).ToDataSourceResult(request));
                return result;
            }
        }

        // get: timeslots/details/5
        //public async Task<IActionResult> Details(int? id)
        //{
        //    if (id == null || await _timeSlotService.GetTimeSlots() == null)
        //    {
        //        return NotFound();
        //    }

        //    var timeslot = await _timeSlotService.DetailTimeSlot(id);

        //    DetailsViewModel detailsVM = new DetailsViewModel
        //    {
        //        Id = timeslot.Id,
        //        StartTime = timeslot.StartTime,
        //        IsOccupied = timeslot.IsOccupied,
        //        CookFirstName = timeslot.Cook.FirstName,
        //        OrderId = timeslot.Order.Id,
        //        OrderExtraComments = timeslot.Order.ExtraComments
        //    };


        //    if (timeslot == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(detailsVM);
        //}

        //// GET: TimeSlots/Create
        //public IActionResult Create()
        //{
        //    ViewData["CookId"] = new SelectList(_context.Cooks, "Id", "Id");
        //    return View();
        //}

        //// POST: TimeSlots/Create
        //// To protect from overposting attacks, enable the specific properties you want to bind to.
        //// For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Create([Bind("Id,StartTime,Date,IsOccupied,CookId")] TimeSlot timeSlot)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        _context.Add(timeSlot);
        //        await _context.SaveChangesAsync();
        //        return RedirectToAction(nameof(Index));
        //    }
        //    ViewData["CookId"] = new SelectList(_context.Cooks, "Id", "Id", timeSlot.CookId);
        //    return View(timeSlot);
        //}

        //// GET: TimeSlots/Edit/5
        //public async Task<IActionResult> Edit(int? id)
        //{
        //    if (id == null || _context.TimeSlots == null)
        //    {
        //        return NotFound();
        //    }

        //    var timeSlot = await _context.TimeSlots.FindAsync(id);
        //    if (timeSlot == null)
        //    {
        //        return NotFound();
        //    }
        //    ViewData["CookId"] = new SelectList(_context.Cooks, "Id", "Id", timeSlot.CookId);
        //    return View(timeSlot);
        //}

        //// POST: TimeSlots/Edit/5
        //// To protect from overposting attacks, enable the specific properties you want to bind to.
        //// For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(int id, [Bind("Id,StartTime,Date,IsOccupied,CookId")] TimeSlot timeSlot)
        //{
        //    if (id != timeSlot.Id)
        //    {
        //        return NotFound();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            _context.Update(timeSlot);
        //            await _context.SaveChangesAsync();
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            if (!TimeSlotExists(timeSlot.Id))
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                throw;
        //            }
        //        }
        //        return RedirectToAction(nameof(Index));
        //    }
        //    ViewData["CookId"] = new SelectList(_context.Cooks, "Id", "Id", timeSlot.CookId);
        //    return View(timeSlot);
        //}

        //// GET: TimeSlots/Delete/5
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    if (id == null || _context.TimeSlots == null)
        //    {
        //        return NotFound();
        //    }

        //    var timeSlot = await _context.TimeSlots
        //        .Include(t => t.Cook)
        //        .FirstOrDefaultAsync(m => m.Id == id);
        //    if (timeSlot == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(timeSlot);
        //}

        //// POST: TimeSlots/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(int id)
        //{
        //    if (_context.TimeSlots == null)
        //    {
        //        return Problem("Entity set 'LekkerbekDbContext.TimeSlots'  is null.");
        //    }
        //    var timeSlot = await _context.TimeSlots.FindAsync(id);
        //    if (timeSlot != null)
        //    {
        //        _context.TimeSlots.Remove(timeSlot);
        //    }

        //    await _context.SaveChangesAsync();
        //    return RedirectToAction(nameof(Index));
        //}

        //private bool TimeSlotExists(int id)
        //{
        //    return (_context.TimeSlots?.Any(e => e.Id == id)).GetValueOrDefault();
        //}
    }
}
