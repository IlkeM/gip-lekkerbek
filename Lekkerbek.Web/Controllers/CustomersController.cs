﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Lekkerbek.Web.Data;
using Lekkerbek.Web.Models.Entities;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Lekkerbek.Web.Services;
using Microsoft.AspNetCore.Authorization;
using Lekkerbek.Web.ViewModels.Customers;
using System.Security.Claims;

namespace Lekkerbek.Web.Controllers
{
    [Authorize(Roles = "Clerk, Customer, Super, Administrator")]
    public class CustomersController : Controller
    {
        //private readonly LekkerbekDbContext _context;
        private readonly ICustomerService _customerService;

        public CustomersController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        // GET: Customers
        public async Task<IActionResult> Index()
        {
            var customers = await _customerService.GetCustomers();

            List<IndexViewModel> customersVMList = new List<IndexViewModel>();

            foreach (var item in customers)
            {
                IndexViewModel customerVM = new IndexViewModel
                {
                    Id = item.Id,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    BirthDate = item.BirthDate,
                    Adress = item.Adress,
                    Email = item.Email,
                    PhoneNumber = item.PhoneNumber,
                    Orders = item.Orders,
                    FoodPreference = item.FoodPreference,
                    Company = item.Company,
                    IsProfessional = item.IsProfessional, 
                    IdentityUserId = item.IdentityUserId
                };
                customersVMList.Add(customerVM);
            }
            if (User.IsInRole("Administrator") || User.IsInRole("Clerk") || User.IsInRole("Super"))
            {
                return View(customersVMList);
            } else
            {
                var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
                return View(customersVMList.Where(c => c.IdentityUserId == userId));
            }
        }
        // Kendo Table Customers Index
        public async Task<IActionResult> GetCustomers([DataSourceRequest] DataSourceRequest request)
        {
            var kendolekkerbekDbContextCustomers = await _customerService.GetCustomers();

            List<IndexViewModel> customersVMList = new List<IndexViewModel>();

            foreach (var item in kendolekkerbekDbContextCustomers)
            {
                IndexViewModel customerVM = new IndexViewModel
                {
                    Id = item.Id,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    BirthDate = item.BirthDate,
                    Adress = item.Adress,
                    Email = item.Email,
                    PhoneNumber = item.PhoneNumber,
                    Orders = item.Orders,
                    FoodPreference = item.FoodPreference,
                    Company = item.Company,
                    IsProfessional = item.IsProfessional
                };
                customersVMList.Add(customerVM);
            }

            JsonResult result = Json(customersVMList.ToDataSourceResult(request));
            return result;
        }

        // GET: Customers/Details/5

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || await _customerService.GetCustomers() == null)
            {
                return NotFound();
            }

            var customerDetail = await _customerService.GetCustomer(id);

            if (customerDetail == null)
            {
                return NotFound();
            }

            DetailsViewModel customerVM = new DetailsViewModel
            {
                Id = customerDetail.Id,
                FirstName = customerDetail.FirstName,
                LastName = customerDetail.LastName,
                BirthDate = customerDetail.BirthDate,
                Adress = customerDetail.Adress,
                Email = customerDetail.Email,
                PhoneNumber = customerDetail.PhoneNumber,
                Orders = customerDetail.Orders,
                FoodPreference = customerDetail.FoodPreference,
                Company = customerDetail.Company,
                IsProfessional = customerDetail.IsProfessional
            };

            return View(customerVM);
        }

        // GET: Customers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Customers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("FoodPreference,Id,FirstName,LastName,BirthDate,Adress,Email,PhoneNumber")] Customer customer)
        {
            try
            {
                await _customerService.CreateCustomer(customer);
                return RedirectToAction(nameof(Index));

            }
            catch (Exception ex)
            {
                return RedirectToAction(nameof(Create));
            }
        }

        // GET: Customers/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || await _customerService.GetCustomers() == null)
            {
                return NotFound();
            }

            var customer = await _customerService.GetCustomer(id);
            if (customer == null)
            {
                return NotFound();
            }
            return View(customer);
        }

        // POST: Customers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("FoodPreference,Id,FirstName,LastName,BirthDate,Adress,Email,PhoneNumber")] Customer customer)
        {
            if (id != customer.Id)
            {
                return NotFound();
            }

            try
            {
                await _customerService.EditCustomer(customer);
                return RedirectToAction(nameof(Index));
            }
            catch (Exception)
            {
                if (!CustomerExists(customer.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
        }

        // GET: Customers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || await _customerService.GetCustomers() == null)
            {
                return NotFound();
            }

            var customerGetDelete = await _customerService.GetCustomer(id);

            if (customerGetDelete == null)
            {
                return NotFound();
            }

            DeleteViewModel customerVM = new DeleteViewModel
            {
                Id = customerGetDelete.Id,
                FirstName = customerGetDelete.FirstName,
                LastName = customerGetDelete.LastName,
                BirthDate = customerGetDelete.BirthDate,
                Adress = customerGetDelete.Adress,
                Email = customerGetDelete.Email,
                PhoneNumber = customerGetDelete.PhoneNumber,
                Orders = customerGetDelete.Orders,
                FoodPreference = customerGetDelete.FoodPreference,
                Company = customerGetDelete.Company,
                IsProfessional = customerGetDelete.IsProfessional
            };

            return View(customerVM);
        }

        // POST: Customers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            if (await _customerService.GetCustomers() == null)
            {
                return Problem("Entity set 'LekkerbekDbContext.Customers'  is null.");
            }

            var customerPostDelete = await _customerService.DeleteCustomer(id);

            if (customerPostDelete != null)
            {
                return RedirectToAction(nameof(Index));
            }

            var customerGetDelete = await _customerService.GetCustomer(id);

            DeleteViewModel customerVM = new DeleteViewModel
            {
                Id = customerGetDelete.Id,
                FirstName = customerGetDelete.FirstName,
                LastName = customerGetDelete.LastName,
                BirthDate = customerGetDelete.BirthDate,
                Adress = customerGetDelete.Adress,
                Email = customerGetDelete.Email,
                PhoneNumber = customerGetDelete.PhoneNumber,
                Orders = customerGetDelete.Orders,
                FoodPreference = customerGetDelete.FoodPreference,
                Company = customerGetDelete.Company,
                IsProfessional = customerGetDelete.IsProfessional
            };

            return View(customerVM);

        }

        private bool CustomerExists(int id)
        {
            return _customerService.CustomerExist(id);
        }
    }
}
