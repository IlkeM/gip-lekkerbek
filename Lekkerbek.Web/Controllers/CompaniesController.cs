﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Lekkerbek.Web.Data;
using Lekkerbek.Web.Models.Entities;
using Lekkerbek.Web.Services;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Microsoft.AspNetCore.Authorization;
using Lekkerbek.Web.ViewModels.Companies;

namespace Lekkerbek.Web.Controllers
{
    [Authorize(Roles = "Clerk, Super, Administrator")]
    public class CompaniesController : Controller
    {
        private readonly ICompanyService _companyService;
        private readonly IContactPersonService _contactPersonService;

        public CompaniesController(ICompanyService company, IContactPersonService contactPersonService)
        {
            _companyService = company;
            _contactPersonService = contactPersonService;
        }

        // GET: Companies
        public async Task<IActionResult> Index()
        {
            if (User.IsInRole("Customer"))
            {
                return Unauthorized();
            }
            var companies = await _companyService.GetCompanies();
            var companiesViewModels = companies.Select(c => new IndexViewModel() { Id = c.Id, ContactPersonId = c.ContactPersonId, ContactName = c.ContactPerson.FullName, Name = c.Name, VatNumber = c.VatNumber }).ToList();
            return View(companiesViewModels);
        }

        // Kendo Table Company Index
        public async Task<IActionResult> GetCompanies([DataSourceRequest] DataSourceRequest request)
        {
            var companies = await _companyService.GetCompanies();
            var companiesViewModels = companies.Select(c => new IndexViewModel() { Id = c.Id, ContactPersonId = c.ContactPersonId/*, ContactName = c.ContactPerson.FullName*/, Name = c.Name, VatNumber = c.VatNumber }).ToList();
            JsonResult result = Json(companiesViewModels.ToDataSourceResult(request));
            return result;
        }

        // GET: Companies/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Company company;
            try
            {
                company = await _companyService.GetCompany(id);
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
            catch (Exception)
            {

                throw;
            }

            return View(company);
        }

        // GET: Companies/Create
        public async Task<IActionResult> Create()
        {
            var contactPersonViewModel = await _contactPersonService.GetContactPersons();
            ViewData["ContactPersonId"] = new SelectList(contactPersonViewModel.Where(cp => cp.Company == null).Select(cp => new { Id = cp.Id, ViewText = $"{cp.FirstName} {cp.LastName}" }), "Id", "ViewText");
            return View();
        }

        // POST: Companies/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,VatNumber,ContactPersonId")] Company company)
        {
            try
            {
                await _companyService.CreateCompany(company);
                return RedirectToAction(nameof(Index));
            }
            catch (Exception)
            {
                var contactPersonViewModel = await _contactPersonService.GetContactPersons();
                ViewData["ContactPersonId"] = new SelectList(contactPersonViewModel.Select(cp => new { Id = cp.Id, ViewText = $"{cp.FirstName} {cp.LastName}" }), "Id", "ViewText");
                return View(company);
            }
        }

        public async Task<IActionResult> CreateContactPerson()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateContactPerson([Bind("FoodPreference,Id,FirstName,LastName,BirthDate,Adress,Email,PhoneNumber")] Customer customer)
        {
            try
            {
                await _contactPersonService.CreateContactPerson(customer);
                return RedirectToAction(nameof(Index));

            }
            catch (Exception)
            {
                return RedirectToAction(nameof(Create));
            }
        }

        // GET: Companies/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || await _companyService.GetCompanies() == null)
            {
                return NotFound();
            }

            var company = await _companyService.GetCompany(id);
            if (company == null)
            {
                return NotFound();
            }
            var contactPersonViewModel = await _contactPersonService.GetContactPersons();
            ViewData["ContactPersonId"] = new SelectList(contactPersonViewModel.Select(cp => new { Id = cp.Id, ViewText = $"{cp.FirstName} {cp.LastName}" }), "Id", "ViewText");
            return View(company);
        }

        // POST: Companies/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,VatNumber,ContactPersonId")] Company company)
        {
            if (id != company.Id)
            {
                return NotFound();
            }

            try
            {
                await _companyService.EditCompany(company);
                return RedirectToAction(nameof(Index));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CompanyExists(company.Id))
                {
                    return NotFound();
                }
                else
                {
                    var contactPersonViewModel = await _contactPersonService.GetContactPersons();
                    ViewData["ContactPersonId"] = new SelectList(contactPersonViewModel.Select(cp => new { Id = cp.Id, ViewText = $"{cp.FirstName} {cp.LastName}" }), "Id", "ViewText");
                    return View(company);
                }
            }
        }

        // GET: Companies/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || await _companyService.GetCompanies() == null)
            {
                return NotFound();
            }

            var company = await _companyService.GetCompany(id);
            if (company == null)
            {
                return NotFound();
            }

            return View(company);
        }

        // POST: Companies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (await _companyService.GetCompanies() == null)
            {
                return Problem("Entity set 'LekkerbekDbContext.Companies'  is null.");
            }
            var company = await _companyService.GetCompany(id);
            if (company != null)
            {
                await _companyService.DeleteCompany(company);
                return RedirectToAction(nameof(Index));
            }
            return View(company);

        }
        private bool CompanyExists(int id)
        {
            return _companyService.CompanyExist(id);
        }
    }
}
