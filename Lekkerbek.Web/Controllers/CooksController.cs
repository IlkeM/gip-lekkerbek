﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Lekkerbek.Web.Data;
using Lekkerbek.Web.Models.Entities;
using Lekkerbek.Web.Services;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Lekkerbek.Web.ViewModels.Cooks;
using System.ComponentModel;

namespace Lekkerbek.Web.Controllers
{
    [Authorize(Roles = "Administrator, Super")]
    public class CooksController : Controller
    {
        private readonly ICookService _cookService;
        private readonly IOrderService _orderService;

        public CooksController(ICookService cookService, IOrderService orderService)
        {
            _cookService = cookService;
            _orderService = orderService;
        }

        // GET: Cooks
        public async Task<IActionResult> Index()
        {
            var cooks = await _cookService.GetCooks();
            return View(cooks);
        }

        // Kendo Table Cooks Index
        public async Task<IActionResult> GetCooks([DataSourceRequest] DataSourceRequest request)
        {
            var kendolekkerbekDbContextCooks = await _cookService.GetCooks();
            JsonResult result = Json(kendolekkerbekDbContextCooks.ToDataSourceResult(request));
            return result;
        }

        // GET: Cooks/Details/5
        public async Task<IActionResult> Details(int? id)
        {

            if (id == null || await _cookService.GetCooks() == null)
            {
                return NotFound();
            }

            var cook = await _cookService.GetCook(id);
            if (cook == null)
            {
                return NotFound();
            }

            DetailsViewModel detailsVM = new DetailsViewModel
            {
                TimeSlots = cook.TimeSlots,
                Id = cook.Id,
                FirstName = cook.FirstName,
                LastName = cook.LastName,
                BirthDate = cook.BirthDate,
                Adress = cook.Adress,
                Email = cook.Email,
                PhoneNumber = cook.PhoneNumber,
                IsDeleted = cook.IsDeleted,
                IdentityUserId = cook.IdentityUserId,
                IdentityUser = cook.IdentityUser

            };

            return View(detailsVM);
        }

        // GET: Cooks/Create

        public IActionResult Create()
        {
            return View();
        }

        // POST: Cooks/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,FirstName,LastName,BirthDate,Adress,Email,PhoneNumber,Password")] Cook cook)
        {
            try
            {
                await _cookService.CreateCook(cook);
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                return RedirectToAction(nameof(Create));
            }

        }

        // GET: Cooks/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || await _cookService.GetCooks() == null)
            {
                return NotFound();
            }

            var cook = await _cookService.GetCook(id);
            if (cook == null)
            {
                return NotFound();
            }
            return View(cook);
        }

        // POST: Cooks/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,FirstName,LastName,BirthDate,Adress,Email,PhoneNumber")] Cook cook)
        {
            if (id != cook.Id)
            {
                return NotFound();
            }

            try
            {
                await _cookService.EditCook(cook);
                return RedirectToAction(nameof(Index));
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CookExists(cook.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
        }

        // GET: Cooks/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || await _cookService.GetCooks() == null)
            {
                return NotFound();
            }

            var cook = await _cookService.GetCook(id);
            if (cook == null)
            {
                return NotFound();
            }

            return View(cook);
        }

        // POST: Cooks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int id)
        {
            if (await _cookService.GetCooks() == null)
            {
                return Problem("Entity set 'LekkerbekDbContext.Cooks'  is null.");
            }
            var cook = await _cookService.GetCook(id);
            if (cook != null)
            {
                await _cookService.DeleteCook(cook);
                return RedirectToAction(nameof(Index));
            }

            return View(cook);
        }

        private bool CookExists(int id)
        {
            return _cookService.CookExist(id);
        }
    }
}
