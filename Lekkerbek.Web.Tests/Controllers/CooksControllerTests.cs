﻿using Lekkerbek.Web.Controllers;
using Lekkerbek.Web.Data;
using Lekkerbek.Web.Models.Entities;
using Lekkerbek.Web.Services;
using Lekkerbek.Web.ViewModels.Cooks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Lekkerbek.Web.Tests.Controllers
{
  [TestClass]
  public class CooksControllerTests
  {
    private CooksController _controller;

    [TestInitialize]
    public void Initialize()
    {
      var connection = new SqliteConnection("DataSource=:memory:");
      connection.Open();

      var options = new DbContextOptionsBuilder<LekkerbekDbContext>().UseSqlite(connection).Options;


      // Configure any additional dependencies for the controller (e.g.,services, repositories)
      var mockCookService = new Mock<ICookService>();
      var mockOrderService = new Mock<IOrderService>();


      // Configure mockServices as needed 
      var cookNr1 = new Cook { Id = 1, FirstName = "Jan", LastName="de Pan" };
      mockCookService.Setup(x =>
      x.GetCook(cookNr1.Id)).ReturnsAsync(cookNr1);

      var cookNr2 = new Cook { Id = 2, FirstName = "Bob", LastName = "de Kok" };
      mockCookService.Setup(x => x.GetCooks()).ReturnsAsync(
       new List<Cook> { cookNr1, cookNr2 });


      var userManagerMock = new Mock<UserManager<IdentityUser>>(new
      Mock<IUserStore<IdentityUser>>().Object,
       null, null, null, null, null, null, null, null);
      userManagerMock.Setup(x => x.FindByIdAsync(It.IsAny<string>()))
      .ReturnsAsync((string userId) => new IdentityUser { Id = userId });



      // Create the controller instance with the required dependencies
      var user = new Mock<ClaimsPrincipal>();
      user.Setup(x => x.Identity.IsAuthenticated).Returns(true);
      user.Setup(x => x.Identity.Name).Returns("testuser");
      user.Setup(x => x.FindFirst(ClaimTypes.Role)).Returns(new
      Claim(ClaimTypes.Role, "Administrator"));

      _controller = new CooksController(mockCookService.Object, mockOrderService.Object)
      {
        ControllerContext = new ControllerContext
        {
          HttpContext = new DefaultHttpContext { User = user.Object }
        }
      };
    }
    [TestCleanup]
    public void Cleanup()
    {
      _controller.Dispose();
    }

    [TestMethod]
    public async Task Details_ReturnsViewResult_ofCookViewModel()
    {
      try
      {
        // Arrange
        // service objects setup in the Initialize method

        //Act
        var result = await _controller.Details(1);

        //Assert
        Assert.IsInstanceOfType(result, typeof(ViewResult));
        var viewResult = (ViewResult)result;

        //check the viewresult model
        Assert.IsTrue(viewResult.Model is DetailsViewModel cook && cook.Id == 1);
      }
      catch (AssertFailedException)
      {
        Assert.Fail("The Details result is no viewResult");
      }
      
    }
    [TestMethod]
    public async Task Create_ReturnsRedirectToActionResult_ofIndex()
    {
      try
      {
        //Arrange
        var cookNr3 = new Cook { Id = 3, FirstName = "Gordon", LastName = "Ramsay" };
        //Act
        var result = await _controller.Create(cookNr3);

        //Assert
        Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
        var redirectToActionResult = (RedirectToActionResult)result;

        //check the action name
        Assert.AreEqual(nameof(Index),redirectToActionResult.ActionName);
      }
      catch (AssertFailedException)
      {
        Assert.Fail("Create result is no RedirectToActionResult to Index"); 
      }    
    }
    [TestMethod]
    public async Task Edit_ReturnsNotFoundResult()
    {
      // Arrange
      // service objects setup in the Initialize method

      //Act
      var result = _controller.Edit(10).Result;

      //Assert
      Assert.IsInstanceOfType(result, typeof(NotFoundResult));
    }
  }
}
