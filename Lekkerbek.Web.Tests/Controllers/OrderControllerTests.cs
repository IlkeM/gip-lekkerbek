﻿using Lekkerbek.Web.Controllers;
using Lekkerbek.Web.Data;
using Lekkerbek.Web.Models.Entities;
using Lekkerbek.Web.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Lekkerbek.Web.Tests.Controllers
{
  [TestClass]
  public class OrderControllerTests
  {
    private OrdersController _controller;
    [TestInitialize]
    public void Initialize()
    {
      var connection = new SqliteConnection("DataSource=:memory:");
      connection.Open();

      var options = new DbContextOptionsBuilder<LekkerbekDbContext>().UseSqlite(connection).Options;


      // Configure any additional dependencies for the controller (e.g.,services, repositories)
      var mockOrderService = new Mock<IOrderService>();
      var mockCustomerService = new Mock<ICustomerService>();
      var mockMailService = new Mock<IMailService>();


      // Configure mockServices as needed 
      var customerNr1 = new Customer { Id = 1, LastName = "Doe", FirstName = "John" };
      mockCustomerService.Setup(x =>
      x.GetCustomer(customerNr1.Id)).ReturnsAsync(customerNr1);
      var customerNr2 = new Customer { Id = 2, LastName = "Doe", FirstName = "Jane" };
      mockCustomerService.Setup(x => x.GetCustomers()).ReturnsAsync(
       new List<Customer> { customerNr1, customerNr2 });

      var orderNr1 = new Order { Id = 1, CustomerId = 1};
      mockOrderService.Setup(x =>
      x.GetOrder(orderNr1.Id)).ReturnsAsync(orderNr1);
      mockOrderService.Setup(x =>
      x.DeleteOrder(orderNr1)).ReturnsAsync(orderNr1);

      var orderNr2 = new Order { Id = 1, CustomerId = 2};
      mockOrderService.Setup(x =>
      x.GetOrders()).ReturnsAsync(new List<Order> { orderNr1, orderNr2 });

      
      var userManagerMock = new Mock<UserManager<IdentityUser>>(new
      Mock<IUserStore<IdentityUser>>().Object,
       null, null, null, null, null, null, null, null);
      userManagerMock.Setup(x => x.FindByIdAsync(It.IsAny<string>()))
      .ReturnsAsync((string userId) => new IdentityUser { Id = userId });


      // Create the controller instance with the required dependencies
      var user = new Mock<ClaimsPrincipal>();
      user.Setup(x => x.Identity.IsAuthenticated).Returns(true);
      user.Setup(x => x.Identity.Name).Returns("testuser");
      user.Setup(x => x.FindFirst(ClaimTypes.Role)).Returns(new
      Claim(ClaimTypes.Role, "Administrator"));

      _controller = new OrdersController(mockOrderService.Object, mockCustomerService.Object, mockMailService.Object)
      {
        ControllerContext = new ControllerContext
        {
          HttpContext = new DefaultHttpContext { User = user.Object }
        }
      };
    }
    [TestMethod]
    public async Task Delete_ReturnsRedirectToActionResult_OfIndex()
    {
      
      try
      {
        // Arrange
        // service objects setup in the Initialize method

        // Act
        var result = await _controller.Delete(1);

        // Assert
        Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
        var redirectResult = (RedirectToActionResult)result;

        // check the ActionName
        Assert.AreEqual(nameof(Index), redirectResult.ActionName);

      }
      catch (AssertFailedException)
      {
        Assert.Fail("The delete result is no RedirectToActionResult!");
      }
    }
  }
}
