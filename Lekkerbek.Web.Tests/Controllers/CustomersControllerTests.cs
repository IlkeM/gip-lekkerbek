﻿using Kendo.Mvc.UI;
using Lekkerbek.Web.Controllers;
using Lekkerbek.Web.Data;
using Lekkerbek.Web.Models.Entities;
using Lekkerbek.Web.Services;
using Lekkerbek.Web.ViewModels.Customers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Telerik.SvgIcons;

namespace Lekkerbek.Web.Tests.Controllers
{
  [TestClass]
  public class CustomersControllerTests
  {
    private CustomersController _controller;

    [TestInitialize]
    public void Initialize()
    {
      var connection = new SqliteConnection("DataSource=:memory:");
      connection.Open();

      var options = new DbContextOptionsBuilder<LekkerbekDbContext>().UseSqlite(connection).Options;

      // Configure any additional dependencies for the controller (e.g.,services, repositories)
      var mockCustomerService = new Mock<ICustomerService>();


      // Configure mockServices as needed 
      var customerNr1 = new Customer { Id = 1, LastName = "Doe", FirstName = "John" };
      mockCustomerService.Setup(x =>
      x.GetCustomer(customerNr1.Id)).ReturnsAsync(customerNr1);
      mockCustomerService.Setup(x =>
      x.DeleteCustomer(customerNr1.Id)).ReturnsAsync(customerNr1);

      var customerNr2 = new Customer { Id = 2, LastName = "Doe", FirstName = "Jane" };
      mockCustomerService.Setup(x => x.GetCustomers()).ReturnsAsync(
       new List<Customer> { customerNr1, customerNr2 });

      var userManagerMock = new Mock<UserManager<IdentityUser>>(new
      Mock<IUserStore<IdentityUser>>().Object,
       null, null, null, null, null, null, null, null);
      userManagerMock.Setup(x => x.FindByIdAsync(It.IsAny<string>()))
      .ReturnsAsync((string userId) => new IdentityUser { Id = userId });


      // Create the controller instance with the required dependencies
      var user = new Mock<ClaimsPrincipal>();
      user.Setup(x => x.Identity.IsAuthenticated).Returns(true);
      user.Setup(x => x.Identity.Name).Returns("testuser");
      user.Setup(x => x.FindFirst(ClaimTypes.Role)).Returns(new
      Claim(ClaimTypes.Role, "Administrator"));

      _controller = new CustomersController(mockCustomerService.Object)
      {
        ControllerContext = new ControllerContext
        {
          HttpContext = new DefaultHttpContext { User = user.Object }
        }
      };
    }
    [TestCleanup]
    public void Cleanup()
    {
      _controller.Dispose();
    }
    [TestMethod]
    public async Task Details_ReturnsViewResult_ofCustomerViewModel()
    {
      
      try
      {
        // Arrange
        // service objects setup in the Initialize method

        // Act
        var result = await _controller.Details(1);

        // Assert
        Assert.IsInstanceOfType(result, typeof(ViewResult));
        var viewResult = (ViewResult)result;
        // check the viewresult model
        Assert.IsTrue(viewResult.Model is DetailsViewModel customer && customer.Id == 1);

      }
      catch (AssertFailedException)
      {
        Assert.Fail("The Details result is no ViewResult!");
      }
    }

    [TestMethod]
    public async Task DeleteCustomer_ReturnsRedirectToActionResult_OfIndex()
    {
      
      try
      {
        // Arrange
        // service objects setup in the Initialize method

        // Act
        var result = await _controller.Delete(1);

        // Assert
        Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
        var redirectResult = (RedirectToActionResult)result;

        // check the ActionName
        Assert.AreEqual(nameof(Index), redirectResult.ActionName);

      }
      catch (AssertFailedException)
      {
        Assert.Fail("The delete result is no RedirectToActionResult!");
      }
    }
    
   
  }
}

