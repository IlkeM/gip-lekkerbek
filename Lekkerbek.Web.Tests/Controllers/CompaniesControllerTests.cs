﻿using Lekkerbek.Web.Controllers;
using Lekkerbek.Web.Data;
using Lekkerbek.Web.Models.Entities;
using Lekkerbek.Web.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Lekkerbek.Web.Tests.Controllers
{
  [TestClass]
  public class CompaniesControllerTests
  {
    private CompaniesController _controller;

    [TestInitialize]
    public void Initialize()
    {
      var connection = new SqliteConnection("DataSource=:memory:");
      connection.Open();

      var options = new DbContextOptionsBuilder<LekkerbekDbContext>().UseSqlite(connection).Options;


      // Configure any additional dependencies for the controller (e.g.,services, repositories)
      var mockCompanyService = new Mock<ICompanyService>();
      var mockContactPersonService = new Mock<IContactPersonService>();


      // Configure mockServices as needed 
      var companyNr1 = new Company { Id = 1, Name = "Dovy Keukens"};
      mockCompanyService.Setup(x =>
      x.GetCompany(companyNr1.Id)).ReturnsAsync(companyNr1);
      
      var companyNr2 = new Company { Id = 2, Name = "Coca Cola" };
      mockCompanyService.Setup(x => x.GetCompanies()).ReturnsAsync(
       new List<Company> { companyNr1, companyNr2 });

      var contactPersonNr1 = new Customer { Id = 1, FirstName = "Jane", LastName = "Doe" };
      mockContactPersonService.Setup(x => x.GetContactPerson(contactPersonNr1.Id)).ReturnsAsync(contactPersonNr1);

      /*var contactPersonNr2 = new Customer { Id = 2, FirstName = "John", LastName = "Doe" };
      mockContactPersonService.Setup(x => x.GetContactPersons()).ReturnsAsync(new List<Customer> { contactPersonNr1, contactPersonNr2});
      */

      var userManagerMock = new Mock<UserManager<IdentityUser>>(new
      Mock<IUserStore<IdentityUser>>().Object,
       null, null, null, null, null, null, null, null);
      userManagerMock.Setup(x => x.FindByIdAsync(It.IsAny<string>()))
      .ReturnsAsync((string userId) => new IdentityUser { Id = userId });


      // Create the controller instance with the required dependencies
      var user = new Mock<ClaimsPrincipal>();
      user.Setup(x => x.Identity.IsAuthenticated).Returns(true);
      user.Setup(x => x.Identity.Name).Returns("testuser");
      user.Setup(x => x.FindFirst(ClaimTypes.Role)).Returns(new
      Claim(ClaimTypes.Role, "Administrator"));

      _controller = new CompaniesController(mockCompanyService.Object, mockContactPersonService.Object)
      {
        ControllerContext = new ControllerContext
        {
          HttpContext = new DefaultHttpContext { User = user.Object }
        }
      };
    }

    [TestCleanup]
    public void Cleanup()
    {
      _controller.Dispose();
    }

    [TestMethod]
    public async Task Details_ReturnsViewResult_OfCompany()
    {
      try
      {
        // Arrange
        // service objects setup in the Initialize method

        //Act
        var result = await _controller.Details(1);

        //Assert
        Assert.IsInstanceOfType(result, typeof(ViewResult));
        var viewResult = (ViewResult)result;

        //check the viewresult model
        Assert.IsTrue(viewResult.Model is Company company && company.Id == 1);
      }
      catch (AssertFailedException)
      {
        Assert.Fail("The Details result is no ViewResult");
      }
    }
    [TestMethod]
    public async Task Create_ReturnsRedirectToActionResult_ofIndex()
    {
      try
      {
        // Arrange
        // service objects setup in the Initialize method
        var companyNr3 = new Company { Id = 3, Name = "Nintendo" };
        //Act

        var result = await _controller.Create(companyNr3);

        //Assert
        Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
        var redirectToActionResult = (RedirectToActionResult)result;

        //check the action name
        Assert.AreEqual(nameof(Index), redirectToActionResult.ActionName);
      }
      catch(AssertFailedException)
      {
        Assert.Fail("Create result is no RedirectToActionResult");
      }
    }

    [TestMethod]
    public async Task CreateContactPerson_ReturnsRedirectToActionResult_ofIndex()
    {
      try
      {
        // Arrange
        // service objects setup in the Initialize method
        var contactPersonNr2 = new Customer { Id = 2, FirstName = "John", LastName = "Doe" };
        //Act

        var result = await _controller.CreateContactPerson(contactPersonNr2);

        //Assert
        Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
        var redirectToActionResult = (RedirectToActionResult)result;

        //check the action name
        Assert.AreEqual(nameof(Index), redirectToActionResult.ActionName);
      }
      catch (AssertFailedException)
      {
        Assert.Fail("CreateContactPerson result is no RedirectToActionResult"); 
      }
    }
  }
}
