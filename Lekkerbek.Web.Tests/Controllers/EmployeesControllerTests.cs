﻿using Kendo.Mvc.UI;
using Lekkerbek.Web.Controllers;
using Lekkerbek.Web.Data;
using Lekkerbek.Web.Models.Entities;
using Lekkerbek.Web.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Lekkerbek.Web.Tests.Controllers
{
  [TestClass]
  public class EmployeesControllerTests
  {
    private EmployeesController _controller;

    [TestInitialize]
    public void Initialize()
    {
      var connection = new SqliteConnection("DataSource=:memory:");
      connection.Open();

      var options = new DbContextOptionsBuilder<LekkerbekDbContext>().UseSqlite(connection).Options;


      // Configure any additional dependencies for the controller (e.g.,services, repositories)
      var mockCookService = new Mock<ICookService>();
      var mockClerkService = new Mock<IClerkService>();


      // Configure mockServices as needed 
      var cookNr1 = new Cook { Id = 1, FirstName = "Jan", LastName = "de Pan" };
      mockCookService.Setup(x =>
      x.GetCook(cookNr1.Id)).ReturnsAsync(cookNr1);

      var cookNr2 = new Cook { Id = 2, FirstName = "Bob", LastName = "de Kok" };
      mockCookService.Setup(x => x.GetCooks()).ReturnsAsync(
       new List<Cook> { cookNr1, cookNr2 });


      var clerkNr1 = new Clerk { Id = 1, FirstName = "John", LastName = "Doe" };
      mockClerkService.Setup(x =>
      x.GetClerk(clerkNr1.Id)).ReturnsAsync(clerkNr1);

      mockClerkService.Setup(x =>
      x.EditClerk(clerkNr1)).ReturnsAsync(clerkNr1);

      var clerkNr2 = new Clerk { Id = 2, FirstName = "Jane", LastName = "Doe" };
      mockClerkService.Setup(x => x.GetClerks()).ReturnsAsync(
       new List<Clerk> { clerkNr1, clerkNr2 });


      var userManagerMock = new Mock<UserManager<IdentityUser>>(new
      Mock<IUserStore<IdentityUser>>().Object,
       null, null, null, null, null, null, null, null);
      userManagerMock.Setup(x => x.FindByIdAsync(It.IsAny<string>()))
      .ReturnsAsync((string userId) => new IdentityUser { Id = userId });


      // Create the controller instance with the required dependencies
      var user = new Mock<ClaimsPrincipal>();
      user.Setup(x => x.Identity.IsAuthenticated).Returns(true);
      user.Setup(x => x.Identity.Name).Returns("testuser");
      user.Setup(x => x.FindFirst(ClaimTypes.Role)).Returns(new
      Claim(ClaimTypes.Role, "Administrator"));

      _controller = new EmployeesController(mockCookService.Object, mockClerkService.Object)
      {
        ControllerContext = new ControllerContext
        {
          HttpContext = new DefaultHttpContext { User = user.Object }
        }
      };
    }
    [TestCleanup]
    public void Cleanup()
    {
      _controller.Dispose();
    }

    [TestMethod]
    public async Task CookDetails_ReturnsViewResult_ofCook()
    {
      try
      {
        // Arrange
        // service objects setup in the Initialize method
        //Act
        var result = await _controller.CookDetails(1);

        //Assert
        Assert.IsInstanceOfType(result, typeof(ViewResult));
        var viewResult = (ViewResult)result;

        //check the viewresult model
        Assert.IsTrue(viewResult.Model is Cook cook && cook.Id == 1);
      }
      catch (AssertFailedException)
      {
        Assert.Fail("The CookDetails result is no viewResult");
      }
    }
    [TestMethod]
    public async Task ClerkDetails_ReturnsViewResult_ofCook()
    {
      try
      {
        // Arrange
        // service objects setup in the Initialize method
        //Act
        var result = await _controller.ClerkDetails(1);

        //Assert
        Assert.IsInstanceOfType(result, typeof(ViewResult));
        var viewResult = (ViewResult)result;

        //check the viewresult model
        Assert.IsTrue(viewResult.Model is Clerk clerk && clerk.Id == 1);
      }
      catch (AssertFailedException)
      {
        Assert.Fail("The ClerkDetails result is no viewResult");
      }
    }
    [TestMethod]
    public async Task CookCreate_ReturnsRedirectToActionResult_ofIndex()
    {
      try
      {
        //Arrange
        // service objects setup in the Initialize method
        var cookNr3 = new Cook { Id = 3, FirstName = "Gordon", LastName = "Ramsay" };
        //Act
        var result = await _controller.CookCreate(cookNr3);

        //Assert
        Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
        var redirectToActionResult = (RedirectToActionResult)result;

        //check the action name
        Assert.AreEqual(nameof(Index), redirectToActionResult.ActionName);
      }
      catch (AssertFailedException)
      {
        Assert.Fail("CookCreate result is no RedirectToActionResult");
      }
    }
    [TestMethod]
    public async Task ClerkCreate_ReturnsRedirectToActionResult_ofIndex()
    {
      try
      {
        //Arrange
        // service objects setup in the Initialize method
        var clerkNr3 = new Clerk { Id = 3, FirstName = "Bob", LastName = "de Bouwer" };
        //Act
        var result = await _controller.ClerkCreate(clerkNr3);

        //Assert
        Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
        var redirectToActionResult = (RedirectToActionResult)result;

        //check the action name
        Assert.AreEqual(nameof(Index), redirectToActionResult.ActionName);
      }
      catch (AssertFailedException)
      {
        Assert.Fail("ClerkCreate result is no RedirectToActionResult");
      }
    }
    [TestMethod]
    public async Task CookEdit_ReturnsNotFoundResult()
    {
      try
      {
        //Arrange
        // service objects setup in the Initialize method

        //Act
        var result = _controller.CookEdit(10).Result;

        //Assert
        Assert.IsInstanceOfType(result, typeof(NotFoundResult));
      }
      catch (AssertFailedException)
      {
        Assert.Fail("CookEdit result is no NotFoundResult");
      }

    }
    [TestMethod]
    public async Task ClerkEdit_ReturnsNotFoundResult()
    {
      try
      {
        //Arrange
        // service objects setup in the Initialize method

        //Act
        var result = _controller.ClerkEdit(10).Result;

        //Assert
        Assert.IsInstanceOfType(result, typeof(NotFoundResult));
      }
      catch (AssertFailedException)
      {
        Assert.Fail("ClerkEdit result is no NotFoundResult");
      }
    }
    [TestMethod]
    public async Task ClerkEdit_ReturnsRedirectToActionResult_ofIndex()
    {
      try
      {
        //Arrange
        // service objects setup in the Initialize method
        var clerkNr1 = new Clerk() { Id = 1, FirstName = "Johnny", LastName = "Doe" };
        //Act
        var result = _controller.ClerkEdit(1, clerkNr1).Result;

        //Assert
        Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
        var redirectToActionResult = (RedirectToActionResult)result;

        //check the action name
        Assert.AreEqual(nameof(Index), redirectToActionResult.ActionName);
      }
      catch (AssertFailedException)
      {
        Assert.Fail("ClerkEdit result is no RedirectToActionResult Index");
      }
    }
    [TestMethod]
    public async Task CookDelete_ReturnsRedirectToActionResult_OfIndex()
    {
      
      try
      {
        // Arrange
        // service objects setup in the Initialize method

        // Act
        var result = await _controller.CookDelete(1);

        // Assert
        Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
        var redirectResult = (RedirectToActionResult)result;

        // check the ActionName
        Assert.AreEqual(nameof(Index), redirectResult.ActionName);

      }
      catch (AssertFailedException)
      {
        Assert.Fail("CookDelete result is no RedirectToActionResult!");
      }
    }
    [TestMethod]
    public async Task ClerkDelete_ReturnsRedirectToActionResult_OfIndex()
    {
      
      try
      {
        // Arrange
        // service objects setup in the Initialize method

        // Act
        var result = await _controller.ClerkDelete(1);

        // Assert
        Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
        var redirectResult = (RedirectToActionResult)result;

        // check the ActionName
        Assert.AreEqual(nameof(Index), redirectResult.ActionName);

      }
      catch (AssertFailedException)
      {
        Assert.Fail("ClerkDelete result is no RedirectToActionResult!");
      }
    }
  }
}
