﻿using Castle.Components.DictionaryAdapter.Xml;
using Lekkerbek.Web.Controllers;
using Lekkerbek.Web.Data;
using Lekkerbek.Web.Models.Entities;
using Lekkerbek.Web.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Moq;
using NuGet.Protocol;
using System.Security.Claims;

namespace Lekkerbek.Web.Tests.Authorise_Authenticate
{
    [TestClass]
    public class CompaniesRoleIdentityTests
    {
        private CompaniesController _controller;

        [TestInitialize]
        public void Initialize()
        {
            // Setup an in-memory SQLite database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<LekkerbekDbContext>()
            .UseSqlite(connection)
            .Options;

            var mockCompanyService = new Mock<ICompanyService>();
            var mockContactPersonService = new Mock<IContactPersonService>();

            Customer contactPerson = new Customer() { Id = 1, FirstName = "John", LastName = "Doe" };
            Company company = new Company() { Id = 1, ContactPersonId = contactPerson.Id, ContactPerson = contactPerson, Name = "UCLL", VatNumber = "BE0123456789" };

            mockCompanyService.Setup(x => x.GetCompanies()).ReturnsAsync(new List<Company>() { company });

            var userMock = new Mock<ClaimsPrincipal>();
            userMock.Setup(x => x.Identity.IsAuthenticated).Returns(true);
            userMock.Setup(x => x.Identity.Name).Returns("customerUser");
            userMock.Setup(x => x.IsInRole("Customer")).Returns(true);
            userMock.Setup(x => x.IsInRole("Clerk")).Returns(false);
            userMock.Setup(x => x.IsInRole("Super")).Returns(false);
            userMock.Setup(x => x.IsInRole("Administrator")).Returns(false);

            _controller = new CompaniesController(mockCompanyService.Object, mockContactPersonService.Object)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = new DefaultHttpContext { User = userMock.Object }
                }
            };

        }
        [TestCleanup]
        public void Cleanup()
        {
            _controller.Dispose();
        }

        [TestMethod]
        public async Task CompaniesController_Create_ForbiddenForCustomerRole()
        {
            // Arrange
            // Act
            var result = await _controller.Index();
            //var viewResult = (ViewResult)result;
            // Assert
            //await Console.Out.WriteLineAsync(result.);
            Assert.IsInstanceOfType(result, typeof(UnauthorizedResult));
            //await Console.Out.WriteLineAsync(viewResult.ToJson());
            //await Console.Out.WriteLineAsync("test");
            //Assert.IsTrue(true);
        }

    }
}
