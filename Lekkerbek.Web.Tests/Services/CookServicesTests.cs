﻿using Lekkerbek.Web.Data;
using Lekkerbek.Web.Models.Entities;
using Lekkerbek.Web.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.SvgIcons;

namespace Lekkerbek.Web.Tests.Services
{
    [TestClass]
    public class CookServicesTests
    {
        private LekkerbekDbContext _context;
        private UserManager<IdentityUser> _userManager;
        private TimeSlotService _timeSlotService;
        private CookService _cookService;
        [TestInitialize]
        public void Setup()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<LekkerbekDbContext>()
            .UseSqlite(connection)
            .Options;

            _context = new LekkerbekDbContext(options);
            _context.Database.EnsureCreated();


            var userManagerMock = new Mock<UserManager<IdentityUser>>(new
Mock<IUserStore<IdentityUser>>().Object,
 null, null, null, null, null, null, null, null);
            userManagerMock.Setup(x => x.FindByIdAsync(It.IsAny<string>()))
.ReturnsAsync((string userId) => new IdentityUser { Id = userId });
            //userManagerMock.Setup(x => x.CreateAsync(It.IsAny<IdentityUser>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success);

            var identityUser = new IdentityUser { UserName = "test@ucll.be" };
            // voeg the aspnetuser toe aan de db (zodat de clerk kan gelinkt worden)
            _context.Users.Add(identityUser);
            _context.SaveChanges();

            string userId = identityUser.Id;

            userManagerMock
                .Setup(x => x.CreateAsync(It.IsAny<IdentityUser>(), It.IsAny<string>()))
                .ReturnsAsync((IdentityUser identityUser, string _) =>
                {
                    identityUser.Id = userId; // Set a test Id for the newly created user
                    return IdentityResult.Success;
                });

            _userManager = userManagerMock.Object;

            _timeSlotService = new TimeSlotService(_context);

            _cookService = new CookService(_context, _userManager, _timeSlotService);
        }
        [TestCleanup]
        public void Cleanup()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }

        [TestMethod]
        public async Task CreateCook_Succes()
        {
            // Arrange
            var cook = new Cook { Id = 1, FirstName = "Jan", LastName = "Kok", Adress = "Kokstraat", BirthDate = DateTime.Now.AddMonths(-5), Email = "jankok@ucll.be", PhoneNumber = "123456789", Password = "JankokPass123.." };

            // Act
            await _cookService.CreateCook(cook);

            // Assert
            var cookCreated = _context.Cooks.FirstOrDefaultAsync(o => o.Id == cook.Id);
            Assert.IsNotNull(cookCreated);
        }

        [TestMethod]
        public async Task CreateCook_ReturnsNull()
        {
            // Arrange
            Cook cook = null;

            // Act
            var cookCreated = await _cookService.CreateCook(cook);

            // Assert
            Assert.IsNull(cookCreated);
        }

        [TestMethod]
        public async Task EditCook_Succes()
        {
            // Arrange
            var cook = new Cook { Id = 1, FirstName = "Jan", LastName = "Kok", Adress = "Kokstraat", BirthDate = DateTime.Now.AddMonths(-5), Email = "jankok@ucll.be", PhoneNumber = "123456789", Password = "JankokPass123.." };

            var cookUser = new IdentityUser
            {
                Email = cook.Email,
                UserName = cook.Email,
                EmailConfirmed = true
            };
            cook.IdentityUser = cookUser;

            _context.Cooks.Add(cook);
            await _context.SaveChangesAsync();

            cook.IsDeleted = true;

            // Act
            await _cookService.EditCook(cook);

            // Arrange
            var retrievedCook = await _context.Cooks.FirstOrDefaultAsync(o => o.Id == cook.Id);
            Assert.IsNotNull(retrievedCook);
            Assert.AreEqual(retrievedCook.IsDeleted, true);
        }

        [TestMethod]
        public async Task EditCook_ReturnNull()
        {
            // Arrange
            Cook cook = null;

            // Act
            var cookEdited = await _cookService.EditCook(cook);

            // Assert
            Assert.IsNull(cookEdited);
        }
    }
}
