﻿using Lekkerbek.Web.Data;
using Lekkerbek.Web.Models.Entities;
using Lekkerbek.Web.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lekkerbek.Web.Tests.Services
{
    [TestClass]
    public class ClerkServiceTests
    {
        private LekkerbekDbContext _context;
        private UserManager<IdentityUser> _userManager;
        private ClerkService _clerkService;
        [TestInitialize]
        public void Setup()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<LekkerbekDbContext>()
            .UseSqlite(connection)
            .Options;

            _context = new LekkerbekDbContext(options);
            _context.Database.EnsureCreated();

            var userManagerMock = new Mock<UserManager<IdentityUser>>(new
Mock<IUserStore<IdentityUser>>().Object,
 null, null, null, null, null, null, null, null);
            userManagerMock.Setup(x => x.FindByIdAsync(It.IsAny<string>()))
 .ReturnsAsync((string userId) => new IdentityUser { Id = userId });

            var identityUser = new IdentityUser { UserName = "test@ucll.be" };
            // voeg the aspnetuser toe aan de db (zodat de clerk kan gelinkt worden)
            _context.Users.Add(identityUser);
            _context.SaveChanges();

            string userId = identityUser.Id;

            userManagerMock
                .Setup(x => x.CreateAsync(It.IsAny<IdentityUser>(), It.IsAny<string>()))
                .ReturnsAsync((IdentityUser identityUser, string _) =>
                {
                    identityUser.Id = userId; // Set a test Id for the newly created user
                    return IdentityResult.Success;
                });

            _userManager = userManagerMock.Object;

            _clerkService = new ClerkService(_userManager, _context);

        }
        [TestCleanup]
        public void Cleanup()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }

        [TestMethod]
        public async Task CreateClerk_Succes()
        {
            // Assert 
            var clerk2 = new Clerk { Id = 2, FirstName = "Roderik", LastName = "Geijsels", BirthDate = DateTime.Now.AddDays(-30), Adress = "Probeerstraat 990", Email = "diederik@ucll.be", PhoneNumber = "04555557987", Password = "DiederikPass123.." };

            // Act
            await _clerkService.CreateClerk(clerk2);

            // Assert
            var createdClerk = await _context.Clerks.FirstOrDefaultAsync(o => o.Id == clerk2.Id);
            Assert.IsNotNull(createdClerk);
        }

        [TestMethod]
        public async Task CreateClerk_InvalidData()
        {
            // Arrange
            Clerk invalidClerk = null;

            // Act
            var noClerkCreated = await _clerkService.CreateClerk(invalidClerk);

            // Assert
            Assert.IsNull(noClerkCreated);
        }

        [TestMethod]
        public async Task GetClerk_ClerkNotFound()
        {
            // Arrange
            var nonExistentClerkId = 123;

            // Act 
            var retrievedClerk = await _clerkService.GetClerk(nonExistentClerkId);

            // Assert
            Assert.IsNull(retrievedClerk);

        }

        [TestMethod]
        public async Task EditClerk_Succes()
        {
            // Assert
            var clerk = new Clerk { Id = 1, FirstName = "Diederik", LastName = "Billiet", BirthDate = DateTime.Now.AddDays(-30), Adress = "Probeerstraat 999", Email = "diederik@ucll.be", PhoneNumber = "04555557987", Password = "DiederikPass123.." };

            var clerkUser = new IdentityUser
            {
                Email = clerk.Email,
                UserName = clerk.Email,
                EmailConfirmed = true
            };
            clerk.IdentityUser = clerkUser;

            _context.Clerks.Add(clerk);
            await _context.SaveChangesAsync();

            clerk.FirstName = "Axel";

            // Act 
            await _clerkService.EditClerk(clerk);

            // Assert
            var retrievedClerk = await _context.Clerks.FirstOrDefaultAsync(o => o.Id == clerk.Id);
            Assert.IsNotNull(retrievedClerk);
            Assert.AreEqual(retrievedClerk.FirstName, "Axel");
        }
    }
}
