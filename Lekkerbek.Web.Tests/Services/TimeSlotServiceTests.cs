﻿using Lekkerbek.Web.Data;
using Lekkerbek.Web.Models.Entities;
using Lekkerbek.Web.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lekkerbek.Web.Tests.Services
{
    [TestClass]
    public class TimeSlotServiceTests
    {
        private LekkerbekDbContext _context;
        private TimeSlotService _timeSlotService;

        [TestInitialize]
        public void Setup()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<LekkerbekDbContext>()
            .UseSqlite(connection)
            .Options;

            _context = new LekkerbekDbContext(options);
            _context.Database.EnsureCreated();

            _timeSlotService = new TimeSlotService(_context);
        }

        [TestCleanup]
        public void Cleanup()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }

        [TestMethod]
        public async Task CreateTimeSlots_Succes()
        {
            // Arrange
            var cook = new Cook { Id = 1, FirstName = "Jan", LastName = "Kok", Adress = "Kokstraat", BirthDate = DateTime.Now.AddMonths(-5), Email = "jankok@ucll.be", PhoneNumber = "123456789", Password = "JankokPass123.." };
            var cookUser = new IdentityUser
            {
                Email = cook.Email,
                UserName = cook.Email,
                EmailConfirmed = true
            };
            cook.IdentityUser = cookUser;
            _context.Cooks.Add(cook);
            await _context.SaveChangesAsync();

            // Act
            await _timeSlotService.CreateTimeSlots(DateTime.Now, cook);

            // Assert
            var retrievedTimeslots = await _context.TimeSlots.ToListAsync();
            Assert.IsNotNull(retrievedTimeslots);
        }
    }
}
