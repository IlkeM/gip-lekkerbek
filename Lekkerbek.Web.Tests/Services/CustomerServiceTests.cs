﻿using Lekkerbek.Web.Data;
using Lekkerbek.Web.Models.Entities;
using Lekkerbek.Web.Services;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lekkerbek.Web.Tests.Services
{
    [TestClass]
    public class CustomerServiceTests
    {
        private LekkerbekDbContext _context;
        private CustomersService _customerService;
        [TestInitialize]
        public void Setup()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<LekkerbekDbContext>()
            .UseSqlite(connection)
            .Options;

            _context = new LekkerbekDbContext(options);
            _context.Database.EnsureCreated();

            _customerService = new CustomersService(_context);
        }
        [TestCleanup]
        public void Cleanup()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }

        [TestMethod]
        public async Task CreateCustomer_Succes()
        {
            // Arrange
            var customer = new Customer { Id = 1, FirstName = "Diederik", LastName = "Billiet", BirthDate = DateTime.Now.AddDays(-30), Adress = "Probeerstraat 999", Email = "diederik@ucll.be", PhoneNumber = "04555557987" };

            // Act
            await _customerService.CreateCustomer(customer);

            // Assert
            var createdCustomer = await _context.Customers.FirstOrDefaultAsync(o => o.Id == customer.Id);
            Assert.IsNotNull(createdCustomer);
        }

        [TestMethod]
        [ExpectedException(typeof(DbUpdateException))]
        public async Task CreateCustomer_InvalidData()
        {
            // Arrange
            var invalidCustomer = new Customer { Id = 1, FirstName = null, LastName = "Billiet", BirthDate = DateTime.Now.AddDays(-30), Adress = "Probeerstraat 999", Email = "diederik@ucll.be", PhoneNumber = "04555557987", };

            // Act
            await _customerService.CreateCustomer(invalidCustomer);

            // Assert
        }

        [TestMethod]
        public async Task GetCustomerById_Succes()
        {
            // Arrange
            var customer = new Customer { Id = 1, FirstName = "Diederik", LastName = "Billiet", BirthDate = DateTime.Now.AddDays(-30), Adress = "Probeerstraat 999", Email = "diederik@ucll.be", PhoneNumber = "04555557987" };
            _context.Customers.Add(customer);
            await _context.SaveChangesAsync();

            // Act 
            var retrievedCustomer = await _customerService.GetCustomer(customer.Id);

            // Arrange
            Assert.IsNotNull(retrievedCustomer);
            Assert.AreEqual(customer.Id, retrievedCustomer.Id);
        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public async Task GetCustomerById_CustomerNotFound()
        {
            // Arrange
            var nonExistentCustomerId = 123;

            // Act 
            var retrievedCustomer = await _customerService.GetCustomer(nonExistentCustomerId);

            // Assert

        }

        [TestMethod]
        public async Task EditCustomer_Succes()
        {
            // Arrange
            var customer = new Customer { Id = 1, FirstName = "Diederik", LastName = "Billiet", BirthDate = DateTime.Now.AddDays(-30), Adress = "Probeerstraat 999", Email = "diederik@ucll.be", PhoneNumber = "04555557987" };
            _context.Customers.Add(customer);
            await _context.SaveChangesAsync();

            customer.Adress = "Probeerselstraat 999";

            // Act 
            await _customerService.EditCustomer(customer);

            // Assert
            var retrievedCustomer = await _context.Customers.FirstOrDefaultAsync(o => o.Id == customer.Id);
            Assert.IsNotNull(retrievedCustomer);
            Assert.AreEqual(customer.Adress, "Probeerselstraat 999");
        }

        [TestMethod]
        public async Task DeleteCustomer_Succes()
        {
            // Arrange
            var customer = new Customer { Id = 1, FirstName = "Diederik", LastName = "Billiet", BirthDate = DateTime.Now.AddDays(-30), Adress = "Probeerstraat 999", Email = "diederik@ucll.be", PhoneNumber = "04555557987" };
            _context.Customers.Add(customer);
            await _context.SaveChangesAsync();

            // Act 
            await _customerService.DeleteCustomer(customer.Id);

            // Assert
            var retrievedCustomer = await _context.Customers.FirstOrDefaultAsync(o => o.Id == customer.Id);
            Assert.IsNull(retrievedCustomer);
        }
    }
}
