﻿using Lekkerbek.Web.Data;
using Lekkerbek.Web.Models.Entities;
using Lekkerbek.Web.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.SvgIcons;

namespace Lekkerbek.Web.Tests.Services
{
    [TestClass]
    public class OrderServicesTests
    {
        private LekkerbekDbContext _context;
        private OrdersService _orderService;
        [TestInitialize]
        public void Setup()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<LekkerbekDbContext>()
            .UseSqlite(connection)
            .Options;

            _context = new LekkerbekDbContext(options);
            _context.Database.EnsureCreated();

            _orderService = new OrdersService(_context);
        }

        [TestCleanup]
        public void Cleanup()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }

        [TestMethod]
        public async Task OrderGetDetails_Succes()
        {
            // Arrange

            var customer = new Customer
            {
                Id = 1,
                FirstName = "Diederik",
                LastName = "Billiet",
                BirthDate = DateTime.Now,
                Adress = "Pieter Verhaeghenlaan 6",
                Email = "diederik@ucll.be",
                PhoneNumber = "123456789"
            };
            var cook = new Cook { Id = 1, FirstName = "Jan", LastName = "Kok", Adress = "Kokstraat", BirthDate = DateTime.Now.AddMonths(-5), Email = "jankok@ucll.be", PhoneNumber = "123456789", Password = "JankokPass123.." };

            var cookUser = new IdentityUser
            {
                Email = cook.Email,
                UserName = cook.Email,
                EmailConfirmed = true
            };
            cook.IdentityUser = cookUser;

            var timeSlot = new TimeSlot { CookId = cook.Id, Cook = cook, Id = 1, Date = DateTime.Now, StartTime = DateTime.Now.Date.AddHours(1), IsOccupied = true };

            var productCategory = new ProductCategory
            {
                Id = 1,
                Category = "Afhaalgerechten",
                Tax = 6,
            };
            var product = new Product
            {
                Id = 1,
                Name = "Frietjes",
                Description = "Krokante frietjes met mayo",
                Price = 10,
                ProductCategoryId = 1,
                ProductCategory = productCategory,
            };
            var orderline = new OrderLine { OrderLineId = 1, OrderID = 1, DishId = 1, Dish = product, Quantity = 1 };

            var order = new Order
            {
                Id = 1,
                CreationDate = DateTime.Now.AddHours(-3),
                TimeSlotId = timeSlot.Id,
                TimeSlot = timeSlot,
                CustomerId = customer.Id,
                Customer = customer,
                OrderStatus = Models.Enums.OrderStatus.Wachten_op_betaling,
                OrderLines = new[] { orderline },
                PaymentProcessor = Models.Enums.PaymentProcessor.Contant,
            };
            _context.Orders.Add(order);
            await _context.SaveChangesAsync();

            // Act
            var retrievedTuple = await _orderService.GetDetails(order.Id);

            // Assert
            Assert.IsNotNull(retrievedTuple);
            Assert.AreEqual(order, retrievedTuple.Item1);
            Assert.AreEqual(typeof(decimal), retrievedTuple.Item2.GetType());
            Assert.AreEqual(10, retrievedTuple.Item2);
            Assert.AreEqual(1, retrievedTuple.Item3.Count());
            Assert.AreEqual(1, retrievedTuple.Item4.Count());
        }

        [TestMethod]
        public async Task GetCreate_Succes()
        {
            // Arrange
            var customer = new Customer
            {
                Id = 1,
                FirstName = "Diederik",
                LastName = "Billiet",
                BirthDate = DateTime.Now,
                Adress = "Pieter Verhaeghenlaan 6",
                Email = "diederik@ucll.be",
                PhoneNumber = "123456789"
            };
            _context.Customers.Add(customer);
            await _context.SaveChangesAsync();

            var cook = new Cook { Id = 1, FirstName = "Jan", LastName = "Kok", Adress = "Kokstraat", BirthDate = DateTime.Now.AddMonths(-5), Email = "jankok@ucll.be", PhoneNumber = "123456789", Password = "JankokPass123.." };
            var cookUser = new IdentityUser
            {
                Email = cook.Email,
                UserName = cook.Email,
                EmailConfirmed = true
            };
            cook.IdentityUser = cookUser;
            _context.Cooks.Add(cook);
            await _context.SaveChangesAsync();

            var timeSlot1 = new TimeSlot { CookId = cook.Id, Cook = cook, Id = 1, Date = DateTime.Now.AddHours(2), StartTime = DateTime.Now.AddHours(2), IsOccupied = false };
            _context.TimeSlots.Add(timeSlot1);
            await _context.SaveChangesAsync();

            var timeSlot2 = new TimeSlot { CookId = cook.Id, Cook = cook, Id = 2, Date = DateTime.Now.AddHours(2).AddMinutes(15), StartTime = DateTime.Now.AddHours(2).AddMinutes(15), IsOccupied = false };
            _context.TimeSlots.Add(timeSlot2);
            await _context.SaveChangesAsync();

            var productCategory = new ProductCategory
            {
                Id = 1,
                Category = "Afhaalgerechten",
                Tax = 6,
            };
            var product = new Product
            {
                Id = 1,
                Name = "Frietjes",
                Description = "Krokante frietjes met mayo",
                Price = 10,
                ProductCategoryId = 1,
                ProductCategory = productCategory,
            };
            _context.Products.Add(product);
            await _context.SaveChangesAsync();

            // Act
            var retrievedTuple = await _orderService.GetCreate();

            // Assert;
            Assert.AreEqual(1, retrievedTuple.Item1.Count());
            Assert.AreEqual(typeof(List<Customer>), retrievedTuple.Item1.GetType());
            Assert.AreEqual(1, retrievedTuple.Item2.Count());
            Assert.AreEqual(1, retrievedTuple.Item3.Count());
            Assert.AreEqual(typeof(List<Product>), retrievedTuple.Item3.GetType());
        }

        [TestMethod]
        public async Task GetEdit_Succes()
        {
            // Arrange
            var customer = new Customer
            {
                Id = 1,
                FirstName = "Diederik",
                LastName = "Billiet",
                BirthDate = DateTime.Now,
                Adress = "Pieter Verhaeghenlaan 6",
                Email = "diederik@ucll.be",
                PhoneNumber = "123456789"
            };
            _context.Customers.Add(customer);
            await _context.SaveChangesAsync();

            var cook = new Cook { Id = 1, FirstName = "Jan", LastName = "Kok", Adress = "Kokstraat", BirthDate = DateTime.Now.AddMonths(-5), Email = "jankok@ucll.be", PhoneNumber = "123456789", Password = "JankokPass123.." };
            var cookUser = new IdentityUser
            {
                Email = cook.Email,
                UserName = cook.Email,
                EmailConfirmed = true
            };
            cook.IdentityUser = cookUser;
            _context.Cooks.Add(cook);
            await _context.SaveChangesAsync();

            var timeSlot1 = new TimeSlot { CookId = cook.Id, Cook = cook, Id = 1, Date = DateTime.Now.AddHours(2), StartTime = DateTime.Now.AddHours(2), IsOccupied = false };
            _context.TimeSlots.Add(timeSlot1);
            await _context.SaveChangesAsync();

            var timeSlot2 = new TimeSlot { CookId = cook.Id, Cook = cook, Id = 2, Date = DateTime.Now.AddHours(2).AddMinutes(15), StartTime = DateTime.Now.AddHours(2).AddMinutes(15), IsOccupied = false };
            _context.TimeSlots.Add(timeSlot2);
            await _context.SaveChangesAsync();

            var productCategory = new ProductCategory
            {
                Id = 1,
                Category = "Afhaalgerechten",
                Tax = 6,
            };
            var product = new Product
            {
                Id = 1,
                Name = "Frietjes",
                Description = "Krokante frietjes met mayo",
                Price = 10,
                ProductCategoryId = 1,
                ProductCategory = productCategory,
            };
            _context.Products.Add(product);
            await _context.SaveChangesAsync();

            var orderline = new OrderLine { OrderLineId = 1, Dish = product, DishId = product.Id, Quantity = 5 };

            var order = new Order
            {
                Id = 1,
                CreationDate = DateTime.Now,
                Customer = customer,
                CustomerId = customer.Id,
                ExtraComments = "Niets op aan te merken",
                OrderStatus = Models.Enums.OrderStatus.Wachten_op_betaling,
                PaymentProcessor = Models.Enums.PaymentProcessor.Contant,
                TimeSlot = timeSlot1,
                TimeSlotId = timeSlot1.Id,
                OrderLines = new[] { orderline }
            };
            orderline.Order = order;
            orderline.OrderID = order.Id;
            _context.OrderLines.Add(orderline);
            _context.Orders.Add(order);
            await _context.SaveChangesAsync();

            // Act
            var retrievedTuple = await _orderService.GetEdit(order.Id);

            // Assert
            Assert.IsNotNull(retrievedTuple);
            Assert.AreEqual(order, retrievedTuple.Item1);
            //Assert.AreEqual(typeof(List<Customer>), retrievedTuple.Item2);
            Assert.AreEqual(1, retrievedTuple.Item2.Count());
            Assert.AreEqual(1, retrievedTuple.Item3.Count());
            //Assert.AreEqual(typeof(List<Product>), retrievedTuple.Item4);
            Assert.AreEqual(1, retrievedTuple.Item4.Count());
        }

        [TestMethod]
        public async Task CheckTimeSlotEdit_ReturnsNull()
        {
            // Arrange
            var customer = new Customer
            {
                Id = 1,
                FirstName = "Diederik",
                LastName = "Billiet",
                BirthDate = DateTime.Now,
                Adress = "Pieter Verhaeghenlaan 6",
                Email = "diederik@ucll.be",
                PhoneNumber = "123456789"
            };
            _context.Customers.Add(customer);
            await _context.SaveChangesAsync();

            var cook = new Cook { Id = 1, FirstName = "Jan", LastName = "Kok", Adress = "Kokstraat", BirthDate = DateTime.Now.AddMonths(-5), Email = "jankok@ucll.be", PhoneNumber = "123456789", Password = "JankokPass123.." };
            var cookUser = new IdentityUser
            {
                Email = cook.Email,
                UserName = cook.Email,
                EmailConfirmed = true
            };
            cook.IdentityUser = cookUser;
            _context.Cooks.Add(cook);
            await _context.SaveChangesAsync();

            var timeSlot1 = new TimeSlot { CookId = cook.Id, Cook = cook, Id = 1, Date = DateTime.Now.AddHours(2), StartTime = DateTime.Now.AddHours(2), IsOccupied = false };
            _context.TimeSlots.Add(timeSlot1);
            await _context.SaveChangesAsync();

            var timeSlot2 = new TimeSlot { CookId = cook.Id, Cook = cook, Id = 2, Date = DateTime.Now.AddHours(2).AddMinutes(15), StartTime = DateTime.Now.AddHours(2).AddMinutes(15), IsOccupied = false };
            _context.TimeSlots.Add(timeSlot2);
            await _context.SaveChangesAsync();

            var productCategory = new ProductCategory
            {
                Id = 1,
                Category = "Afhaalgerechten",
                Tax = 6,
            };
            var product = new Product
            {
                Id = 1,
                Name = "Frietjes",
                Description = "Krokante frietjes met mayo",
                Price = 10,
                ProductCategoryId = 1,
                ProductCategory = productCategory,
            };
            _context.Products.Add(product);
            await _context.SaveChangesAsync();

            var orderline = new OrderLine { OrderLineId = 1, Dish = product, DishId = product.Id, Quantity = 5 };

            var order = new Order
            {
                Id = 1,
                CreationDate = DateTime.Now,
                Customer = customer,
                CustomerId = customer.Id,
                ExtraComments = "Niets op aan te merken",
                OrderStatus = Models.Enums.OrderStatus.Wachten_op_betaling,
                PaymentProcessor = Models.Enums.PaymentProcessor.Contant,
                TimeSlot = timeSlot1,
                TimeSlotId = timeSlot1.Id,
                OrderLines = new[] { orderline }
            };
            orderline.Order = order;
            orderline.OrderID = order.Id;
            _context.OrderLines.Add(orderline);
            _context.Orders.Add(order);
            await _context.SaveChangesAsync();

            int[] productIds = new int[] { product.Id };
            int[] quantities = new int[] { orderline.Quantity };
            // Act
            var resultIsNull = await _orderService.CheckTimeSlotEdit(order.Id, productIds, quantities);

            // Assert
            Assert.IsNull(resultIsNull);
        }

        [TestMethod]
        public async Task ChangeTimeslotEdit_Succes()
        {
            // Arrange
            var customer = new Customer
            {
                Id = 1,
                FirstName = "Diederik",
                LastName = "Billiet",
                BirthDate = DateTime.Now,
                Adress = "Pieter Verhaeghenlaan 6",
                Email = "diederik@ucll.be",
                PhoneNumber = "123456789"
            };
            _context.Customers.Add(customer);
            await _context.SaveChangesAsync();

            var cook = new Cook { Id = 1, FirstName = "Jan", LastName = "Kok", Adress = "Kokstraat", BirthDate = DateTime.Now.AddMonths(-5), Email = "jankok@ucll.be", PhoneNumber = "123456789", Password = "JankokPass123.." };
            var cookUser = new IdentityUser
            {
                Email = cook.Email,
                UserName = cook.Email,
                EmailConfirmed = true
            };
            cook.IdentityUser = cookUser;
            _context.Cooks.Add(cook);
            await _context.SaveChangesAsync();

            var timeSlot1 = new TimeSlot { CookId = cook.Id, Cook = cook, Id = 1, Date = DateTime.Now.AddHours(2), StartTime = DateTime.Now.AddHours(2), IsOccupied = false };
            _context.TimeSlots.Add(timeSlot1);
            await _context.SaveChangesAsync();

            var timeSlot2 = new TimeSlot { CookId = cook.Id, Cook = cook, Id = 2, Date = DateTime.Now.AddHours(2).AddMinutes(15), StartTime = DateTime.Now.AddHours(2).AddMinutes(15), IsOccupied = false };
            _context.TimeSlots.Add(timeSlot2);
            await _context.SaveChangesAsync();

            var productCategory = new ProductCategory
            {
                Id = 1,
                Category = "Afhaalgerechten",
                Tax = 6,
            };
            var product = new Product
            {
                Id = 1,
                Name = "Frietjes",
                Description = "Krokante frietjes met mayo",
                Price = 10,
                ProductCategoryId = 1,
                ProductCategory = productCategory,
            };
            _context.Products.Add(product);
            await _context.SaveChangesAsync();

            var orderline = new OrderLine { OrderLineId = 1, Dish = product, DishId = product.Id, Quantity = 5 };

            var order = new Order
            {
                Id = 1,
                CreationDate = DateTime.Now,
                Customer = customer,
                CustomerId = customer.Id,
                ExtraComments = "Niets op aan te merken",
                OrderStatus = Models.Enums.OrderStatus.Wachten_op_betaling,
                PaymentProcessor = Models.Enums.PaymentProcessor.Contant,
                TimeSlot = timeSlot1,
                TimeSlotId = timeSlot1.Id,
                OrderLines = new[] { orderline }
            };
            orderline.Order = order;
            orderline.OrderID = order.Id;
            _context.OrderLines.Add(orderline);
            _context.Orders.Add(order);
            await _context.SaveChangesAsync();

            // Act
            _orderService.ChangeTimeslotEdit(order, timeSlot1.Id, timeSlot2.Id);

            // Assert
            Assert.IsFalse(timeSlot1.IsOccupied);
            Assert.IsTrue(timeSlot2.IsOccupied);
        }

        [TestMethod]
        public async Task GetOrder_Succes()
        {
            // Arrange
            var customer = new Customer
            {
                Id = 1,
                FirstName = "Diederik",
                LastName = "Billiet",
                BirthDate = DateTime.Now,
                Adress = "Pieter Verhaeghenlaan 6",
                Email = "diederik@ucll.be",
                PhoneNumber = "123456789"
            };
            _context.Customers.Add(customer);
            await _context.SaveChangesAsync();

            var cook = new Cook { Id = 1, FirstName = "Jan", LastName = "Kok", Adress = "Kokstraat", BirthDate = DateTime.Now.AddMonths(-5), Email = "jankok@ucll.be", PhoneNumber = "123456789", Password = "JankokPass123.." };
            var cookUser = new IdentityUser
            {
                Email = cook.Email,
                UserName = cook.Email,
                EmailConfirmed = true
            };
            cook.IdentityUser = cookUser;
            _context.Cooks.Add(cook);
            await _context.SaveChangesAsync();

            var timeSlot1 = new TimeSlot { CookId = cook.Id, Cook = cook, Id = 1, Date = DateTime.Now.AddHours(2), StartTime = DateTime.Now.AddHours(2), IsOccupied = false };
            _context.TimeSlots.Add(timeSlot1);
            await _context.SaveChangesAsync();

            var timeSlot2 = new TimeSlot { CookId = cook.Id, Cook = cook, Id = 2, Date = DateTime.Now.AddHours(2).AddMinutes(15), StartTime = DateTime.Now.AddHours(2).AddMinutes(15), IsOccupied = false };
            _context.TimeSlots.Add(timeSlot2);
            await _context.SaveChangesAsync();

            var productCategory = new ProductCategory
            {
                Id = 1,
                Category = "Afhaalgerechten",
                Tax = 6,
            };
            var product = new Product
            {
                Id = 1,
                Name = "Frietjes",
                Description = "Krokante frietjes met mayo",
                Price = 10,
                ProductCategoryId = 1,
                ProductCategory = productCategory,
            };
            _context.Products.Add(product);
            await _context.SaveChangesAsync();

            var orderline = new OrderLine { OrderLineId = 1, Dish = product, DishId = product.Id, Quantity = 5 };

            var order = new Order
            {
                Id = 1,
                CreationDate = DateTime.Now,
                Customer = customer,
                CustomerId = customer.Id,
                ExtraComments = "Niets op aan te merken",
                OrderStatus = Models.Enums.OrderStatus.Wachten_op_betaling,
                PaymentProcessor = Models.Enums.PaymentProcessor.Contant,
                TimeSlot = timeSlot1,
                TimeSlotId = timeSlot1.Id,
                OrderLines = new[] { orderline }
            };
            orderline.Order = order;
            orderline.OrderID = order.Id;
            _context.OrderLines.Add(orderline);
            _context.Orders.Add(order);
            await _context.SaveChangesAsync();

            // Act
            var retrievedOrder = await _orderService.GetOrder(order.Id);

            // Assert
            Assert.IsNotNull(retrievedOrder);
            Assert.AreEqual(order, retrievedOrder);
        }

        [TestMethod]
        public async Task GetOrder_ReturnNull()
        {
            // Arrange
            var orderNotExistId = 123;

            // Act
            var retrievedOrder = await _orderService.GetOrder(orderNotExistId);

            // Assert
            Assert.IsNull(retrievedOrder);
        }

        [TestMethod]
        public async Task DeleteOrder_WhenTimeSlotBiggerThen120Minutes_DeleteSucces()
        {
            // Arrange
            var customer = new Customer
            {
                Id = 1,
                FirstName = "Diederik",
                LastName = "Billiet",
                BirthDate = DateTime.Now,
                Adress = "Pieter Verhaeghenlaan 6",
                Email = "diederik@ucll.be",
                PhoneNumber = "123456789"
            };
            _context.Customers.Add(customer);
            await _context.SaveChangesAsync();

            var cook = new Cook { Id = 1, FirstName = "Jan", LastName = "Kok", Adress = "Kokstraat", BirthDate = DateTime.Now.AddMonths(-5), Email = "jankok@ucll.be", PhoneNumber = "123456789", Password = "JankokPass123.." };
            var cookUser = new IdentityUser
            {
                Email = cook.Email,
                UserName = cook.Email,
                EmailConfirmed = true
            };
            cook.IdentityUser = cookUser;
            _context.Cooks.Add(cook);
            await _context.SaveChangesAsync();

            var timeSlot1 = new TimeSlot { CookId = cook.Id, Cook = cook, Id = 1, Date = DateTime.Now.AddHours(3), StartTime = DateTime.Now.AddHours(3), IsOccupied = false };
            _context.TimeSlots.Add(timeSlot1);
            await _context.SaveChangesAsync();

            var timeSlot2 = new TimeSlot { CookId = cook.Id, Cook = cook, Id = 2, Date = DateTime.Now.AddHours(3).AddMinutes(15), StartTime = DateTime.Now.AddHours(3).AddMinutes(15), IsOccupied = false };
            _context.TimeSlots.Add(timeSlot2);
            await _context.SaveChangesAsync();

            var productCategory = new ProductCategory
            {
                Id = 1,
                Category = "Afhaalgerechten",
                Tax = 6,
            };
            var product = new Product
            {
                Id = 1,
                Name = "Frietjes",
                Description = "Krokante frietjes met mayo",
                Price = 10,
                ProductCategoryId = 1,
                ProductCategory = productCategory,
            };
            _context.Products.Add(product);
            await _context.SaveChangesAsync();

            var orderline = new OrderLine { OrderLineId = 1, Dish = product, DishId = product.Id, Quantity = 5 };

            var order = new Order
            {
                Id = 1,
                CreationDate = DateTime.Now,
                Customer = customer,
                CustomerId = customer.Id,
                ExtraComments = "Niets op aan te merken",
                OrderStatus = Models.Enums.OrderStatus.Wachten_op_betaling,
                PaymentProcessor = Models.Enums.PaymentProcessor.Contant,
                TimeSlot = timeSlot1,
                TimeSlotId = timeSlot1.Id,
                OrderLines = new[] { orderline }
            };
            orderline.Order = order;
            orderline.OrderID = order.Id;
            _context.OrderLines.Add(orderline);
            _context.Orders.Add(order);
            await _context.SaveChangesAsync();

            // Act
            await _orderService.DeleteOrder(order);

            // Assert
            var deletedOrder = await _context.Orders.FirstOrDefaultAsync(o => o.Id == order.Id);
            Assert.IsNull(deletedOrder);
        }

        [TestMethod]
        public async Task DeleteOrder_WhenTimeSlotSmallerThen120Minutes_ReturnOrder()
        {
            // Arrange
            var customer = new Customer
            {
                Id = 1,
                FirstName = "Diederik",
                LastName = "Billiet",
                BirthDate = DateTime.Now,
                Adress = "Pieter Verhaeghenlaan 6",
                Email = "diederik@ucll.be",
                PhoneNumber = "123456789"
            };
            _context.Customers.Add(customer);
            await _context.SaveChangesAsync();

            var cook = new Cook { Id = 1, FirstName = "Jan", LastName = "Kok", Adress = "Kokstraat", BirthDate = DateTime.Now.AddMonths(-5), Email = "jankok@ucll.be", PhoneNumber = "123456789", Password = "JankokPass123.." };
            var cookUser = new IdentityUser
            {
                Email = cook.Email,
                UserName = cook.Email,
                EmailConfirmed = true
            };
            cook.IdentityUser = cookUser;
            _context.Cooks.Add(cook);
            await _context.SaveChangesAsync();

            var timeSlot1 = new TimeSlot { CookId = cook.Id, Cook = cook, Id = 1, Date = DateTime.Now.AddHours(1), StartTime = DateTime.Now.AddHours(1), IsOccupied = false };
            _context.TimeSlots.Add(timeSlot1);
            await _context.SaveChangesAsync();

            var timeSlot2 = new TimeSlot { CookId = cook.Id, Cook = cook, Id = 2, Date = DateTime.Now.AddHours(3).AddMinutes(15), StartTime = DateTime.Now.AddHours(3).AddMinutes(15), IsOccupied = false };
            _context.TimeSlots.Add(timeSlot2);
            await _context.SaveChangesAsync();

            var productCategory = new ProductCategory
            {
                Id = 1,
                Category = "Afhaalgerechten",
                Tax = 6,
            };
            var product = new Product
            {
                Id = 1,
                Name = "Frietjes",
                Description = "Krokante frietjes met mayo",
                Price = 10,
                ProductCategoryId = 1,
                ProductCategory = productCategory,
            };
            _context.Products.Add(product);
            await _context.SaveChangesAsync();

            var orderline = new OrderLine { OrderLineId = 1, Dish = product, DishId = product.Id, Quantity = 5 };

            var order = new Order
            {
                Id = 1,
                CreationDate = DateTime.Now,
                Customer = customer,
                CustomerId = customer.Id,
                ExtraComments = "Niets op aan te merken",
                OrderStatus = Models.Enums.OrderStatus.Wachten_op_betaling,
                PaymentProcessor = Models.Enums.PaymentProcessor.Contant,
                TimeSlot = timeSlot1,
                TimeSlotId = timeSlot1.Id,
                OrderLines = new[] { orderline }
            };
            orderline.Order = order;
            orderline.OrderID = order.Id;
            _context.OrderLines.Add(orderline);
            _context.Orders.Add(order);
            await _context.SaveChangesAsync();

            // Act
            await _orderService.DeleteOrder(order);

            // Assert
            var deletedOrder = await _context.Orders.FirstOrDefaultAsync(o => o.Id == order.Id);
            Assert.IsNotNull(deletedOrder);
        }
    }
}
