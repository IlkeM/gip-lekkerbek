﻿using Lekkerbek.Web.Data;
using Lekkerbek.Web.Models.Entities;
using Lekkerbek.Web.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lekkerbek.Web.Tests.Services
{
    [TestClass]
    public class CompanyServiceTests
    {
        private LekkerbekDbContext _context;
        private CompanyService _companyService;
        [TestInitialize]
        public void Setup()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<LekkerbekDbContext>()
            .UseSqlite(connection)
            .Options;

            _context = new LekkerbekDbContext(options);
            _context.Database.EnsureCreated();

            _companyService = new CompanyService(_context);
        }
        [TestCleanup]
        public void Cleanup()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }

        [TestMethod]
        public async Task CreateCompany_Succes()
        {
            // Arrange
            var company = new Company { Id = 1, Name = "Pelgrims", VatNumber = "BE123456789" };

            // Act
            await _companyService.CreateCompany(company);

            // Assert
            var retrievedCompany = await _context.Companies.FirstOrDefaultAsync(o => o.Id == company.Id);
            Assert.IsNotNull(retrievedCompany);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task CreateCompany_ExceptionThrown()
        {
            // Arrange
            Company company = null;

            // Act
            await _companyService.CreateCompany(company);

            // Assert
        }

        [TestMethod]
        public async Task GetCompany_Succes()
        {
            // Arrange
            var company = new Company { Id = 1, Name = "Pelgrims", VatNumber = "BE123456789" };
            _context.Companies.Add(company);
            await _context.SaveChangesAsync();

            // Act
            await _companyService.GetCompany(company.Id);

            // Assert
            var retrievedCompany = await _context.Companies.FirstOrDefaultAsync(o => o.Id == company.Id);
            Assert.IsNotNull(retrievedCompany);
            Assert.AreEqual(retrievedCompany, company);
        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public async Task GetCompany_ExceptionThrown()
        {
            // Arrange
            var companyThatNotExists = 123;

            // Act
            await _companyService.GetCompany(companyThatNotExists);

            // Assert
        }

        [TestMethod]
        public async Task EditCompany_Succes()
        {
            // Assert
            var company = new Company { Id = 1, Name = "Pelgrims", VatNumber = "BE123456789" };
            _context.Companies.Add(company);
            await _context.SaveChangesAsync();

            company.Name = "Test";

            // Act
            await _companyService.EditCompany(company);

            // Assert
            var retrievedCompany = await _context.Companies.FirstOrDefaultAsync(o => o.Id == company.Id);
            Assert.IsNotNull(retrievedCompany);
            Assert.AreEqual(retrievedCompany.Name, company.Name);
        }

        [TestMethod]
        public async Task DeleteCompany_Succes()
        {
            // Assert
            var company = new Company { Id = 1, Name = "Pelgrims", VatNumber = "BE123456789" };
            _context.Companies.Add(company);
            await _context.SaveChangesAsync();

            // Act
            await _companyService.DeleteCompany(company);

            // Assert
            var retrievedCompany = await _context.Companies.FirstOrDefaultAsync(o => o.Id == company.Id);
            Assert.IsNull(retrievedCompany);
        }

        [TestMethod]
        public async Task DeleteCompany_ReturnsNull()
        {
            // Arrange
            Company company = null;

            // Act
            var deletedCompany = await _companyService.DeleteCompany(company);

            // 
            Assert.IsNull(deletedCompany);
        }
    }
}
