﻿using Lekkerbek.Web.Data;
using Lekkerbek.Web.Models.Entities;
using Lekkerbek.Web.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lekkerbek.Web.Tests.Services
{
    [TestClass]
    public class MailServiceTests
    {
        private MailService _mailService;
        [TestInitialize]
        public void Setup()
        {
            _mailService = new MailService();
        }
        [TestMethod]
        public async Task MailTest_Succes()
        {
            // Arrange
            var customer = new Customer
            {
                Id = 1,
                FirstName = "Diederik",
                LastName = "Billiet",
                BirthDate = DateTime.Now,
                Adress = "Pieter Verhaeghenlaan 6",
                Email = "diederik@ucll.be",
                PhoneNumber = "123456789"
            };
            var cook = new Cook { Id = 1, FirstName = "Jan", LastName = "Kok", Adress = "Kokstraat", BirthDate = DateTime.Now.AddMonths(-5), Email = "jankok@ucll.be", PhoneNumber = "123456789", Password = "JankokPass123.." };

            var cookUser = new IdentityUser
            {
                Email = cook.Email,
                UserName = cook.Email,
                EmailConfirmed = true
            };
            cook.IdentityUserId = cookUser.Id;

            var timeSlot = new TimeSlot { CookId = cook.Id, Cook = cook, Id = 1, Date = DateTime.Now, StartTime = DateTime.Now.Date.AddHours(1), IsOccupied = true };

            var productCategory = new ProductCategory
            {
                Id = 1,
                Category = "Afhaalgerechten",
                Tax = 6,
            };
            var product = new Product
            {
                Id = 1,
                Name = "Frietjes",
                Price = 10,
                ProductCategoryId = 1,
                ProductCategory = productCategory,
            };
            var orderline = new OrderLine { OrderLineId = 1, OrderID = 1, DishId = 1, Dish = product, Quantity = 1 };

            var order = new Order
            {
                Id = 1,
                CreationDate = DateTime.Now.AddHours(-3),
                TimeSlotId = timeSlot.Id,
                TimeSlot = timeSlot,
                CustomerId = customer.Id,
                Customer = customer,
                OrderStatus = Models.Enums.OrderStatus.Wachten_op_betaling,
                OrderLines = new[] { orderline },
                PaymentProcessor = Models.Enums.PaymentProcessor.Contant,
            };

            // Act
            _mailService.Mail(order);

            // Arrange
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Mail_NoOrder_ExceptionThrown()
        {
            // Arrange
            Order orderNoExist = null;

            // Act
            _mailService.Mail(orderNoExist);

            // Assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task SendReminderMail_NoOrder_ExceptionThrown()
        {
            // Arrange
            Order orderNoExist = null;

            // Act
            await _mailService.SendReminderMail(orderNoExist);

            // Assert 
        }
    }
}
