﻿using Azure.Core;
using Lekkerbek.Web.Data;
using Lekkerbek.Web.Services;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace Lekkerbek.Web.Tests.Services
{
    [TestClass]
    public class EmailSenderTests
    {
        private EmailSender _emailSender;

        [TestInitialize]
        public void Setup()
        {
            _emailSender = new EmailSender();
        }

        [TestMethod]
        public void SendMail_Succes()
        {
            // Arrange
            string email = "diederik.billiet@student.ucll.be";
            string subject = "Confirm your mail";
            string HTMLMessage = $"Please confirm your account by <a href='#'>clicking here</a>.";

            // Act
            var retrievedsmtpClient = _emailSender.SendEmailAsync(email, subject, HTMLMessage);

            // Assert
            Assert.IsNotNull(retrievedsmtpClient);
        }
    }
}
