﻿using Lekkerbek.Web.Controllers;
using Lekkerbek.Web.Data;
using Lekkerbek.Web.Models.Entities;
using Lekkerbek.Web.Services;
using Lekkerbek.Web.ViewModels.Timeslots;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Telerik.SvgIcons;

namespace Lekkerbek.Web.Tests.Routes
{
    [TestClass]
    public class TimeSlotsRouteTest
    {
        private TimeSlotsController _controller;
        [TestInitialize]
        public void Init()
        {
            // Setup an in-memory SQLite database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();
            var options = new DbContextOptionsBuilder<LekkerbekDbContext>()
            .UseSqlite(connection)
            .Options;

            var mockTimeSlotService = new Mock<ITimeSlotService>();

            Cook cook1 = new Cook() { Id = 1, FirstName = "John", IdentityUserId = "1" };
            Cook cook2 = new Cook() { Id = 2, FirstName = "Nadir", IdentityUserId = "2" };
            List<TimeSlot> times = new List<TimeSlot>();

            for (int i = 0; i < 10; i++)
            {
                times.Add(new TimeSlot() { Id = i, CookId = i < 5 ? cook1.Id : cook2.Id, Cook = i < 5 ? cook1 : cook2, Date = DateTime.Now }); ;
            }


            mockTimeSlotService.Setup(x => x.GetTimeSlots()).ReturnsAsync(times);

            //var userManagerMock = new Mock<UserManager<IdentityUser>>(new Mock<IUserStore<IdentityUser>>().Object, null, null, null, null, null, null, null, null);
            //userManagerMock.Setup(x => x.FindByIdAsync(It.IsAny<string>())).ReturnsAsync((string userId) => new IdentityUser { Id = userId });


            //var superUser = new IdentityUser
            //{
            //    UserName = "super@ucll.be",
            //    Email = "super@ucll.be"
            //};
            //var cookUser = new IdentityUser
            //{
            //    UserName = "cook@ucll.be",
            //    Email = "cook@ucll.be"
            //};

            //var superRole = new IdentityRole { Name = "Super" };
            //var cookRole = new IdentityRole { Name = "Cook" };

            //var users = new List<IdentityUser> { superUser, cookUser }.AsQueryable();
            //userManagerMock.Setup(u => u.Users).Returns(users);

            //userManagerMock.Setup(u => u.IsInRoleAsync(superUser, superRole.Name)).ReturnsAsync(true);
            //userManagerMock.Setup(u => u.IsInRoleAsync(cookUser, cookRole.Name)).ReturnsAsync(true);

            //var adminUsers = new List<IdentityUser> { superUser };
            //userManagerMock.Setup(u => u.GetUsersInRoleAsync(superRole.Name)).ReturnsAsync(adminUsers);
            //var customerUsers = new List<IdentityUser> { cookUser };
            //userManagerMock.Setup(u => u.GetUsersInRoleAsync(cookRole.Name)).ReturnsAsync(customerUsers);

            var user = new Mock<ClaimsPrincipal>();
            user.Setup(x => x.Identity.IsAuthenticated).Returns(true);
            user.Setup(x => x.Identity.Name).Returns("super");
            user.Setup(x => x.IsInRole("Super")).Returns(true);
            _controller = new TimeSlotsController(mockTimeSlotService.Object)
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = new DefaultHttpContext { User = user.Object }
                }
            };

        }

        [TestCleanup]
        public void Cleanup()
        {
            _controller.Dispose();
        }

        [TestMethod]
        public async Task TimeSlotController_IndexRoute_ReturnsViewResult()
        {
            // Arrange
            // Act
            var result = await _controller.Index();
            //// Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            var viewResult = (ViewResult)result;
            var model = (List<IndexViewModel>)viewResult.Model;
            Assert.IsInstanceOfType(model, typeof(List<IndexViewModel>));
            var cook1VMs = model.Where(m => m.CookIdentityUserId == "1");
            var cook2VMs = model.Where(m => m.CookIdentityUserId == "2");
            var cook3VMs = model.Where(m => m.CookIdentityUserId == "3");
            Assert.IsNotNull(cook1VMs.FirstOrDefault());
            Assert.IsNotNull(cook2VMs.FirstOrDefault());
            Assert.IsNull(cook3VMs.FirstOrDefault());
        }



    }
}
