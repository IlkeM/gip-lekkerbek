﻿using Lekkerbek.Web.Data;
using Lekkerbek.Web.Models.Entities;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lekkerbek.Web.Tests.ModelValidations
{
    [TestClass]
    public class ProductTests
    {
        private LekkerbekDbContext _context;

        [TestInitialize]
        public void Initialize()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<LekkerbekDbContext>().UseSqlite(connection).Options;

            _context = new LekkerbekDbContext(options);
            _context.Database.EnsureCreated();
        }

        [TestCleanup]
        public void Cleanup()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }
        [TestMethod]
        public void ValidateProductModel_ProductCategoryChoice()
        {
            // Arrange
            var productCategory = new ProductCategory
            {
                Id = 1,
                Category = "Afhaalgerechten",
                Tax = 6,
            };
            var product = new Product
            {
                Id = 1,
                Name = "Frietjes",
                Price = 10,
                ProductCategoryId = 1,
                ProductCategory = productCategory,
            };

            // Act & Assert
            Assert.AreEqual(typeof(ProductCategory), product.ProductCategory.GetType());
        }

        [TestMethod]
        public void ValidateProductModel_PriceIsDecimal()
        {
            // Arrange
            var productCategory = new ProductCategory
            {
                Id = 1,
                Category = "Afhaalgerechten",
                Tax = 6,
            };
            var product = new Product
            {
                Id = 1,
                Name = "Frietjes",
                Price = 10,
                ProductCategoryId = 1,
                ProductCategory = productCategory,
            };

            // Act & Assert
            Assert.AreEqual(typeof(decimal), product.Price.GetType());
        }
    }
}
