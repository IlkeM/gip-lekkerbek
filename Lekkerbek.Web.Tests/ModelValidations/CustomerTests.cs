﻿using Lekkerbek.Web.Data;
using Lekkerbek.Web.Models.Entities;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lekkerbek.Web.Tests.ModelValidations
{
    [TestClass]
    public class CustomerTests
    {
        private LekkerbekDbContext _context;

        [TestInitialize]
        public void Initialize()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<LekkerbekDbContext>().UseSqlite(connection).Options;

            _context = new LekkerbekDbContext(options);
            _context.Database.EnsureCreated();
        }

        [TestCleanup]
        public void Cleanup()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }

        [TestMethod]
        public void ValidateCustomerModel_NameIsRequired()
        {
            // Arrange
            var customer = new Customer
            {
                Id = 1,
                FirstName = null,
                LastName = null,
                BirthDate = DateTime.Now,
                Adress = "Pieter Verhaeghenlaan 6",
                Email = "diederik@ucll.be",
                PhoneNumber = "123456789"
            };
            // Act & Assert
            Assert.ThrowsExceptionAsync<ValidationException>(() => _context.SaveChangesAsync());
        }

        [TestMethod]
        public void ValidateCustomerModel_FoodCategoryChoice()
        {
            // Arrange
            var customer = new Customer
            {
                FoodPreference = Models.Enums.FoodCategory.Vegetarisch,
                Id = 1,
                FirstName = "Diederik",
                LastName = "Billiet",
                BirthDate = DateTime.Now,
                Adress = "Pieter Verhaeghenlaan 6",
                Email = "diederik@ucll.be",
                PhoneNumber = "123456789"
            };
            // Act & Assert
            Assert.IsTrue(Enum.IsDefined(typeof(Models.Enums.FoodCategory), customer.FoodPreference));
        }
    }
}
