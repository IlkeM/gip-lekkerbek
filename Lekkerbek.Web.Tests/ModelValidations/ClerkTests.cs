﻿using Lekkerbek.Web.Data;
using Lekkerbek.Web.Models.Entities;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lekkerbek.Web.Tests.ModelValidations
{
    [TestClass]
    public class ClerkTests
    {
        private LekkerbekDbContext _context;

        [TestInitialize]
        public void Initialize()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<LekkerbekDbContext>().UseSqlite(connection).Options;

            _context = new LekkerbekDbContext(options);
            _context.Database.EnsureCreated();
        }

        [TestCleanup]
        public void Cleanup()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }

        [TestMethod]
        public void ValidateCookModel_PasswordIsRequired()
        {
            // Arrange
            var clerk = new Clerk { Id = 1, FirstName = "Dirk", LastName = "Dirkens", BirthDate = DateTime.Now, Adress = "Kokstraat 1", Email = "dirk@ucll.be", PhoneNumber = "149876541", Password = null };

            // Act & Assert
            Assert.ThrowsExceptionAsync<ValidationException>(() => _context.SaveChangesAsync());
        }
    }
}
