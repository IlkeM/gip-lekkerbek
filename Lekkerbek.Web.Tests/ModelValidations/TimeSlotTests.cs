﻿using Lekkerbek.Web.Data;
using Lekkerbek.Web.Models.Entities;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lekkerbek.Web.Tests.ModelValidations
{
    [TestClass]
    public class TimeSlotTests
    {
        private LekkerbekDbContext _context;

        [TestInitialize]
        public void Initialize()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<LekkerbekDbContext>().UseSqlite(connection).Options;

            _context = new LekkerbekDbContext(options);
            _context.Database.EnsureCreated();
        }

        [TestCleanup]
        public void Cleanup()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }

        [TestMethod]
        public void IsOccupied_NoOrderMade_ReturnsFalse()
        {
            // Arrange
            var timeSlot = new TimeSlot { Id = 1, StartTime = DateTime.Now, Date = DateTime.Now };

            // Act & Assert
            Assert.IsFalse(timeSlot.IsOccupied);
        }

        [TestMethod]
        public void EndTime_DifferenceIs15Minutes_ReturnTrue()
        {
            // Arrange
            var timeSlot = new TimeSlot { Id = 1, StartTime = DateTime.Now, Date = DateTime.Now };

            // Act & Assert
            Assert.AreEqual(timeSlot.EndTime, timeSlot.StartTime.AddMinutes(15));
        }
    }
}
