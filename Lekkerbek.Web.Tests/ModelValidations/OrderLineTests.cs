﻿using Lekkerbek.Web.Data;
using Lekkerbek.Web.Models.Entities;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lekkerbek.Web.Tests.ModelValidations
{
    [TestClass]
    public class OrderLineTests
    {
        private LekkerbekDbContext _context;

        [TestInitialize]
        public void Initialize()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            var options = new DbContextOptionsBuilder<LekkerbekDbContext>().UseSqlite(connection).Options;

            _context = new LekkerbekDbContext(options);
            _context.Database.EnsureCreated();
        }

        [TestCleanup]
        public void Cleanup()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }

        [TestMethod]
        public void ValidateOrderlineModel_WhenQuantityBelowZero_ExceptionThrown()
        {
            // Arrange

            //var customer = new Customer
            //{
            //    Id = 1,
            //    FirstName = "Diederik",
            //    LastName = "Billiet",
            //    BirthDate = DateTime.Now,
            //    Adress = "Pieter Verhaeghenlaan 6",
            //    Email = "diederik@ucll.be",
            //    PhoneNumber = "123456789"
            //};
            //var productCategory = new ProductCategory
            //{
            //    Id = 1,
            //    Category = "Afhaalgerechten",
            //    Tax = 6,
            //};
            //var product = new Product
            //{
            //    Id = 1,
            //    Name = "Frietjes",
            //    Price = 10,
            //    ProductCategoryId = 1,
            //    ProductCategory = productCategory,
            //};
            //var order1 = new Order
            //{
            //    Id = 1,
            //    CreationDate = DateTime.Now.AddHours(-3),
            //    TimeSlotId = 1,
            //    CustomerId = customer.Id,
            //    OrderStatus = Models.Enums.OrderStatus.Wachten_op_betaling,
            //};
            var orderline = new OrderLine { Quantity = -5 };

            // Act & Assert
            Assert.ThrowsExceptionAsync<ValidationException>(() => _context.SaveChangesAsync());
        }
    }
}
