﻿using Lekkerbek.Web.Controllers;
using Lekkerbek.Web.Data;
using Lekkerbek.Web.Models.Entities;
using Lekkerbek.Web.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.SvgIcons;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using Moq;
using Microsoft.AspNetCore.Http;

namespace Lekkerbek.Web.Tests.Integrations
{
    [TestClass]
    public class EmployeeTests
    {
        private LekkerbekDbContext _context;
        private UserManager<IdentityUser> userManager;
        private ClaimsPrincipal _user;
        ITimeSlotService timeSlotService;
        string userId = string.Empty;

        [TestInitialize]
        public void Initialize()
        {
            //Create an in-memory SQLite database
            var connection = new SqliteConnection("DataSource=:memory:");
            connection.Open();

            //Initialize the DbContext using the SQLite connection
            _context = new LekkerbekDbContext(new DbContextOptionsBuilder<LekkerbekDbContext>().UseSqlite(connection).Options);

            //Create the database schema
            _context.Database.EnsureCreated();

            //User manager

            var userManagerMock = new Mock<UserManager<IdentityUser>>(new Mock<IUserStore<IdentityUser>>().Object, null, null, null, null, null, null, null, null);

            userManagerMock.Setup(x => x.FindByIdAsync(It.IsAny<string>())).ReturnsAsync((string userId) => new IdentityUser { Id = userId });

            var identityUser = new IdentityUser { UserName = "test@ucll.be" };
            // voeg the aspnetuser toe aan de db (zodat de clerk kan gelinkt worden)
            _context.Users.Add(identityUser);
            _context.SaveChanges();

            userId = identityUser.Id;

            userManagerMock
                .Setup(x => x.CreateAsync(It.IsAny<IdentityUser>(), It.IsAny<string>()))
                .ReturnsAsync((IdentityUser identityUser, string _) =>
                {
                    identityUser.Id = userId; // Set a test Id for the newly created user
                    return IdentityResult.Success;
                });

            

            //Create the controller instance with the required dependencies

            var userMock = new Mock<ClaimsPrincipal>();
            userMock.Setup(x => x.Identity.IsAuthenticated).Returns(true);
            userMock.Setup(x => x.Identity.Name).Returns("testuser");
            userMock.Setup(x => x.FindFirst(ClaimTypes.Role)).Returns(new
           Claim(ClaimTypes.Role, "Administrator"));
            userManager = userManagerMock.Object;
            _user = userMock.Object;
        }

        [TestCleanup]
        public void Cleanup()
        {
            _context.Dispose();
        }

        [TestMethod]
        public async Task AddEmployee_ValidData_Success()
        {
            // Arrange
            var employeeController = new EmployeesController(new CookService(_context, userManager, timeSlotService), new ClerkService(userManager, _context))
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = new DefaultHttpContext { User = _user }
                }
            };

            var newEmployee = new Clerk
            {
                IdentityUserId = "1",
                Password = "Lekkerbekje14!",
                LastName = "Peeters",
                Email = "hallojan@devos.com",
                Adress = "Wilselsesteenweg 355",
                PhoneNumber = "0498 62 32 15",
                BirthDate = DateTime.Now,
                FirstName = "Viola"
            };

            // Act 
            var result = await employeeController.ClerkCreate(newEmployee);

            //Assert
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));

            var redirectResult = (RedirectToActionResult)result;
            Assert.AreEqual("Index", redirectResult.ActionName);

            //Verify that the newCustomer object is created in the DbContext

            var savedEmployee = await _context.Clerks.FirstOrDefaultAsync(c => c.IdentityUserId == userId);
            Assert.IsNotNull(savedEmployee);
            Assert.AreEqual("hallojan@devos.com", savedEmployee.Email);
        }
    }
}
